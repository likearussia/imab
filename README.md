# Este projeto está sendo desenvolvido com o framework laravel.
## Definições do projeto:
    - Versão do php: 5.6
    - Versão do laravel: 1.3.3
        - Documentação em português: http://laravel.artesaos.org/ 
        - Documentação em inglês: https://laravel.com/docs/5.2/ 


## Instalação.
### Ao fazer clone do projeto, com o composer já instalado na máquina, vá até a pasta dos arquivos e execute os comandos:
* composer global require "laravel/installer".
* composer update.

#### Criar um arquivo com nome .env na raiz do projeto, e copiar o conteudo de .env.example, onde você deve mudar as configurações para as configurações do servidor que está sendo utilizado.

#### Após modificar o .env executar o seguinte comando:

* php artisan key:generate.
