<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Dicas extends Model
{
    protected $table = 'dicas';
    protected $fillable  = ['titulo','conteudo','imagem_destaque','slug','categoria','status', 'miniatura', 'resumo'];
    public static $diretorio_imagens = 'imagens/upload/posts';
}
