<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class configuracoes extends Model
{
    protected $table = "configuracoes";

    protected $fillable = ["email"];
}
