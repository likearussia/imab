<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Institucional extends Model
{
    protected $table = "institucionais";
    
    protected $fillable = ['titulo','subtitulo','imagem_destaque','texto','slug','timeline'];
    
}
