<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class curriculo extends Model
{
    protected $table = "curriculo";
    
    protected $fillable = ['id','dadospessoais','formacao','idiomas','aplicativos','experiencia','pretensao', 'created_at', 'deleted_at', 'updated_at'];
    
    
}
