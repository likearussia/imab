<?php 

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = "banners";
    public static $imageDir = "imagens/upload/banners";
    protected $fillable = ['link','caminho','status','nova_aba','titulo'];
}
