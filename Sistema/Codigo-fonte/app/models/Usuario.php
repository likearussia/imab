<?php

namespace App\models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class Usuario extends Authenticatable
{
    
    use SoftDeletes;
    
    protected $table = "usuarios";
    
    protected $guarded = ['id','deleted_at'];
    
    protected $hidden = [
        'senha', 'remeber_token'
    ];
    
    protected $dates = ['deleted_at'];
    
    
    public function getCreatedAttribute($value){
        return Carbon::parse($value)->format("d/m/Y H:i:s");
    }
            
}
