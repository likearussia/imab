<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;

class usuario_site extends Authenticatable {

    protected $table = 'usuarios_site';
    protected $hidden = [
        'password', 'remeber_token'
    ];
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['nome', 'email', 'password', 'tipo', 'status', 'segmento', 'nome_fantasia', 'telefone', 'cidade', 'estado', 'site', 'razao_social', 'cnpj', 'cargo', 'celular'];

}
