<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class download extends Model
{
    protected $table = 'downloads';
    protected $fillable = ['titulo','caminho', 'descricao', 'usuarios', 'nome'];
    public static $diretorio_imagens = 'downloads';
}
