<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class categoria extends Model
{
    protected $table = 'categoria_dicas';
    protected $fillable = ['nome','slug','status'];
    

    public static function listar(){
        $query = categoria::select('id','nome')->where('status','1');
        $retorno = [];
        $resultados = $query->get();
        foreach ($resultados as $resultado){
            $retorno[$resultado->id] = $resultado->nome;
        }
        
        return $retorno;
    }
    
    
}
