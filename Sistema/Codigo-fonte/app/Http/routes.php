<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */



Route::group([], function(){



    Route::get('/fechaduras', function(){
        return view('site.fechaduras');
    });


    Route::get('/dobradicas-pivots', function(){
        return view('site.dobradicas-pivots');
    });


    Route::get('/puxadores', function(){
        return view('site.puxadores');
    });


    Route::get('/ferragens', function(){
        return view('site.ferragens');
    });


    Route::get('/componentes', function(){
        return view('site.componentes');
    });

    Route::match(['get', 'post'],'/recuperar/{id?}', [

        'uses' => 'Site\usuarioController@novaSenha', 'as' => 'site::novasenha'

    ]);


    Route::post('/buscar', [
        'uses' => 'Site\HomeController@buscar', 'as' => 'site::buscar'
    ]);

    Route::get('/', [
        'uses' => 'Site\HomeController@index', 'as' => 'site::index'
    ]);

    Route::get('/mail', [
       'uses' => 'Site\HomeController@testemail', 'as' => 'site::mail'
    ]);

    Route::get('/usuario', [
        'uses' => 'Site\usuarioController@index', 'as' => 'index'
    ]);

    Route::get('/arquivos-digitais', [
        'uses' => 'Site\downloadController@listar', 'as' => 'arquivos'
    ]);

    Route::get('/getFile/{arquivo}', [
        'uses' => 'Site\downloadController@download', 'as' => 'baixar'
    ]);
    
    Route::get('/ver-produto', [
        'uses' => 'Site\HomeController@produto', 'as' => 'produto'
    ]);


    Route::group(['as' => 'usuario::', 'prefix' => 'usuario', 'middleware' => ['authUser']], function() {

        Route::get('/perfil', [
            'uses' => 'Site\usuarioController@perfil', 'as' => 'perfil'
        ]);

        Route::get('/editar', [
            'uses' => 'Site\usuarioController@formEditar', 'as' => 'editar'
        ]);

        Route::post('/editar/{id}', [
            'uses' => 'Site\usuarioController@editar', 'as' => 'editar'
        ]);


        Route::post('/alterar-senha/{id}', [
            'uses' => 'Site\usuarioController@alterarSenha', 'as' => 'alterar-senha'
        ]);
    });

    Route::post('/logar', [
        'uses' => 'Site\usuarioController@logar', 'as' => 'site::logar'
    ]);

    Route::post('/recuperar-senha', [
        'uses' => 'Site\usuarioController@recuperar_senha', 'as' => 'site::recuperar'
    ]);


    Route::match(['get', 'head'], '/logout', [
        'uses' => 'Site\usuarioController@logout', 'as' => 'site::logout'
    ]);


    Route::group(['as' => 'trabalhe::', 'prefix' => 'trabalhe-conosco'], function() {

        Route::get('/', [
            'uses' => 'Site\curriculoController@index', 'as' => 'index'
        ]);

        Route::post('/enviar-curriculo', [
            'uses' => 'Site\curriculoController@enviar', 'as' => 'enviar'
        ]);
    });


    Route::group(['as' => 'dicas::', 'prefix' => 'dicas-e-solucoes'], function() {

        Route::get('/', [
            'uses' => 'Site\DicasController@index', 'as' => 'site::dicas::index'
        ]);

        Route::get('/{slug}/{id}', [
            'uses' => 'Site\DicasController@visualizar', 'as' => 'visualizar'
        ]);

        Route::post('/', [
            'uses' => 'Site\DicasController@buscar', 'as' => 'buscar'
        ]);



    });

    Route::post('/fale-conosco', [
        'uses' => 'Site\contatoController@enviar', 'as' => 'site::faleconosco'
    ]);

    Route::post('/cadastrarUsuario', [
        'uses' => 'Site\usuarioController@cadastrar', 'as' => 'site::cadastrar'
    ]);


    //institucional - Valores Humanos
    Route::get('/valores-humanos', [
        'uses' => 'Site\InstitucionaisController@valoresHumanos', 'as' => 'valores'
    ]);

    //institucional - Inovação e qualidade
    Route::get('/cidadania-corporativa', [
        'uses' => 'Site\InstitucionaisController@cidadaniaCorporativa', 'as' => 'cidadania'
    ]);

    //institucional - historia
    Route::get('/historia', [
        'uses' => 'Site\InstitucionaisController@historia', 'as' => 'historia'
    ]);

    //institucional - Inovação e qualidade
    Route::get('/inovacao-e-qualidade', [
        'uses' => 'Site\InstitucionaisController@inovacao', 'as' => 'inovacao'
    ]);







    //institucional - Dicas e soluções - single
    Route::get('/single-dica', function () {
        return view('site/single-dica');
    });

    //institucional - Dicas e soluções - single 2
    Route::get('/single-dica-2', function () {
        return view('site/single-dica2');
    });


    //produtos - Linhas 

    Route::get('/linhas', function () {
        return view('site/linhas');
    });

    //produtos - Modelo

    Route::get('/modelos', function () {
        return view('site/modelos');
    });

    //institucional - Fale conosco
    Route::get('/fale-conosco', [
        'uses' => 'Site\contatoController@index', 'as' => 'site::fale-conosco'
    ]);


    //Sistema - Personalize
    Route::get('/personalize', function () {
        return view('site/personalize');
    });

    // Sistema - Duvidas 
    Route::get('/duvidas', function () {
        return view('site/duvidas');
    });
});

//Route::auth();
Route::group(['namespace' => 'Auth', 'prefix' => '_auth'], function () {
    Route::match(['get', 'head'], '/login', [
        'uses' => 'AuthController@showFormLogin', 'as' => 'app::login'
    ]);

    Route::post('/login', [
        'uses' => 'AuthController@login', 'as' => 'app::post_login'
    ]);

    Route::match(['get', 'head'], '/logout', [
        'uses' => 'AuthController@logout', 'as' => 'app::logout'
    ]);
});

// App\Http\Controllers\App
Route::group(['namespace' => 'app', 'as' => 'app::', 'prefix' => '_app', 'middleware' => ['auth']], function () {

    Route::match(['get', 'post'], '/image', [
        'uses' => 'imageController@UpImagem', 'as' => 'imagem'
    ]);


    // Início
    Route::match(['get', 'post'], '/', ['uses' => 'IndexController@index', 'as' => 'inicio']);


    // Banners
    Route::group(['as' => 'banner::', 'prefix' => 'banner'], function () {

        Route::get('/', [
            'uses' => 'BannerController@index', 'as' => 'index'
        ]);

        Route::get('/criar', [
            'uses' => 'BannerController@create', 'as' => 'criar'
        ]);

        Route::post('/salvar/{id?}', [
            'uses' => 'BannerController@salvar', 'as' => 'salvar'
        ]);

        Route::get('/editar/{id}', [
            'uses' => 'BannerController@editar', 'as' => 'editar'
        ]);

        Route::get('/deletar/{id?}', [
            'uses' => 'BannerController@delete', 'as' => 'deletar'
        ]);

        Route::get('/mudarstatus/{id?}/{status?}', [
            'uses' => 'BannerController@changeStatus', 'as' => 'status'
        ]);
    });




    Route::group(['as' => 'dicas::', 'prefix' => 'dicas-solucoes'], function () {
        Route::get('/', [
            'uses' => 'DicasController@index', 'as' => 'index'
        ]);

        Route::match(['get', 'post'], '/criar', [
            'uses' => 'DicasController@criar', 'as' => 'criar'
        ]);

        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'DicasController@editar', 'as' => 'editar'
        ]);

        Route::get('/mudarstatus/{id}/{status}', [
            'uses' => 'DicasController@changestatus', 'as' => 'status'
        ]);

        Route::get('/deletar/{id}', [
            'uses' => 'DicasController@deletar', 'as' => 'deletar'
        ]);


        Route::group(['as' => 'categoria::', 'prefix' => 'categorias'], function() {

            Route::get('/', [
                'uses' => 'ControllerCategoria@index', 'as' => 'index'
            ]);

            Route::match(['get', 'post'], '/criar', [
                'uses' => 'ControllerCategoria@criar', 'as' => 'criar'
            ]);

            Route::match(['get', 'post'], '/editar/{id}', [
                'uses' => 'ControllerCategoria@editar', 'as' => 'editar'
            ]);

            Route::get('/mudarstatus/{id}/{status}', [
                'uses' => 'ControllerCategoria@changeStatus', 'as' => 'status'
            ]);
        });
    });

    Route::group(['as' => 'usuario::', 'prefix' => 'usuarios'], function() {

        Route::get('/', [
            'uses' => 'UsuarioController@index', 'as' => 'index'
        ]);

        Route::match(['get', 'post'], '/criar', [
            'uses' => 'UsuarioController@criar', 'as' => 'criar'
        ]);

        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'UsuarioController@editar', 'as' => 'editar'
        ]);

        Route::get('/deletar/{id}', [
            'uses' => 'UsuarioController@deletar', 'as' => 'deletar'
        ]);
    });

    Route::group(['as' => 'usuarios-site::', 'prefix' => 'usuarios-site'], function() {

        Route::get('/', [
            'uses' => 'UsuariossiteController@index', 'as' => 'index'
        ]);

        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'UsuariossiteController@editar', 'as' => 'editar'
        ]);

        Route::get('/mudartipo/{id}/{para}', [
            'uses' => 'UsuariossiteController@mudarTipo', 'as' => 'mudarTipo'
        ]);

        Route::get('/status/{id}/{novoStatus}', [
            'uses' => 'UsuariossiteController@status', 'as' => 'status'
        ]);

        Route::get('/deletar/{id}', [
            'uses' => 'UsuariossiteController@deletar', 'as' => 'deletar'
        ]);
    });

    Route::group(['as' => 'institucional::', 'prefix' => 'institucionais'], function() {

        Route::get('/', [
            'uses' => 'InstitucionalController@index', 'as' => 'index'
        ]);

        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'InstitucionalController@editar', 'as' => 'editar'
        ]);
    });

    Route::group(['as' => 'curriculo::', 'prefix' => 'curriculo'], function() {
        Route::get('/curriculos', [
            'uses' => 'curriculoController@index', 'as' => 'index'
        ]);

        Route::get('/ver-curriculo/{id}', [
            'uses' => 'curriculoController@visualizar', 'as' => 'visualizar'
        ]);
    });

    Route::group(['as' => 'download::', 'prefix' => 'download'], function() {

        Route::get('/', [
            'uses' => 'downloadController@index', 'as' => 'index'
        ]);

        Route::match(['get', 'post'], '/criar', [
            'uses' => 'downloadController@criar', 'as' => 'criar'
        ]);

        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'downloadController@editar', 'as' => 'editar'
        ]);

        Route::get('/getFile/{arquivo}', [
            'uses' => 'downloadController@download', 'as' => 'baixar'
        ]);

        Route::get('/deletar/{id}', [
            'uses' => 'downloadController@deletar', 'as' => 'deletar'
        ]);
    });

    Route::group(['as' => 'configuracoes::', 'prefix' => 'configuracoes'], function (){

        Route::get('/', [
            'uses' => 'configuracaoController@index', 'as' => 'index'
        ]);

        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'configuracaoController@editar', 'as' => 'editar'
        ]);
    });


    Route::group(['as' => 'news::', 'prefix' => 'newsletter'], function(){
        Route::get('/', [
            'uses' => 'newsletterController@index', 'as' => 'index'
        ]);

        Route::get('/deletar/{id}', [
           'uses' => 'newsletterController@apagar', 'as' => 'deletar'
        ]);
    });
});



