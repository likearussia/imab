<?php

namespace App\Http\Controllers\Auth;

use App\models\Usuario;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class AuthController extends Controller {

    use AuthenticatesAndRegistersUsers,
        ThrottlesLogins;

    protected $redirectTo = '/';

    public function __construct() {

        $this->redirectTo = route('app::inicio');

        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    protected function showFormLogin() {
        return view('auth.login');
    }

    protected function login(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
                ], [
            'required' => 'O campo :attribute é obrigatório'
                ], [
            'inputUser' => 'e-mail',
            'inputPassword' => 'senha'
        ]);

        $usuario = $request->input('email');
        $senha = $request->input('password');
        $lembrar = ($request->has('remember') ? true : false);

        if (Auth::attempt(['email' => $usuario, 'password' => $senha], $lembrar)) {
            $request->session()->put('eh_admin', true);
            return redirect()->intended(route('app::inicio'));
        }

        flash()->error("Usuario e/ou senha incorretos");

        return redirect()->back();
    }

    protected function validator(array $data) {
        return Validator::make($data, [
                    'nome' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function logout() {
        Auth::logout();

        return redirect(url('/'));
    }

}
