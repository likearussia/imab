<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\curriculo;
use Illuminate\Support\Facades\Mail;
use App\models\configuracoes;

class curriculoController extends Controller {

    public function index() {
        return view('site.trabalheconosco');
    }

    public function enviar(Request $request) {
        
        $this->validate($request, [
            'dados.nome' => 'required',
            'dados.nascimento' => 'required',
            'dados.email' => 'required',
            'dados.cep' => 'required',
            'dados.endereco' => 'required',
            'dados.numero' => 'required',
            'dados.complemento' => 'required',
            'dados.cidade' => 'required',
            'dados.bairro' => 'required',
            'dados.telefone' => 'required',
            'dados.celular' => 'required',
            'dados.estado_civil' => 'required',
            'dados.filhos' => 'required',
            'dados.conheceu' => 'required',
            'dados.area' => 'required',
            'formacao.instituicao.0' => 'required',
            'formacao.curso.0' => 'required',
            'formacao.conclusao.0' => 'required',
            'formacao.instituicao.1' => 'required',
            'formacao.curso.1' => 'required',
            'formacao.conclusao.1' => 'required',
            'espanhol.leitura' => 'required',
            'espanhol.escrita' => 'required',
            'espanhol.fala' => 'required',
            'ingles.leitura' => 'required',
            'ingles.escrita' => 'required',
            'ingles.fala' => 'required',
            'experiencia.empresa.0' => 'required',
            'experiencia.cargo.0' => 'required',
            'experiencia.inicio.0' => 'required',
            'experiencia.termino.0' => 'required',
            'experiencia.empresa.1' => 'required',
            'experiencia.cargo.1' => 'required',
            'experiencia.inicio.1' => 'required',
            'experiencia.termino.1' => 'required',
            'pretensao_salarial' => 'required',
            'aplcativos.windows' => 'required',
            'aplcativos.internet' => 'required',
            'aplcativos.excel' => 'required',
            'aplcativos.powerpoint' => 'required',
            'aplcativos.word' => 'required',
            'aplcativos.outros' => 'required',
                ],[
            'required' => 'O campo :attribute é obrigatório',
            ], [
            'dados.nome' => 'nome',
            'dados.nascimento' => 'data de nascimento',
            'dados.email' => 'e-mail',
            'dados.cep' => 'CEP',
            'dados.endereco' => 'endereço',
            'dados.numero' => 'número',
            'dados.complemento' => 'complemento',
            'dados.cidade' => 'cidade',
            'dados.bairro' => 'bairro',
            'dados.telefone' => 'DDD/telefone',
            'dados.celular' => 'DDD/celular',
            'dados.estado_civil' => 'estado civil',
            'dados.filhos' => 'Número de filhos',
            'dados.conheceu' => 'como conheceu a imab?',
            'dados.area' => 'área desejada',
            'formacao.instituicao.0' => 'instituição 1',
            'formacao.curso.0' => 'curso 1',
            'formacao.conclusao.0' => 'conclusão 1',
            'formacao.instituicao.1' => 'instituição 2',
            'formacao.curso.1' => 'curso 2',
            'formacao.conclusao.1' => 'conclusão 2',
            'espanhol.leitura' => 'espanhol (leitura)',
            'espanhol.escrita' => 'espanhol (escrita)',
            'espanhol.fala' => 'espanhol (fala)',
            'ingles.leitura' => 'inglês (leitura)',
            'ingles.escrita' => 'inglês (escrita)',
            'ingles.fala' => 'inglês (fala)',
            'aplcativos.windows' => 'aplicativos (windows)',
            'aplcativos.internet' => 'aplicativos - internet',
            'aplcativos.excel' => 'aplicativos - excel',
            'aplcativos.powerpoint' => 'aplicativos - powerpoint',
            'aplcativos.word' => 'aplicativos - word',
            'aplcativos.outros' => 'aplicativos - outros',
            'experiencia.empresa.0' => 'emrepsa 1',
            'experiencia.cargo.0' => 'cargo 1',
            'experiencia.inicio.0' => 'inicio 1',
            'experiencia.termino.0' => 'termino 1',
            'experiencia.empresa.1' => 'empresa 2',
            'experiencia.cargo.1' => 'cargo 2',
            'experiencia.inicio.1' => 'inicio 2',
            'experiencia.termino.1' => 'termino 2',
            'pretensao_salarial' => 'pretensão salarial',
        ]);


        $input = $request->input();

        unset($input['_token']);

        $idiomas['espanhol'] = $input['espanhol'];
        $idiomas['ingles'] = $input['ingles'];

        $input['dadospessoais'] = json_encode($input['dados']);
        $input['formacao'] = json_encode($input['formacao']);
        $input['idiomas'] = json_encode($idiomas);
        $input['aplicativos'] = json_encode($input['aplcativos']);
        $input['experiencia'] = json_encode($input['experiencia']);
        $input['pretensao'] = $input['pretensao_salarial'];


        try {
            curriculo::create($input);

            $this->sendMail();

            flash()->success("Dados enviados com sucesso");

            return redirect(route('trabalhe::index'));
        } catch (\Illuminate\Database\QueryException $ex) {
            $exception = $ex->getPrevious();
            flash()->error("Erro ao enviar dados. Detalhes: " . $exception->getMessage());
            return redirect()->back()->withInput();
        }
    }



    public function sendMail(){

        $data = "";

        try{
            $enviar = Mail::send(['html' => 'site.emails.curriculo'], ['data' => $data], function($m){

                $config = configuracoes::findOrFail(4);

                $m->from('igor@ph2.com.br', 'imab');
                $m->to($config->email)->subject('contato pelo site');
                $m->to('igorcarlos1997@gmail.com','imab')->subject('contato pelo site');
                //$m->to('keyla@ph2.com.br','imab')->subject('contato pelo site');

            });

        }catch (Exception $ex){
            echo $ex;
        }

    }

}
