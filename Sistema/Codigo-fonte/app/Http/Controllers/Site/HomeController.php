<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Banner;
use App\models\Institucional;
use Illuminate\Support\Facades\Mail;
use App\models\Dicas;
use Illuminate\Support\Facades\DB;
use App\models\categoria;


class HomeController extends Controller {

    public function index() {
        $dados = [];

        $banners = Banner::orderBy('created_at', 'DESC')->where('status', '1');

        $dados['banners'] = $banners->get();
        $dados['banners_qtd'] = $banners->get()->count();
        $dados['dir_uploads'] = Banner::$imageDir;


        return view('site.home', $dados);
    }

    public function produto() {
        return view('site.produto');
    }

    public function testemail(){

        /*// dados para email de contato
        $data['data']['segmento'] = "teste";
        $data['data']['nome'] = "teste";
        $data['data']['telefone'] = "teste";
        $data['data']['email'] = "teste";
        $data['data']['cep'] = "teste";
        $data['data']['endereco'] = "teste";
        $data['data']['numero'] = "teste";
        $data['data']['complemento'] = "teste";
        $data['data']['bairro'] = "teste";
        $data['data']['estado'] = "teste";
        $data['data']['cidade'] = "teste";
        $data['data']['atividade'] = "teste";
        $data['data']['conheceu'] = "teste";
        $data['data']['mensagem'] = "teste";*/
        /*$data['segmento'] = "teste";
        $data['nome'] = "teste";
        $data['telefone'] = "teste";
        $data['email'] = "teste";
        $data['cep'] = "teste";
        $data['endereco'] = "teste";
        $data['numero'] = "teste";
        $data['complemento'] = "teste";
        $data['bairro'] = "teste";
        $data['estado'] = "teste";
        $data['cidade'] = "teste";
        $data['atividade'] = "teste";
        $data['conheceu'] = "teste";
        $data['mensagem'] = "teste";*/

        /*$data['data']['tipo'] = '2';
        $data['data']['nome'] = 'Igor Carlos da Silva';
        $data['data']['email'] = 'igorcarlos1997@hotmail.com';
        $data['data']['segmento'] = 'Ferragem';
        $data['data']['site'] = 'site.igorcarlos.com.br';
        $data['data']['telefone'] = '(11) 4168-4502';
        $data['data']['cidade'] = 'Barueri';
        $data['data']['estado'] = 'SP';
        $data['data']['cargo'] = 'Programador';
        $data['data']['celular'] = '(11) 9 5455 62076';
        $data['data']['user_id'] = '10';*/

        $data['tipo'] = '2';
        $data['nome'] = 'Igor Carlos da Silva';
        $data['email'] = 'igorcarlos1997@hotmail.com';
        $data['segmento'] = 'Ferragem';
        $data['site'] = 'site.igorcarlos.com.br';
        $data['telefone'] = '(11) 4168-4502';
        $data['cidade'] = 'Barueri';
        $data['estado'] = 'SP';
        $data['cargo'] = 'Programador';
        $data['celular'] = '(11) 9 5455 62076';
        $data['user_id'] = '10';

        $enviar = Mail::send(['html' => 'site.emails.imprensa'], ['data' => $data], function($m){
            //$config = configuracoes::findOrFail(3);

            $m->from('igor@ph2.com.br', 'imab');
            //$m->to($config->email)->subject('contato pelo site');
            $m->to('igorcarlos1997@gmail.com','imab')->subject('contato pelo site');
            $m->to('igor@ph2.com.br','imab')->subject('contato pelo site');

        });


        //return view("site.emails.imprensa", $data);

    }


    public function buscar(Request $request){

        $dados = [];

        $dados['pesquisa'] = $request->input("busca");

        $dados['upDir'] = Dicas::$diretorio_imagens;
        $dados['dicas'] = DB::table('dicas')
            ->join('categoria_dicas', 'dicas.categoria', '=','categoria_dicas.id')->where('categoria_dicas.status','1')->where('dicas.status','1')->where('dicas.titulo', 'like', "%{$request->input('busca')}%")->orWhere('dicas.conteudo', 'like', "%{$request->input('busca')}%")->orWhere('dicas.resumo', 'like', "%{$request->input('busca')}%")
            ->select('dicas.*','categoria_dicas.status as categoriastatus', 'categoria_dicas.nome as nomecategoria')->orderBy('dicas.created_at','DESC')->paginate(3);

        $dados['quantidade'] =  sizeOf($dados['dicas']);

        $dados['categorias'] = categoria::orderBy('created_at','DESC')->where('status','1')->get();
        $dados['recentes'] = Dicas::orderBy('created_at','DESC')->take(8)->get();

        return view('site.busca', $dados);
    }

}
