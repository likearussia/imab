<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\usuario_site;
use DB;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Mail;
use App\models\configuracoes;
use Illuminate\Http\RedirectResponse;

class usuarioController extends Controller {

    protected $codigo;

    function setCod($val){
        $this->codigo = $val;
    }

    function getCod(){
        return $this->codigo;
    }

    public function perfil() {

        $dados['page'] = 'Usuário';
        $dados['subpage'] = 'Perfil';

        Auth::guard('usuarios_site')->check();
        $dados['usuario'] = usuario_site::findOrFail(Auth::guard('usuarios_site')->getUser()->id);

        return view('site.usuario.index', $dados);
    }

    public function index() {
        return redirect(route('usuario::perfil'));
    }

    public function cadastrar(Request $request) {

        //print_r($request->input());

        if ($request->input('tipo_usuario') == 1) {
            $this->validate($request, [
                'name' => 'required',
                'username' => 'required|confirmed',
                'password' => 'required|confirmed|max:12|min:6',
                'cidade' => 'required'
                    ], [
                'required' => 'O campo :attribute é obrigatório',
                'confirmed' => 'A confirmação de :attribute não confere'
                    ], [
                'username' => 'e-mail',
                'password' => 'senha',
                'name' => 'nome'
            ]);
        }

        if ($request->input('tipo_usuario') == 3) {
            $this->validate($request, [
                'name' => 'required',
                'username' => 'required|confirmed',
                'password' => 'required|confirmed|max:12|min:6',
                'cidade' => 'required',
                'segmento' => 'required',
                'cnpj' => 'required',
                'razao_social' => 'required',
                'fantasia' => 'required',
                'telefone' => 'required',
                'profissao' => 'required',
                'celular' => 'required'
                    ], [
                'required' => 'O campo :attribute é obrigatório',
                'confirmed' => 'A confirmação de :attribute não confere'
                    ], [
                'username' => 'e-mail',
                'password' => 'senha',
                'name' => 'nome',
                'celular' => 'Telefone / Celular',
                'profissao' => 'cargo'
            ]);
        }

        if ($request->input('tipo_usuario') == 2) {
            $this->validate($request, [
                'name' => 'required',
                'username' => 'required|confirmed',
                'password' => 'required|confirmed|max:12|min:6',
                'cidade' => 'required',
                'segmento' => 'required',
                'fantasia' => 'required',
                'telefone' => 'required',
                'profissao' => 'required',
                'celular' => 'required'
                    ], [
                'required' => 'O campo :attribute é obrigatório',
                'confirmed' => 'A confirmação de :attribute não confere'
                    ], [
                'username' => 'e-mail',
                'password' => 'senha',
                'name' => 'nome',
                'celular' => 'Telefone / Celular',
                'profissao' => 'cargo'
            ]);
        }

        //print_r($request->input());

        $users = DB::table('usuarios_site')->where('email', $request->input('username'));

        if ($users->count() > 0) {
            echo '3';
        } else {

            $this->salvar($request);
        }
    }

    public function editar(Request $request, $id) {

        $user = usuario_site::findOrFail($id);

        if ($user['email'] == $request->input('username')) {
            $qtd = 0;
        } else {
            $qtd = usuario_site::orderBy('created_at', 'DESC')->where('email', $request->input('username'))->count();
        }

        if ($qtd > 0) {
            flash()->error("Este e-mail já esta em uso.");
            return redirect()->back()->withInput();
        } else {

            $retorno = $this->salvar($request, $id);

            if($retorno instanceof RedirectResponse){
                return $retorno;
            }

        }
    }

    public function alterarSenha(Request $request, $id) {

        $user = usuario_site::findOrFail($id);

        $this->validate($request, [
            'newpassword' => 'required|confirmed|max:12|min:6',
            'password' => 'required|max:12|min:6'
                ], [
            'required' => 'O campo :attribute é obrigatório',
            'confirmed' => 'A confirmação de :attribute não confere'
                ], [
            'password' => 'senha',
            'newpassword' => 'nova senha',
        ]);



        if (Hash::check($request->input('password'), $user['password'])) {

            $input['password'] = bcrypt($request->input('newpassword'));

            $user->update($input);

            flash()->success("Senha alterada com sucesso.");
            return redirect(route('usuario::perfil'));
        } else {

            flash()->error("A senha atual não confere");
            return redirect()->back()->withInput();
        }
    }

    public function salvar(Request $request, $id = null) {

        if ($id) {
            $usuario = usuario_site::findOrFail($id);
            $input["tipo"] = $request->input("tipo_usuario");
        } else {
            $usuario = null;
            $input['password'] = Hash::make($request->input('password'));

            switch ($request->input('tipo_usuario')) {
                case 1:
                    $input['tipo'] = 1;
                    $input['status'] = 1;
                    break;
                case 3:
                    $input['tipo'] = 3;
                    $input['status'] = 0;
                    break;
                case 2:
                    $input['tipo'] = 2;
                    $input['status'] = 0;

                    break;
            }
            unset($input['_token']);
        }

        $input['nome'] = $request->input('name');
        $input['email'] = $request->input('username');
        $input['cargo'] = $request->input('profissao');
        $input['estado'] = $request->input('estado');
        $input['cidade'] = $request->input('cidade');

        if ($input['tipo'] != 3 || $input['tipo'] != 2) {
            
            $input['segmento'] = $request->input('segmento');
            $input['nome_fantasia'] = $request->input('fantasia');
            $input['telefone'] = $request->input('telefone');
            $input['site'] = $request->input('site');
            $input['celular'] = $request->input('celular');
        
        }


        if ($input['tipo'] == 3) {

            $input['cnpj'] = $request->input('cnpj');
            $input['razao_social'] = $request->input('razao_social');
        }



        if ($usuario) {
            try {


                $usuario->update($input);

                flash()->success("Usuário alterado com sucesso");

                return redirect(route('usuario::perfil'));


            } catch (\Illuminate\Database\QueryException $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao atualizar usuário" . $exception->getMessage());
                return redirect()->back()->withInput();
                echo 2;
            }
        } else {
            try {
                $usr = usuario_site::create($input);
                flash()->success("Usuário cadastrado com sucesso");

                $input['user_id'] = $usr->id;

                if ($input['tipo'] == 3 || $input['tipo'] == 2) {
                    $this->sendMail($input);
                }

                echo 1;
            } catch (\Illuminate\Database\QueryException $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao atualizar usuário" . $exception->getMessage());

                echo 2;
            }
        }

    }

    public function logar(Request $request) {

        $email = $request->input('username');
        $senha = $request->input('password');



        if (Auth::guard('usuarios_site')->attempt(array('email' => $email, 'password' => $senha))) {
            $user = usuario_site::where('email', $email)->first()->toArray();
            Auth::guard('usuarios_site')->loginUsingId($user['id']);

            $error['mensagem'] = 'sucesso';
        } else {

            $error['mensagem'] = "Usuário não encontrado.";
        }

        return response()->json($error);
    }

    public function logout() {
        Auth::guard('usuarios_site')->logout();

        return redirect(route('site::index'));
    }

    public function formEditar() {



        $dados['page'] = 'Usuário';
        $dados['subpage'] = 'Editar';
        return view('site.usuario.editar', $dados);
    }

    public function arquivosDigitais() {
        return view('site.usuario.arquivosdigitais');
    }


    public function sendMail($data){


       try{
           $enviar = Mail::send(['html' => 'site.emails.imprensa'], ['data' => $data], function($m){

               $config = configuracoes::findOrFail(5);

               $m->from('igor@ph2.com.br', 'imab');
               $m->to($config->email)->subject('contato pelo site');
               $m->to('igorcarlos1997@gmail.com','imab')->subject('contato pelo site');
               //$m->to('keyla@ph2.com.br','imab')->subject('contato pelo site');

           });

       }catch (Exception $ex){
            echo $ex;
       }
    }

    protected function recuperar_senha(Request $request){
        $selUser = DB::table('usuarios_site')->where([
            ['email', $request->input("username")],
            ['deleted_at', null],
        ])->first();

        $this->setCod($selUser->email);

        if($selUser){
            if($selUser->deleted_at){

            }else{
                $data['codigo'] = base64_encode($selUser->id);
                try{
                    $enviar = Mail::send(['html' => 'site.emails.recuperar_senha'], ['data' => $data], function($m){
                        $m->from('contato@imab.com.br', 'IMAB');
                        $m->to($this->getCod())->subject('Recuperar senha');
                    });

                    echo "1";
                }catch (Exception $ex){
                    echo $ex;
                }
            }
        }else{
            echo "2";
        }
    }

    protected function novasenha(Request $request, $id=''){
        if($request->isMethod('post')){
            $userId = base64_decode($request->input('id'));

            $user = usuario_site::findOrFail($userId);

            $this->validate($request, [
                'password' => 'required|confirmed'
            ],[
                'required' => 'O campo :attribute é obrigatório.'
            ], [
                'password' => 'senha'
            ]);

            $input['password'] = bcrypt($request->input('password'));

            try{
                $user->update($input);
                flash()->success("Senha alterada com sucesso");
            }catch (\Illuminate\Database\QueryException $ex){
                flash()->error("Erro ao alterar a senha");
            }

            echo "1";

        }else{
            $data['id'] = $id;
            return view('site.usuario.recuperar', $data);
        }

    }
    
}
