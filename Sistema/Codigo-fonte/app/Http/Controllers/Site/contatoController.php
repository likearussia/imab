<?php

namespace App\Http\Controllers\Site;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\configuracoes;
use Illuminate\Support\Facades\DB;

class contatoController extends Controller {

    public function index(){
        return view('site.faleconosco');
    }

    public function enviar(Request $request) {

        $data = [];

        $data['form'] = $request->input();

        $this->validate($request, [
            'segmento' => 'required',
            'nome' => 'required',
            'email' => 'required',
            'cep' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'estado' => 'required',
            'cidade' => 'required',
            'telefone' => 'required',
            'atividade' => 'required',
        ]);

        $data['segmento'] = $request->input('segmento');
        $data['nome'] = $request->input('nome');
        $data['telefone'] = $request->input('telefone');
        $data['email'] = $request->input('email');
        $data['cep'] = $request->input('cep');
        $data['endereco'] = $request->input('endereco');
        $data['numero'] = $request->input('numero');
        $data['complemento'] = $request->input('complemento');
        $data['bairro'] = $request->input('bairro');
        $data['estado'] = $request->input('estado');
        $data['cidade'] = $request->input('cidade');
        $data['atividade'] = $request->input('atividade');
        $data['conheceu'] = $request->input('conheceu');
        $data['mensagem'] = $request->input('mensagem');

        if($request->input("news") == "on"){
            DB::table('newsletter')->insert([
                ['nome' => $data['nome'], 'email' => $data['email'], 'telefone' => $data['telefone']]
            ]);
        }else{
        }

        $enviar = Mail::send(['html' => 'site.emails.contato'], ['data' => $data], function($m){
            $config = configuracoes::findOrFail(3);
            $m->from('igor@ph2.com.br', 'imab');
            $m->to($config->email)->subject('contato pelo site');
        });

        if($enviar){
            flash()->success("Mensagem enviada com sucesso!");
            return redirect(Route("site::fale-conosco"));
        }else{
            flash()->error("Erro ao enviar mensagem!");
            return back()->withInput();
        }
    }

}
