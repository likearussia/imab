<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\download;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class downloadController extends Controller {

    public function listar() {
        Auth::guard('usuarios_site')->check();
        if (!isset(Auth::guard('usuarios_site')->getUser()->tipo) || Auth::guard('usuarios_site')->getUser()->tipo == 1) {
            $dados['downloads'] = download::orderBy('created_at', 'desc')->where('usuarios', 'like', '%1%')->paginate(10);
        } else if (Auth::guard('usuarios_site')->getUser()->tipo == 2) {

            $dados['downloads'] = download::orderBy('created_at', 'desc')->where('usuarios', 'like', '%2%')->orWhere('usuarios', 'like', '%1%')->get();

        } else if (Auth::guard('usuarios_site')->getUser()->tipo == 3) {
            $dados['downloads'] = download::orderBy('created_at', 'desc')->where('usuarios', 'like', '%3%')->orWhere('usuarios', 'like', '%1%')->get();
        }

        return view('site.usuario.arquivosdigitais', $dados);
    }

    public function download($arquivo) {
        /*
         * Verifica se o arquivo existe para então poder fazer o download sem que o sistema retorne nenhum tipo de erro
         */
        if (file_exists(public_path() . '/downloads/' . $arquivo)) {
            //Caso o arquivo exista, faz o download
            return Response()->download(public_path() . '/downloads/' . $arquivo);
        } else {
            //caso não exista, retorna um erro
            flash()->error("Arquivo danificado ou não encontrado, tentar novamente mais tarde.");
            return redirect()->back()->withInput();
        }
    }


}
