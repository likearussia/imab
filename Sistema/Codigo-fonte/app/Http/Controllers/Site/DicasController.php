<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Dicas;
use App\models\categoria;
use Illuminate\Support\Facades\DB;

class DicasController extends Controller
{
    public function index(){
        $dados = [];
        
        /*$dados['dicas'] = Dicas::orderBy('created_at','DESC')->where('status','1');
        $dados['categorias'] = categoria::orderBy('creadet_at','DESC')->where('status','1');*/
        
        $dados['upDir'] = Dicas::$diretorio_imagens;
        $dados['dicas'] = DB::table('dicas')
                ->join('categoria_dicas', 'dicas.categoria', '=','categoria_dicas.id')->where('categoria_dicas.status','1')->where('dicas.status','1')
                ->select('dicas.*','categoria_dicas.status as categoriastatus', 'categoria_dicas.nome as nomecategoria')->orderBy('dicas.created_at','DESC')->paginate(3);

        $dados['categorias'] = categoria::orderBy('created_at','DESC')->where('status','1')->get();
        $dados['recentes'] = Dicas::orderBy('created_at','DESC')->take(8)->get();


        $dados['quantidade'] =  sizeOf($dados['dicas']);


        return view('site.dicasesolucoes', $dados);
    }
    
    public function visualizar($slug, $id){
        $dados['upDir'] = Dicas::$diretorio_imagens;
        $dados['dicas'] = Dicas::findOrFail($id);
        $dados['categoria'] = categoria::findOrFail($dados['dicas']->categoria);
        
        return view('site.single-dica', $dados);
        
    }


    public function buscar(Request $request){
        //print_r($request->input());

        $dados = [];

        $dados['upDir'] = Dicas::$diretorio_imagens;
        $dados['dicas'] = DB::table('dicas')
            ->join('categoria_dicas', 'dicas.categoria', '=','categoria_dicas.id')->where('categoria_dicas.status','1')->where('dicas.status','1')->where('dicas.titulo', 'like', "%{$request->input('busca')}%")->orWhere('dicas.conteudo', 'like', "%{$request->input('busca')}%")->orWhere('dicas.resumo', 'like', "%{$request->input('busca')}%")
            ->select('dicas.*','categoria_dicas.status as categoriastatus', 'categoria_dicas.nome as nomecategoria')->orderBy('dicas.created_at','DESC')->paginate(3);


        $dados['quantidade'] =  sizeOf($dados['dicas']);

        $dados['categorias'] = categoria::orderBy('created_at','DESC')->where('status','1')->get();
        $dados['recentes'] = Dicas::orderBy('created_at','DESC')->take(8)->get();

        return view('site.dicasesolucoes', $dados);
    }



}
