<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Institucional;
use Illuminate\Support\Facades\DB;

class InstitucionaisController extends Controller {

    
    /*
     * As paginas institucionais serão listadas a partir de seus ids:
     * |----------------------------|
     * | ID |       PÁGINA          |
     * |----------------------------|
     * |----------------------------|
     * |----------------------------|
     * | 01 |  VALORES HUMANOS      |
     * |----------------------------|
     * | 02 | INOVAÇÃO E QUALIDADE  |
     * |----------------------------|
     * | 03 | CIDADANIA CORPORATIVA |
     * |----------------------------|
     * | 04 |      HISTÓRIA         |
     * |----------------------------|
     * 
     * OBS01: As páginas institucionais não são escaláveis, logo para adicionar 
     * uma nova é preciso fazer um insert padrão no banco e editar a mesma no 
     * admin, adicionando também uma rota e uma função neste controller.
     * 
     * OBS02: Á página de historia tem uma página administrativa e uma exibição 
     * diferente, portanto deve ser sempre alocada com o id de numero 04.
     *        
     */
    
    public function valoresHumanos() {
        $dados = [];
        
        $dados['valores'] = Institucional::findOrFail(1);
        
        return view('site/valoreshumanos', $dados);
    }

    
     public function inovacao(){
        $dados = [];
        
        $dados['inovacao'] = Institucional::findOrFail(2);
        
        return view('site/inovacaoequalidade', $dados);
    }
    
    public function cidadaniaCorporativa(){
        $dados = [];
        
        $dados['cidadania'] = Institucional::findOrFail(3);
        
        return view('site/cidadaniacorporativa', $dados);
    }
    
    
    public function historia(){
        $dados = [];
        
        $dados['historia'] = Institucional::findOrFail(4);
        
        //$dados['timeline'] = json_decode($dados['historia']['timeline']);
        
        //$dados['qtd'] = sizeof($dados['timeline'][0]->titulo);

        $dados['i'] = 0;
        $dados['timeline'] = DB::table('timeline')->get();


        return view('site/historia', $dados);
    }
    
    
    
    
}
