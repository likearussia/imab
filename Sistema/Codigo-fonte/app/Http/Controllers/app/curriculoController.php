<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\curriculo;

class curriculoController extends Controller {

    public function index() {

        $dados['curriculo'] = curriculo::orderBy('created_at', 'DESC')->paginate(10);

        return view('app.curriculo.home', $dados);
    }
    
    public function visualizar($id){
        $curriculo = curriculo::findOrFail($id);
        
        $dados['dadospessoais'] = $curriculo;
        
        $dados['espanhol'] = json_decode($dados['dadospessoais']->idiomas)->espanhol;
        $dados['ingles'] = json_decode($dados['dadospessoais']->idiomas)->ingles;
        
        return view('app.curriculo.visualizar', $dados);
    }

}
