<?php
namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Institucional;
use Image;
use File;
use Illuminate\Support\Facades\DB;

class InstitucionalController extends Controller {

    protected function index() {
        $dados = [];
        $dados['institucionais'] = Institucional::orderBy('created_at', 'DESC')->paginate(10);
        $dados['page'] = "Institucionais";
        return view('app.institucional.index', $dados);
    }

    protected function editar(Request $request, $id) {
        $dados = [];
        $dados['page'] = "Institucional";
        $dados['institucional'] = Institucional::findOrFail($id);
        if ($id == 4) {
            $dados['timeline'] = DB::table('timeline')->get();
        }
        // Setando a vartiavel $input a ser passada para update
        $input = $request->input();
        //verificando se o metodo usado para acessar o metodo é get ou post
        if ($request->isMethod('post')) {
            if ($id == 4) {
                DB::table('timeline')->truncate();
                //setando a variavel $liha para montar o json da timeline
                $linha = $input['linha'];
                //setando a varivel $image para  receber as imagens e aplicar no json
                $image = [];
                //verifica se existe uploads para ser feito
                if ($request->hasFile('timeImage')) {
                    $i = 0;
                    //percorre os arquivos para fazer upload
                    foreach ($request->file('timeImage') as $img) {
                        if (!$img) {
                            $newName = $input['timeUpdate'][$i];
                        } else {
                            $imagem = $img;
                            $newName = sha1(str_random('20')) . '.' . $imagem->guessClientExtension();
                            $img->move(public_path('imagens/upload/timeline'), $newName);
                        }
                        $image[] = $newName;
                        $i++;
                    }
                } else {
                    $i = 0;
                    //percorre os arquivos para fazer upload
                    foreach ($input['timeUpdate'] as $newName) {
                        $image[] = $newName;
                        $i++;
                    }
                }


                
                //$timeline[] = $request->input('linha');
                //$timeline[]['imagem'] = $image;
                //print_r($timeline);
                //$timelineJson = json_encode($timeline);
                //$input['timeline'] = $timelineJson;

                $linha = $request->input("linha");
                $qtd = sizeof($linha['titulo']);


                for ($i=0; $i < $qtd; $i++){
                    DB::table('timeline')->insert(
                        ['titulo' => $linha['titulo'][$i], 'ano' => $linha['ano'][$i], 'texto' => $linha['texto'][$i],  'caminho' => $image[$i]]
                    );
                    //echo $value['titulo'][$key];
                }

            }
            $width = intval($input['wimage']);
            $height = intval($input['himage']);
            $x = intval($input['ximage']);
            $y = intval($input['yimage']);
            if (!empty($input['simage'])) {
                $finalName = str_replace("tmp_uploads/", "", $input['simage']);
                $img = Image::make(public_path() . '/' . $input['simage'])->crop($width, $height, $x, $y)->save(public_path() . '/imagens/upload/' . 'institucionais/' . $finalName);
                $input['imagem_destaque'] = $finalName;
                #TODO - excluir umagem da pasta tmp

                File::delete(public_path() . '/' . $input['simage']);

            }
            $input['slug'] = $this->gerarSlug($input['titulo']);
            unset($input['_token']);
            try {
                $dados['institucional']->update($input);


                //$resposta["mensagem"] = "Institucional alterada com sucesso";
                //$resposta["url_to"] = route('app::institucional::index');
                //echo json_encode($resposta);

                flash()->success("Institucional alterada com sucesso.");
                return redirect(route('app::institucional::index'));

            } catch (Exception $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao alterar. Detalhes: " . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        } else {
            return view('app.institucional.editar', $dados);
        }
    }

    protected function gerarSlug($nome = null) {
        if ($nome) {
            $slug_gerada = str_slug($nome, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        return $slug;

    }



}
