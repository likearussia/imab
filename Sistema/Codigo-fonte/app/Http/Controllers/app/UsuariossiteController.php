<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\usuario_site;

class UsuariossiteController extends Controller {

    public function index() {

        $dados['usuarios'] = usuario_site::orderBy('created_at', 'desc')->paginate(10);

        return view('app.usuarios-site.index', $dados);
    }

    public function mudarTipo($id, $para) {
        $usuario = usuario_site::findOrFail($id);

        $input['tipo'] = $para;

        try {
            $usuario->update($input);
            echo '1';
        } catch (\Illuminate\Database\QueryException $ex) {
            echo '0';
        }
    }

    public function status($id, $novoStatus) {

        $usuario = usuario_site::findOrFail($id);

        $input['status'] = $novoStatus;

        try {
            $usuario->update($input);
            flash()->success("status modificado com sucesso");
        } catch (\Illuminate\Database\QueryException $ex) {
            $exception = $ex->getPrevious();
            flash()->error("Erro ao cadastrar o banner. Detalhes" . $exception->getMessage());
        }

        return redirect(route('app::usuarios-site::index'));
    }

    public function deletar($id) {
        $usuario = usuario_site::findOrFail($id);

        
        try {
            $usuario->delete();
            flash()->success("Usuário excluido com sucesso.");
        } catch (\Illuminate\Database\QueryException $ex) {
            $exception = $ex->getPrevious();
            flash()->error("Erro ao cadastrar o banner. Detalhes" . $exception->getMessage());
        }

        return redirect(route('app::usuarios-site::index'));
    }

}
