<?php
namespace App\Http\Controllers\app;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\categoria;
use App\models\Dicas;
use PhpParser\Node\Stmt\Echo_;

class ControllerCategoria extends Controller {

    protected function index() {
        $dados = [];
        $lista = categoria::orderBy('created_at', 'DESC')->paginate(10);
        $dados['page'] = 'Dicas e Soluções';
        $dados['subpage'] = 'Categorias';
        $dados['lista'] = $lista;
        return view('app.dicas.categorias.index', $dados);
    }

    protected function criar(Request $request) {
        $dados = [];

        $categorias = categoria::orderBy('created_at', 'DESC');

        if ($request->isMethod('post')) {

            $this->validar($request);
            $input = $request->input();
            unset($input['_token']);
            $input['status'] = 1;
            $input['slug'] = $this->gerarSlug($input['nome']);
            $categorias = $categorias->where('nome', $input['nome']);
            if ($categorias->count() == 0) {
                categoria::create($input);
                $resposta["mensagem"] = "Categoria cadastrada com sucesso";
                $resposta["url_to"] = route('app::dicas::categoria::index');
                echo json_encode($resposta);
                //flash()->success("Categoria criada com sucesso");
                //return redirect(route('app::dicas::categoria::index'));

            } else {
                echo 1;
            }
        } else {
            $dados['page'] = 'Dicas e Soluções';
            $dados['subpage'] = 'Categorias';
            return view('app.dicas.categorias.criar', $dados);
        }
    }

    protected function editar(Request $request, $id) {
        $dados = [];
        $categorias = categoria::findOrFail($id);
        if ($request->isMethod('post')) {
            $this->validar($request);
            $input = $request->input();
            unset($input['_token']);
            $qtd = categoria::orderBy('created_at', 'DESC')->where('nome', $input['nome']);
            $input['slug'] = $this->gerarSlug($input['nome']);
            if ($qtd->count() == 0) {
                $categorias->update($input);

                $resposta['mensagem'] = "Categoria alterada com sucesso";
                $resposta['url_to'] = route('app::dicas::categoria::index');
                echo json_encode($resposta);
            } else {
                flash()->error("Esta categoria já existe, utilizar outro nome");
                return redirect()->back()->withInput();
            }
        } else {
            $dados['page'] = 'Dicas e soluções';
            $dados['subpage'] = 'Categorias';
            $dados['lista'] = $categorias;
            return view('app.dicas.categorias.editar', $dados);
        }
    }

    
    public function changeStatus($id, $status){
        $categoria = categoria::findOrFail($id);
        $input['status'] = $status;
        $categoria->update($input);
        flash()->success("Categoria alterada com sucesso");
        return redirect(route('app::dicas::categoria::index'));
    }
    
    protected function validar(Request &$request) {
        $this->validate($request, [
            'nome' => 'required',
        ]);
    }

    protected function gerarSlug($nome = null) {
        if ($nome) {
            $slug_gerada = str_slug($nome, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        return $slug;
    }
}
