<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\download;
use Symfony\Component\HttpFoundation\Response;
use File;

class downloadController extends Controller {

    public function index() {
        $dados['downloads'] = download::orderBy('created_at', 'desc')->paginate(10);
        return view('app.download.index', $dados);
    }

    public function criar(Request $request) {
        //Verifica se metodo de entrada é get ou post
        if ($request->isMethod('post')) {
            /*
             * Define regra de validação para os campos obrigatórios 
             * A principio foi decidido não fazer validação para tamanho de arquivo.
             */
            $this->validate($request, [
                'titulo' => 'required',
                'type' => 'required',
                'caminho' => 'required'
                    ], [
                'required' => 'O campo :attribute é obrigatório',
                    ], [
                'titulo' => 'título',
                'type' => 'usuários',
                'caminho' => 'imagem'
            ]);
            /*
             * Pega os dados passados via post pelo Request
             */
            $input = $request->input();
            /*
             * Pega o arquivo por meio do request->file()
             */
            $arquivo = $request->file('caminho');
            /*
             * Define um nome randomico pro arquivo (para que não se repita dentro do banco de dados)
             */
            $fileName = sha1(str_random('20')) . '.' . $arquivo->guessClientExtension();
            /*
             * Move o arquivo para a pasta de downloads dentro do /public 
             */
            $request->file('caminho')->move(public_path(download::$diretorio_imagens), $fileName);
            /*
             * 
             * Define a variavel caminho dentro do array $input
             * 
             * Define a variavel nome dentro do array input (esse campo ira guardar o nome original do arquivo para apresentação ao usuário)
             * 
             */
            $input['caminho'] = $fileName;
            $input['nome'] = $arquivo->getClientOriginalName();
            /*
             * Monta um json com os tipos de usuários permitidos a baixarem o conteudo
             */
            $users = json_encode($input['type']);
            $input['usuarios'] = $users;
            /*
             * 
             * Executa um try catch para fazer a inserção do dado no banco
             * 
             */
            try {
                // Cria o download no banco de dados
                download::create($input);
                //emite mensagem de sucesso
                $resposta["mensagem"] = "Download criado com sucesso.";
                $resposta["url_to"] = route('app::download::index');
                echo json_encode($resposta);
                //redireciona para a listagem de downloads
                //return redirect(route('app::download::index'));
            } catch (\Exception $ex) {
                echo 0;
            }
        } else {
            //caso a requisição seja do tipo get, retorna a view de cadastro do download

            $dados['page'] = 'Downloads';

            $dados['subpage'] = 'Criar';
            return view('app.download.criar', $dados);
        }
    }

    public function editar(Request $request, $id) {
        #todo = criar função salvar para evitar refação de codigo
        /*
         * define a variavel de $input, array que guarda os campos a irem para o banco de dados
         */
        $input = $request->input();
        /*
         * Seleciona no banco, o download que foi requisitado via id.
         */
        $download = download::findOrFail($id);
        //verifica se o metodo requisitado é get ou post
        if ($request->isMethod('post')) {
            /*
             * Valida os campos alterados
             */
            $this->validate($request, [
                'titulo' => 'required',
                'type' => 'required',
                    ], [
                'required' => 'O campo :attribute é obrigatório',
                    ], [
                'titulo' => 'título',
                'type' => 'usuários'
            ]);
            // Verifica se houve alteração do arquivo (caso tenha alteração faz um novo upload, e apaga o arquivo anterior)
            if ($request->file() == null) {
                $input['caminho'] = $request->input("src_anterior");

            } else {
                /*
                 * Apaga o arquivo anterior
                 */
                File::delete(public_path(download::$diretorio_imagens) . '/' . $download['caminho']);
                /*
                 * Aqui é executada a mesma coisa que no upload do metodo criar()
                 */
                $arquivo = $request->file('caminho');
                $fileName = sha1(str_random('20')) . '.' . $arquivo->guessClientExtension();
                $request->file('caminho')->move(public_path(download::$diretorio_imagens), $fileName);
                $input['nome'] = $arquivo->getClientOriginalName();
                $input['caminho'] = $fileName;

            }
            $users = json_encode($input['type']);
            $input['usuarios'] = $users;
            try {
                $download->update($input);
                $resposta["mensagem"] = "Download alterado com sucesso.";
                $resposta["url_to"] = route('app::download::index');
                echo json_encode($resposta);
                //return redirect(route('app::download::index'));
            } catch (\Exception $ex) {
                echo 0;
            }
        } else {

            $dados['page'] = 'Downloads';
            $dados['subpage'] = 'Editar';
            $dados['download'] = download::findOrFail($id);
            $dados['usuarios'] = json_decode($dados['download']->usuarios);
            return view('app.download.editar', $dados);
        }
    }

    public function download($arquivo) {
        /*
         * Verifica se o arquivo existe para então poder fazer o download sem que o sistema retorne nenhum tipo de erro
         */
        if (file_exists(public_path() . '/downloads/' . $arquivo)) {
            //Caso o arquivo exista, faz o download
            return Response()->download(public_path() . '/downloads/' . $arquivo);
        } else {
            //caso não exista, retorna um erro
            flash()->error("Arquivo danificado ou não encontrado, tentar novamente mais tarde.");
            return redirect()->back()->withInput();
        }
    }

    public function deletar($id) {
        //resgata o registro no banco pelo ID
        $download = download::findOrFail($id);
        //Apaga o arquivo da pasta
        File::delete(public_path(download::$diretorio_imagens) . '/' . $download['caminho']);
        //apaga o registro
        $download->delete();
        //retorna mensagem
        flash()->success("Arquivo deletado com sucesso");
        return redirect(route('app::download::index'));
    }

}
