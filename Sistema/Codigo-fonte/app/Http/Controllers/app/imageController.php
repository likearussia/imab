<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class imageController extends Controller {

    protected function UpImagem(Request $request) {

        $imagem = $request->file('caminho');
        $nome_imagem = sha1(str_random('20')) . '.' . $imagem->guessClientExtension();
        $request->file('caminho')->move(public_path('tmp_uploads'), $nome_imagem);
        echo $nome_imagem;
    }

}
