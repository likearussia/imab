<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Usuario;

class UsuarioController extends Controller {

    protected function index() {

        $dados = [];

        $dados['usuarios'] = Usuario::orderBy('created_at', 'DESC')->paginate(10);
        $dados['page'] = "Usuários";

        return view('app.usuario.index', $dados);
    }

    protected function criar(Request $request) {
        if ($request->isMethod('post')) {
            //print_r($request->input());

            //$input = $request->input();

            $this->validate($request, [
                'nome' => 'required|max:50',
                'email' => 'required|unique:usuarios|max:100',
                'senha' => 'required|min:5|confirmed',
            ]);


            $input['nome'] = $request->input("nome");
            $input['email'] = $request->input("email");
            $input['password'] = bcrypt($request->input('senha'));
            Usuario::create($input);
            $resposta['mensagem'] = "Usuário criado com sucesso";
            $resposta['url_to'] = route('app::usuario::index');
            echo json_encode($resposta);

         } else {

            $dados = [];
            $dados['page'] = 'Usuários';

            return view('app.usuario.criar', $dados);
        }
    }

    protected function editar(Request $request, $id) {

        $dados = [];
        $dados['usuario'] = Usuario::findOrFail($id);

        if ($request->isMethod('post')) {

            $input['nome'] = $request->input("nome");
            $input['email'] = $request->input("email");

            if ($request->has('senha')) {

                $this->validate($request, [
                    'nome' => 'required|max:100',
                    'email' => 'required|max:100',
                    'senha' => 'required|min:5|confirmed'
                ]);

                $input['password'] = bcrypt($request->input('senha'));
            }
            $dados['usuario']->update($input);

            $resposta['mensagem'] = "Usuário alterado com sucesso";
            $resposta['url_to'] = route('app::usuario::index');
            echo json_encode($resposta);
        } else {
            $dados['page'] = "Usuários";

            return view('app.usuario.editar', $dados);
        }
    }

    protected function deletar($id) {
        $quantidade = Usuario::orderBy('created_at', 'DESC')->count();
        if ($quantidade == 1) {
            flash()->error("É necessário ter ao menos um usuário cadastrado");
            
            return redirect()->back();
        } else {
            $usuario = Usuario::findOrFail($id);
            $usuario->delete();

            flash()->success("Usuário deletado com sucesso");

            return redirect()->back();
        }
    }



}
