<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\configuracoes;

class configuracaoController extends Controller
{
    public function index(){

        $dados["configuracoes"] = configuracoes::orderBy('created_at')->paginate(10);
        return view("app.configuracoes.index", $dados);
    }

    public function editar(Request $request, $id){
        if($request->isMethod('post')){

            $configuracao = configuracoes::findOrFail($id);
            $input['email'] = $request->input("email");

            $configuracao->update($input);

            $resposta["mensagem"] = "Configuração alterada com sucesso";
            $resposta["url_to"] = route('app::configuracoes::index');
            echo json_encode($resposta);


        }else{
            $dados["configuracao"] = configuracoes::findOrFail($id);
            return view("app.configuracoes.editar", $dados);
        }
    }
}
