<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class newsletterController extends Controller
{
    protected function index(){
        $dados['newsletter'] = DB::table('newsletter')->paginate(10);
        return view('app.newsletter.index', $dados);
    }


    protected function apagar($id){

        $var = DB::table('newsletter')->where('id', $id)->delete();

        if($var == 1){
            flash()->success("Contato apagado com sucesso.");
        }else{
            flash()->error("Erro, tente novamente mais tarde");
        }

        return redirect(route('app::news::index'));
    }
}
