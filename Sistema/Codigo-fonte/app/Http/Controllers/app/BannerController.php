<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Banner;
use Image;
use File;

class BannerController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $allBanners = Banner::orderBy('created_at', 'DESC')->paginate(10);
        $dados['page'] = 'Banner';
        $dados['banners'] = $allBanners;
        return view('app.banner.index', $dados);
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $dados['page'] = 'Banner';
        $dados ['subpage'] = 'Criar';
        return view('app.banner.criar', $dados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function salvar(Request $request, $id = null) {

        if ($id) {
            $banner = Banner::findOrFail($id);
            $this->validar($request);
        } else {
            $banner = null;
            $this->validate($request, [
                'titulo' => 'required',
                'simage' => 'required'
            ], [
                'required' => 'O campo :attribute é obrigatório',
            ], [
                'titulo' => 'Titulo',
                'simage' => 'imagem'
            ]);
        }
        $input = $request->input();
        //Remove dados que não serão usados diretamente pelos métodos create e update do model Post
        unset($input['_token']);
        // Lida com as imagens
        // #TODO deletar imagens antigas e cujo processo de salvamento do banner falhou

        $width = intval($input['wimage']);
        $height = intval($input['himage']);
        $x = intval($input['ximage']);
        $y = intval($input['yimage']);
        if (!empty($input['simage'])) {
            $finalName = str_replace("tmp_uploads/", "", $input['simage']);
            $img = Image::make(public_path() . '/' . $input['simage'])->crop($width, $height, $x, $y)->save(public_path() . '/imagens/upload/' . 'banners/' . $finalName);
            $input['caminho'] = $finalName;
            #TODO - excluir umagem da pasta tmp

            File::delete(public_path() . '/' . $input['simage']);
        }
        //Fazendo regra para saber se link é em nova guia ou não
        $input['nova_aba'] = ($request->has('nova_aba') ? true : false);
        $input['titulo'] = $request->input('titulo');
        //Atualizar ou criar o banner
        if ($banner) {
            try {
                $input['status'] = $request->input('status');
                $banner->update($input);
                //flash()->success("Banner atualizado com sucesso");
                $retorno["mensagem"] = "Banner atualizado com sucesso";
                $retorno["url_to"] = route('app::banner::index');
                echo json_encode($retorno);
            } catch (\Illuminate\Database\QueryException $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao atualizar banner. Detalhes: " . $exception->getMessage());
                //return redirect()->back()->withInput();
            }
        } else {
            try {
                $input['status'] = 0;
                $banner = Banner::create($input);

                $retorno["mensagem"] = "Banner criado com sucesso";
                $retorno["url_to"] = route('app::banner::index');

                echo json_encode($retorno);
            } catch (\Illuminate\Database\QueryException $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao cadastrar o banner. Detalhes" . $exception->getMessage());
                //return redirect()->back()->withInput();
            }
        }
        //return redirect(route('app::banner::index', $input));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id) {
        $dados['page'] = "Banner";
        $dados['subpage'] = "Editar";
        $dados['diretorio_imagens'] = Banner::$imageDir;
        $dados['banner'] = Banner::findOrFail($id);
        return view('app.banner.editar', $dados);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        $banner = Banner::findOrFail($id);
        $banner->delete();
        flash()->success('Banner deletado com sucesso');
        return redirect()->back();
    }

    public function changeStatus($id, $status) {
        $banner = Banner::findOrFail($id);
        $input['status'] = $status;
        $banner->update($input);
        flash()->success("Status alterado com sucesso");
        return redirect()->back()->withInput();
    }

    /*
     * Função para validar o tipo e tamanho da imagem, caso seja superior ele aborta 
     * a ação retornando um erro.
     */
    protected function validar(Request &$request) {
        $this->validate($request, [
            'titulo' => 'required',
                ], [
            'required' => 'O campo :attribute é obrigatório',
                ], [
            'titulo' => 'Titulo',
        ]);
    }

}
