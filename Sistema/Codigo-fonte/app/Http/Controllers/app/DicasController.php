<?php
namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\categoria;
use App\models\Dicas;
use File;
use Illuminate\Support\Facades\Storage;
use Image;


class DicasController extends Controller {

    public function index() {
        $dados = [];
        $dados['page'] = 'Dicas e soluções';
        $dados['dicas'] = Dicas::orderBy('created_at', 'DESC')->paginate(10);
        return view('app.dicas.index', $dados);
    }

    public function criar(Request $request) {
        $dados = [];
        if ($request->isMethod('post')) {
            $this->validar($request);
            $salvar = $this->salvar($request);

            $resposta["mensagem"] = "Postagem criada com sucesso";
            $resposta["url_to"] = route('app::dicas::index');
            echo json_encode($resposta);
        } else {
            $dados['page'] = 'Dicas e soluções';
            $dados['subpage'] = 'Criar';
            $dados['categorias'] = categoria::listar();
            return view('app.dicas.criar', $dados);
        }
    }
    
    public function editar(Request $request, $id){
        $dados = [];
        $dados['postagem'] = Dicas::findOrFail($id);
        if($request->isMethod('post')){
            $this->validar($request);
            $salvar = $this->salvar($request, $dados['postagem']);
            $resposta["mensagem"] = "Postagem alterada com sucesso";
            $resposta["url_to"] = route('app::dicas::index');
            echo json_encode($resposta);
        }else{
            $dados['page'] = "Dicas e Soluções";
            $dados['subpage'] = 'Editar';
            $dados['diretorio_imagens'] = Dicas::$diretorio_imagens;
            $dados['categorias'] = categoria::listar();
            return view('app.dicas.editar',$dados);
        }
    }
    
    public function changeStatus($id,$status){
        $postagem = Dicas::findOrFail($id);
        $input['status'] = $status;
        $postagem->update($input);
        flash()->success("Postagem alterada com sucesso");
        return redirect(route('app::dicas::index'));
    }

    public function salvar(Request $request, $postagem = null) {
        $input = $request->input();
        unset($input['_token']);
        $input['slug'] = $this->gerarSlug($input['titulo']);
        if ($request->hasFile('caminho')) {
            try {
                $imagem = $request->file('caminho');
                $nome_imagem = sha1(str_random('20')) . '.' . $imagem->guessClientExtension();
                $request->file('caminho')->move(public_path(Dicas::$diretorio_imagens), $nome_imagem);
                $input['imagem_destaque'] = $nome_imagem;
            } catch (\Exception $ex) {
                flash()->error("Erro: " . $ex->getMessage());
                return redirect()->back()->withInput();
            }
        }

        $width = intval($input['wimage']);
        $height = intval($input['himage']);
        $x = intval($input['ximage']);
        $y = intval($input['yimage']);

        if (!empty($input['simage'])) {
            $finalName = str_replace("tmp_uploads/", "", $input['simage']);
            $img = Image::make(public_path() . '/' . $input['simage'])->crop($width, $height, $x, $y)->save(public_path() . '/imagens/upload/' . 'posts/' . $finalName);
            $input['miniatura'] = $finalName;
            #TODO - excluir umagem da pasta tmp
            File::delete(public_path() . '/' . $input['simage']);
        }


        if ($postagem) {
            try {
                $postagem->update($input);
            } catch (QueryException $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao atualizar. Detalhes: " . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        } else {
            try {
                $postagem = Dicas::create($input);
            } catch (QueryException $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao cadastrar. Detalhes: " . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        }
    }

    public function validar(Request &$request) {
        $this->validate($request, [
            'titulo' => 'required',
            'caminho' => 'mimes:jpg,jpeg,png|max:1024',
            'categoria' => 'required',
            'conteudo' => 'required',
                ], [
            'required' => 'O campo :attribute é obrigatório',
                ], [
            'titulo' => 'titulo',
            'caminho' => 'imagem',
            'categoria' => 'categoria',
            'texto' => 'conteudo',
        ]);
    }

    protected function gerarSlug($nome = null) {
        if ($nome) {
            $slug_gerada = str_slug($nome, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        return $slug;
    }
    
    public function deletar($id){
        $dica = Dicas::findOrFail($id);
        if(file_exists(public_path('imagens/upload/posts') .'/'. $dica->imagem_destaque )){
            echo 'existe';
        }
        $filename = public_path('imagens/upload/posts') .'/' . $dica->imagem_destaque;
        try{
            File::delete($filename);
            $dica->delete();
            flash()->success("Postagem deletada com sucesso");
            return redirect(route('app::dicas::index'));
        } catch (QueryException $ex) {
                $exception = $ex->getPrevious();
                flash()->error("Erro ao cadastrar. Detalhes: " . $exception->getMessage());
                return redirect()->back()->withInput();
        }
        return redirect(route("app::dicas::index"));
    }

}
