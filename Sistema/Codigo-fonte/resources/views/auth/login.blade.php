@extends('layouts.login')

@section('conteudo')
<div class="container center-father">
    <div class="row ">
        <div class="col-lg-5 col-md-6 box-login-app center-child top-margin">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-login">
                    <img src="{{url('/')}}/assets/imagens/logo-imab.png"/>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal col-md-12" role="form" method="POST" action="{{ route('app::post_login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">E-mail</label>

                            <div class="col-md-9">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-2 control-label">Senha</label>

                            <div class="col-md-9">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-7">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Lembrar de mim
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-5 right">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-logar">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                            </div>
                        </div>
                    </form>
                    @include('shared.erro-validacao')
                    @include('flash::message')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
