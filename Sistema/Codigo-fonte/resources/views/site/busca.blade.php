@extends('layouts.site')


@section('conteudo')
    <!-- Breadcrumbs -->
    <div class="container-fluid breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">home</a> </li>
                <li>a imab</li>
                <li>Busca</li>
            </ul>
        </div>
    </div>
    <!-- Fim do breadcrumb -->
    <!-- Inicio Header -->
    <div class="container-fluid header-valores">
        <div class="container">
            <h2>Você buscou por: {{ $pesquisa }}</h2>
            <p>Foram encontrados 12 resultados</p>
        </div>
    </div>
    <!-- Fim do Header -->

<!--Início Categorias-->


<div class="container">
    <div class="col-md-12">

        @foreach($dicas as $dica)
            @if($dica->categoriastatus != 0)
                @if($dica->status != 0)
                    <div class="row space-busca">
                        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 categ-busca-img">
                            <a href="{{route('dicas::visualizar',['slug' => $dica->slug, 'id' => $dica->id])}}"><img src="{{$upDir}}/{{$dica->miniatura}}" alt=""></a>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 categ-busca">
                            <a href="{{route('dicas::visualizar',['slug' => $dica->slug, 'id' => $dica->id])}}"><h3>{{ $dica->titulo }}</h3></a>
                            <p>{{  $dica->resumo}} </p>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    </div>
</div>

<!--Fim Categorias-->

<!--Início PRODUTOS-->
    <div class="container">
        <div class="show-pr">
           <div class="row result-p">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <img src="assets/imagens/produtos/1.png" alt="">
                    <h4>OSCAR</h4>
                    <p>Código: 141</p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <img src="assets/imagens/produtos/5.png" alt="">
                     <h4>ORBIT</h4>
                    <p>Código: 142</p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <img src="assets/imagens/produtos/3.png" alt="">
                     <h4>POLARA</h4>
                    <p>Código: 143</p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <img src="assets/imagens/produtos/5.png" alt="">

                     <h4>HATO</h4>
                    <p>Código: 111</p>

                </div>

            </div>

          </div>

    </div>

<!--FIM Produtos-->

    <!-- Box de texto -->

    <div class="container-fluid valoresh">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>
    <!-- Fim do box aberto -->


@endsection