@extends('layouts.site')

@section('conteudo')
<!-- Inicio Banners-->


<div class="container-fluid box-banners">
    <!-- Carousel -->
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">

            @for ($i = 0; $i < $banners_qtd; $i++)
            <li data-target="#myCarousel" data-slide-to="{{$i}}" class="item-boxzinho"></li>
            @endfor
        </ol>

        <div class="carousel-inner" role="listbox">

            @foreach($banners as $banner)


            <div class="item item-banner">
                @if($banner->link != '')
                <a href="{{$banner->link}}" target="{{ $banner->nova_aba or "_blank" }}">
                    @endif
                    <img class="first-slide" src="{{$dir_uploads}}/{{$banner->caminho}}"  alt="First slide">
                    <div class="container"></div>
                    @if($banner->link != '')
                </a>
                @endif
            </div>



            @endforeach
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div><!-- /.carousel -->
</div>
<!-- Fim Banners-->


<!-- Fim do call to action -->
<!-- Inicio Destaque -->
<div class="container-fluid box-destaque">
    <div class="container destaque">
        <div class="col-md-3 col-sm-6 col-xs-12  box-texto-destaque">
            <h2>Em Destaques</h2>
            <p>Conheça nossa linha completa e encontre o produto ideal para você.</p>
            <button class="bto-produto-destaque">Nossos Produtos</button>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12  box-produto-destaque">
            <div class="img-produto">
                <img src="assets/imagens/produtos/1.png" alt="Lumina">
            </div>
            <h2>Linha Lumina</h2>
            <p>Design e perfeição com o máximo de atenção aos detalhes.</p>
            <a href="#"><button class="bto-produtos-destaque-info">ver mais</button></a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12  box-produto-destaque">
            <div class="img-produto">
                <img src="assets/imagens/produtos/4.png" alt="Lumina">
            </div>
            <h2>Linha Lumina</h2>
            <p>Design e perfeição com o máximo de atenção aos detalhes.</p>
            <a href="#"><button class="bto-produtos-destaque-info">ver mais</button></a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12  box-produto-destaque">
            <div class="img-produto">
                <img src="assets/imagens/produtos/5.png" alt="Lumina">
            </div>
            <h2>Linha Lumina</h2>
            <p>Design e perfeição com o máximo de atenção aos detalhes.</p>
            <a href="#"><button class="bto-produtos-destaque-info">ver mais</button></a>
        </div>
    </div>
</div>
<!-- Fim Destaque-->

<!-- CALL TO ACTION --> 

<div class="container-fluid cta">
    <div class="container">
        <div class="box-watch-video">
            <div class="inside-box-watch-video">
                <h1>Abra as possibilidades!</h1>
                <p>Escolha o material, superfície e acabamento da fechadura que quiser. </p>
                <a href="personalize"><button>Clique aqui para fazer a simulação</button></a>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(document).ready(function () {
        $('.item-banner').eq(0).addClass('active');
        $('.item-boxzinho').eq(0).addClass('active');
    });
</script>
@endpush
<!-- Fim ASSISTA O VÍDEO --> 

@endsection