@extends('layouts.site')

@section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">home</a> </li>
            <li>produtos</li> 							
            <li>linhas</li>							
        </ul>
    </div>
</div>
<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-valores">
    <div class="container">
        <h2>Linhas</h2>
        <p>
            “Quando uma equipe de trabalho está motivada, a excelência na produção torna-se inerente ao produto.” </p>
        <p style="text-align:center;">Presidente IMAB</p>
    </div>
</div>
<!-- Fim do Header -->
<!-- Box de texto -->
<div class="container-fluid valoresh">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cont">
                    <div class="text">
                        <p><img src="assets/imagens/valores.png" alt=""></p>
                        <p>Incentivar o crescimento profissional de seus colaboradores é uma prioridade para a IMAB. Através de treinamentos e cursos de capacitações técnicas, a IMAB investe no aperfeiçoamento de sua mão de obra, fomentando sua produção sem comprometer o padrão de qualidade exigido. </p>
                        <br>
                        <p>O reconhecimento das pessoas e seus valores humanos também está presente no dia a dia da IMAB. Constantes programas de melhorias no ambiente de trabalho e investimentos estruturais garantem aos colaboradores maior satisfação em fazer parte da equipe IMAB. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fim do box aberto -->


@endsection