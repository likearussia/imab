@extends('layouts.site')
@section('conteudo')
<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li>  <a href="{{url('/')}}">home</a></li>                         
            <li>trabalhe conosco</li>                           
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-valores">
    <div class="container">
        <h2>Trabalhe conosco</h2>
        <p>
            Cadastre seu currículo e faça parte de nossa equipe de profissionais. Quando surgirem vagas compatíveis com seu perfil e qualificações, entraremos em contato.</p>
    </div>
</div>

<!-- Fim do Header -->

<!-- Início formulário de contato -->
<div class="container-fluid contact-form">
    @include('shared.erro-validacao')
    @include('shared.mensagem')
    @include('flash::message')
    <div class="container">

        <div class="formulario form-trabalhe">
            <div class="row col-md-12"><p>
                    Todos os campos marcados com asterisco <font style="color:#F00">(*)</font> são de preenchimento obrigatório.</p></div>

            {{ Form::open(['route' => 'trabalhe::enviar']) }}
            {{ csrf_field() }}
            <div class="col-md-6 col-sm-12 margin">
                <div class="row">
                    <h2 class="title-work">Dados Pessoais</h2>
                </div>

                <div class="row">
                    <div class="col-md-7 col-sm-8 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Nome</label>
                        <input name="dados[nome]" type="text">
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-12  right">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Data de Nascimento</label>
                        <input name="dados[nascimento]" type="text" class="mask-nascimento" >
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> E-mail</label>
                        <input name="dados[email]" type="text">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> CEP</label>
                        <input name="dados[cep]" type="text">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 right">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Endereço</label>
                        <input name="dados[endereco]" type="text">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Número</label>
                        <input name="dados[numero]" type="text">
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 box-input">
                        <label for="" class="lab2">Complemento</label>
                        <input name="dados[complemento]" type="text">
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Bairro</label>
                        <input name="dados[bairro]" type="text">
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Cidade</label>
                        <input name="dados[cidade]" type="text">
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 right">
                        <label for="" class="lab2"><font style="color:#F00">* </font> UF</label>
                        <input name="dados[uf]" type="text">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> DDD/Telefone</label>
                        <input name="dados[telefone]" type="text" class="mask-telefone" >
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> DDD/Celular</label>
                        <input name="dados[celular]" type="text" class="mask-telefone">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Estado Civil</label>
                        <label class="lab">
                            <select name="dados[estado_civil]" title="Segmento">
                                <option value="">Selecione</option>
                                <option value="Casado">Casado(a)</option>
                                <option value="Divorciado">Divorciado(a)</option>
                                <option value="Solteiro">Solteiro(a)</option>
                                <option value="Viúvo">Viúvo(a)</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Nº de Filhos</label>
                        <input name="dados[filhos]"  type="text">
                    </div>

                    <div class="col-md-5 col-sm-5 col-xs-12 right">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Como conheceu a Imab</label>
                        <label class="lab">
                            <select name="dados[conheceu]">
                                <option value="">Selecione</option>
                                <option value="Feiras">Feiras e Eventos</option>
                                <option value="Indicacao">Indicação</option>
                                <option value="Internet">Internet</option>
                                <option value="Revista">Revista</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <h2 class="title-work">Formação Acadêmica</h2>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Instituição</label>
                        <input name="formacao[instituicao][]" type="text">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Curso</label>
                        <input name="formacao[curso][]" type="text">
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Ano de conclusão</label>
                        <input name="formacao[conclusao][]" type="text" class="mask-ano">
                    </div>
                </div>
                <div class="row top-more">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Instituição</label>
                        <input name="formacao[instituicao][]" type="text">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-9 col-sm-8 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Curso</label>
                        <input name="formacao[curso][]" type="text">
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Ano de conclusão</label>
                        <input name="formacao[conclusao][]" type="text" class="mask-ano">
                    </div>
                </div>
                <div class="row">
                    <h2 class="title-work">Idiomas</h2>
                </div>

                <div class="row">
                    <h3 class="subtitle-work">Espanhol</h3>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Leitura:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" value="Não" name="espanhol[leitura][]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="espanhol[leitura][]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="espanhol[leitura][]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="espanhol[leitura][]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Escrita:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" value="Não" name="espanhol[escrita][]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="espanhol[escrita][]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="espanhol[escrita][]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="espanhol[escrita][]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Conversação:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" value="Não" name="espanhol[fala][]"/>
                            Não
                        </label>

                        <label> 
                            <input type="radio" value="Básico" name="espanhol[fala][]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="espanhol[fala][]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="espanhol[fala][]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row top-more">
                    <h3 class="subtitle-work">Inglês</h3>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Leitura:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" value="Não" name="ingles[leitura][]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="ingles[leitura][]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="ingles[leitura][]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="ingles[leitura][]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Escrita:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" value="Não" name="ingles[escrita][]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="ingles[escrita][]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="ingles[escrita][]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="ingles[escrita][]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Conversação:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" value="Não" name="ingles[fala][]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="ingles[fala][]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="ingles[fala][]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="ingles[fala][]"/>
                            Avançado
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 margin">
                <div class="row">
                    <h2 class="title-work">Aplicativos</h2>
                </div>

                <div class="row">
                    <h3 class="subtitle-work">Windows</h3>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Nível:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" checked="true" value="Não" name="aplcativos[windows]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="aplcativos[windows]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="aplcativos[windows]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="aplcativos[windows]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row">
                    <h3 class="subtitle-work">Internet</h3>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Nível:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" checked="true" value="Não" name="aplcativos[internet]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="aplcativos[internet]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="aplcativos[internet]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="aplcativos[internet]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row">
                    <h3 class="subtitle-work">Excel</h3>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Nível:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" checked="true" value="Não" name="aplcativos[excel]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="aplcativos[excel]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="aplcativos[excel]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="aplcativos[excel]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row">
                    <h3 class="subtitle-work">PowerPoint</h3>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Nível:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" checked="true" value="Não" name="aplcativos[powerpoint]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="aplcativos[powerpoint]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="aplcativos[powerpoint]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="aplcativos[powerpoint]"/>
                            Avançado
                        </label>
                    </div>
                </div>

                <div class="row">
                    <h3 class="subtitle-work">Word</h3>
                </div>

                <div class="row top-medium">
                    <div class="col-md-1 col-sm-2 col-xs-12 txt-idioma">Nível:</div>
                    <div class="col-md-11 col-sm-10 col-xs-12 rd-lbl">
                        <label>
                            <input type="radio" checked="true" value="Não" name="aplcativos[word]"/>
                            Não
                        </label>

                        <label>
                            <input type="radio" value="Básico" name="aplcativos[word]"/>
                            Básico
                        </label>

                        <label>
                            <input type="radio" value="Intermediário" name="aplcativos[word]"/>
                            Intermediário
                        </label>

                        <label>
                            <input type="radio" value="Avançado" name="aplcativos[word]"/>
                            Avançado
                        </label>
                    </div>
                </div>


                <div class="row" style="margin-top: 14px">
                    <div class="col-md-12">                             
                        <label for="" class="lab2">Outros Aplicativos</label>
                        <input name="aplcativos[outros]" type="text">
                    </div>
                </div>


                <div class="row">
                    <h2 class="title-work">Experiência Profissional</h2>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Empresa</label>
                        <input name="experiencia[empresa][]" type="text">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Cargo</label>
                        <input name="experiencia[cargo][]" type="text">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-6 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Início</label>
                        <input name="experiencia[inicio][]" type="text" class="mask-mesano">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Término</label>
                        <input name="experiencia[termino][]" type="text" class="mask-mesano">
                    </div>
                </div>


                <div class="row top-more">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Empresa</label>
                        <input name="experiencia[empresa][]" type="text">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6 col-sm-6 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Cargo</label>
                        <input name="experiencia[cargo][]" type="text">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-6 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Início</label>
                        <input name="experiencia[inicio][]" type="text" class="mask-mesano">
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Término</label>
                        <input name="experiencia[termino][]" type="text" class="mask-mesano">
                    </div>
                </div>


                <div class="row">
                    <h2 class="title-work">Informação Complementar</h2>
                </div>

                <div class="row">

                    <div class="col-md-6 col-sm-6 col-xs-12 box-input">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Pretensão Salarial</label>
                        <input name="pretensao_salarial" type="text" class="mask-valor" >
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <label for="" class="lab2"><font style="color:#F00">* </font> Área de Interesse</label>
                        <label class="lab">
                            <select name="dados[area]">
                                <option value="">Selecione</option>
                                <option value="Engenharia">Engenharia</option>
                                <option value="Produção">Produção</option>
                                <option value="Administração">Administração</option>
                                <option value="Recursos Humanos">Recursos Humanos</option>
                                <option value="Comercial">Comercial</option>
                            </select>
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="Enviar">
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>

    </div>
</div>

<!-- Fim do formulário -->

@push('scripts')
<script src="js/site.js"></script>
@endpush

@endsection