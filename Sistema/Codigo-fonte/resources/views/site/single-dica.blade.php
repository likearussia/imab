@extends('layouts.site')

@section('conteudo')


<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">home</a> </li>							
            <li><a href="{{url('dicas-e-solucoes')}}">dicas e soluções</a></li>	
            <li>Postagem</li>						
        </ul>
    </div>
</div>
@if($dicas->status != 1)

<div class="container-fluid header-valores">
    <div class="container">
        <h2>Postagem não encontrada.</h2>

    </div>
</div>

@else
<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-valores">
    <div class="container">
        <h2>{{$dicas->titulo}}</h2>

    </div>
</div>

<!-- Fim do Header -->
<!-- Masonry Grid -->
<div class="container-fluid single">
    <div class="container">
        <div class="row">
            <div class="col-md-9 single-dica-post">
                <img src="{{$upDir}}/{{$dicas->imagem_destaque}}" alt="">        
                {!! $dicas->conteudo !!}
            </div>

            <div class="col-md-3">
                <div class="row tags">
                    <h3><i class="fa fa-tags" aria-hidden="true"></i> Categoria</h3>
                    <div class="result-tag">
                        <ul>
                            <li>{{$categoria->nome}}</li>
                        </ul>

                    </div>
                </div>
                <div class="row date">
                    <h3><i class="fa fa-clock-o" aria-hidden="true"></i> Postado em</h3>
                    <div class="result-data">
                        <p>{{$dicas->created_at->format('d  M y - H:i')}}</p>
                    </div>
                </div>
                <div class="row share">
                    <h3><i class="fa fa-share-alt" aria-hidden="true"></i> Compartilhar</h3>
                    <div class="result-share">
                        <ul>
                            <li><a href="https://www.facebook.com/sharer.php?u=http://www.imab.com.br/"><i class="fa fa-facebook" aria-hidden="true"></i> Compartilhar no facebook</a> </li>
                            <li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium&original_referer=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium"><i class="fa fa-twitter" aria-hidden="true"></i> Compartilhar no twitter</a>  </li>
                            <li><a href="https://www.instagram.com/imabfechaduras/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> Compartilhar no instagram</a>  </li>
                            <li><a href="https://pt.pinterest.com/pin/create/button/" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i> Compartilhar no pinterest</a>  </li>
                            <li><a href="mailto:contato@imab.com" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i> Enviar por email</a> </li>
                            
                            <a data-pin-do="buttonBookmark" data-pin-lang="pt" data-pin-save="true" href="https://pt.pinterest.com/pin/create/button/"></a>
                            
                            
                        </ul>
                    </div>
                </div>
            </div>
           
        </div>
        <div class="row share">
            <div class="mais-news">
                <ul>
                    <li><a href="{{url('dicas-e-solucoes')}}">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i> Leia Mais</a> </li>  
                </ul>
             </div>
        </div>

    </div>
</div>


@endif


<!-- FIM DO MASONRY -->

@endsection