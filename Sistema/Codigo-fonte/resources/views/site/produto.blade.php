@extends('layouts.site') @section('conteudo')


<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">home</a> </li>
            <li>produtos</li>
            <li>modelos</li>
            <li>Cosmus</li>
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->

<!-- Produtos -->
<div class="container-fluid products">
    <div class="container">
        <div class="row">
            <div class="col-md-8 content-pr">
                <div class="row">
                    <div class="col-md-12 campo-personalizacao-prd">
                        <img src="personalizacao/ambiente/Interna_1.jpg" class="back" alt="">
                        <img src="personalizacao/portas/m_clara_17.png" class="door" alt="">
                        <img src="personalizacao/fechaduras/77E17CA.png" class="fechadura" alt="">
                        <img src="personalizacao/macanetas/MA0932000CA00.png" class="macaneta" alt="">

                    </div>
                </div>
            </div>

            <div class="col-md-4 det-prod">

                <div class="row person-f codigo-prod">
                    <div class="col-md-12">
                        <span>Nome:&nbsp;</span>
                        <h3>Cosmus</h3>
                    </div>
                    <p>Código: 60</p>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <span>Material:&nbsp;</span>
                        <h4>Zamak</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <span>Ambiente:&nbsp;</span>
                        <h4>Interno</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <span>Acabamento:</span>
                        <br>
                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/acabamentos/1.png" alt=""></button>
                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/acabamentos/2.png" alt=""></button>
                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/acabamentos/3.png" alt=""></button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <span>Superfície:</span>
                        <br>
                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/superficies/1.png" alt=""></button>
                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/superficies/2.png" alt=""></button>
                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/superficies/3.png" alt=""></button>
                    </div>
                </div>
                <div class="row person-f">
                    <div class="col-md-12">
                        <a href="personalize.html">
                            <button><i class="fa fa-envelope-o" aria-hidden="true"></i>Indique este produto</button>
                        </a>
                    </div>
                </div>

                <div class="row person-f">
                    <div class="col-md-12">
                        <a href="personalize.html">
                            <button><i class="fa fa-star-o" aria-hidden="true"></i>Adicionar a lista de desejos</button>
                        </a>
                    </div>
                </div>



            </div>

        </div>

    </div>
</div>
<div class="container-fluid desenhot">
    <div class="container">
        <div class="col-md-6">
            <h3>Desenho técnico</h3>
            <img src="assets/imagens/desenhot.png" alt="">
        </div>
        <div class="col-md-6">
            <h3>Informações técnicas</h3>
            <table class="table table-striped">

                <tbody>
                    <tr>
                        <td>Máquina</td>
                        <td>Todos</td>

                    </tr>
                    <tr>
                        <td>Ambientes</td>
                        <td>Todos</td>

                    </tr>
                    <tr>
                        <td>Acabamento</td>
                        <td>Todos</td>

                    </tr>
                    <tr>
                        <td>Material</td>
                        <td>Todos</td>
                    </tr>
                    <tr>
                        <td>Roseta ou Espelho</td>
                        <td>Todos</td>
                    </tr>
                    <tr>
                        <td>Desenho técnico</td>
                        <td>Download <i class="fa fa-download" aria-hidden="true"></i></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="carousel-outrosp">

            <div class="row">
                <div class="col-md-12">
                    <div id="Carousel" class="carousel slide">

                        <h2>Produtos Relacionados</h2>

                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="item active">
                                <div class="row">
                                   <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/69E13CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0978000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/81E17CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0932000CA00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/77E17CA.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0960000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                            <div class="item">
                                <div class="row">
                                   <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/69E13CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0978000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/81E17CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0932000CA00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/77E17CA.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0960000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>
                                        </a>
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                            <div class="item">
                                <div class="row">
                                   <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/69E13CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0978000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>

                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/81E17CR.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0932000CA00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>


                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <a href="#" class="prod-thumb">
                                            <div class="macaneta-montada">
                                                <img class="fechadura-thumb" src="personalizacao/fechaduras/77E17CA.png" alt="Image">
                                                <img class="macaneta-thumb" src="personalizacao/macanetas/MA0960000CR00.png" alt="Image">
                                            </div>
                                            <h3>orbit</h3>


                                        </a>
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->

                        </div>
                        <!--.carousel-inner-->
                        <a data-slide="prev" href="#Carousel" class="left carousel-control"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                        <a data-slide="next" href="#Carousel" class="right carousel-control"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                    <!--.Carousel-->



                </div>


            </div>
            <!--.container-->

        </div>
    </div>
</div>










@endsection