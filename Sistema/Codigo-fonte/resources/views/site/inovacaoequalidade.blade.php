@extends('layouts.site')


@section('conteudo')


<!-- Breadcrumbs -->
    <div class="container-fluid breadcrumb">
       <div class="container">
        <ul class="breadcrumb">
           <li>  <a href="{{url('/')}}">home</a> </li>
            <li>a imab</li>                           
            <li>inovação e qualidade</li>                           
        </ul>
        </div>
    </div>
    
    <!-- Fim do breadcrumb -->
    <!-- Inicio Header -->
    <div class="container-fluid header-valores">
        <div class="container">
            <h2>{{$inovacao->titulo}}</h2>
            <p>{{$inovacao->subtitulo}}</p>
        </div>
    </div>
    
    <!-- Fim do Header -->
    
      <!-- Box de texto -->
    
    <div class="container-fluid inova">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="cont">
                       <div class="text">
                           <p class="text-institucional"><img class="image-inst" src="{{url('/')}}/imagens/upload/institucionais/{{$inovacao->imagem_destaque}}" alt=""></p>
                      </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="cont">
                       <div class="text">
                        {!! $inovacao->texto !!}
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fim do box aberto -->



@endsection