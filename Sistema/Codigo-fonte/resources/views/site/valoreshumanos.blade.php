@extends('layouts.site') @section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li> <a href="{{url('/')}}">home</a> </li>
            <li>a imab</li>
            <li>valores humanos</li>
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-valores">
    <div class="container">
        <h2>{{$valores->titulo}}</h2>
        <p>
            {{$valores->subtitulo}}
    </div>
</div>

<!-- Fim do Header -->

<!-- Box de texto -->

<div class="container-fluid valoresh">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="cont">
                    <div class="text">
                        <p class="text-institucional"><img class="image-inst" src="{{url('/')}}/imagens/upload/institucionais/{{$valores->imagem_destaque}}" alt=""></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="cont">
                    <div class="text">
                        {!! $valores->texto !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fim do box aberto -->


@endsection