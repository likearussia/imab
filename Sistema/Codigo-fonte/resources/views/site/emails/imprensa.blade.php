<center>
    <table border="0" cellpadding="0" cellspacing="0" style="padding-left: 10px; padding-right: 10px;" bgcolor="#333d53">
        <thead>
        <tr>
            <th colspan="2" bgcolor="#333d53" style="color: #fff; font-family: arial; font-weight: normal; padding: 20px; font-size: 20px;">
                <p style="text-transform: uppercase; color: #fff; font-family: arial;">Novo usuário cadastrado.</p>
            </th>
        </tr>
        </thead>
        <tbody style="font-family: arial;" >
        <tr>
            <td colspan="2" style=" border-bottom: 1px solid black;">
                <center>
                    <table style=" width: 100%; padding-left: 0px; font-size: 20px; background: #fff;" border="0" cellspacing="0" bgcolor="#fff" cellpadding="0">
                        <tr>
                            <td  style="padding: 10px; float: left; color: #333d53; text-transform: uppercase; font-weight: bold; vertical-align: middle; font-family: arial;">Dados pessoais</td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center>
                    <table style="width: 100%; background: #fff; " bgcolor="#fff">
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p >Nome: {{ $data['nome'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>E-mail: {{ $data['email'] }} </p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>Telefone: {{ $data['telefone'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial; padding-bottom: 5px;"><p>  {{ ($data['tipo'] == 2) ? "Segmento" : "Tipo de veículo"  }}: {{ $data['segmento'] }} </p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>Site: {{ $data['site'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>Cargo: {{ $data['cargo'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>Telefone/Celular: {{ $data['celular'] }}</p> </td>
                        </tr>

                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>Cidade: {{ $data['cidade'] }}</p> </td>
                        </tr>

                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>Estado: {{ $data['estado'] }}</p> </td>
                        </tr>

                    </table>
                </center>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <center>
                    <table style="width: 100%; background: #fff; border-top: 3px solid #3a4661;">
                        <tr>
                            <td colspan="2" style="font-family: arial; padding: 5px;">
                                <center>
                                    <table>
                                        <tr>
                                            <td style="padding: 10px;">
                                                <a style="text-decoration: none;  font-size: 12px;" href="{{ route('app::usuarios-site::status', ['id' => $data['user_id'] , 'novoStatus' => '1']) }}" target="_blank">
                                                    Ativar usuário
                                                </a>
                                                <a href="{{ url('/') }}/_app" target="_blank" style="text-decoration: none; padding: 10px;  font-size: 12px; margin-left: 10px;">
                                                    Ir para área administrativa
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </center>

                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <table style="width: 98%; margin-left: 1%;" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style=" background: #333d53; color: #fff; font-family: arial;">E-mail gerado automaticamente, favor não responder.</td>
                        <td style="text-align: center; background: #333d53; padding: 10px; color: #fff; font-family: arial;"><img width="80" src="http://imageshack.com/a/img923/747/a5B6Rg.png"></td>
                    </tr>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</center>