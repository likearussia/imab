<table border="0" cellpadding="0" cellspacing="0" style="padding-left: 10px; padding-right: 10px;" bgcolor="#333d53">
    <thead>
    <tr>
        <th colspan="2" bgcolor="#333d53" style="color: #fff; font-family: arial; font-weight: normal; padding: 20px; font-size: 20px;">
            <p style="text-transform: uppercase; color: #fff; font-family: arial;">Recuperar senha</p>
        </th>
    </tr>
    </thead>
    <tbody style="font-family: arial;" >
    <tr>
        <td colspan="2" style="border-bottom: 1px solid black;">
            <center>
                <table style="width: 100%; background: #fff; padding-left: 0px; font-size: 20px; border-top: 10px solid #333d53;" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td  style="padding: 10px; float: left; color: #333d53; text-transform: uppercase; font-weight: bold; vertical-align: middle; font-family: arial;">Mensagem</td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <center>
                <table style="width: 100%; background: #fff;">
                    <tr>
                        <td colspan="2" style="font-family: arial; padding: 5px;"><p >Olá, você solicitou a recuperação de senha para acesso ao site imab.com.br, caso deseje alterar sua senha <a href="{{ route('site::novasenha', ['id' => $data['codigo']]) }}">Clique aqui</a>. Caso não tenha feito a solicitação ignore essa mensagem. </p> </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
    <tr>
        <td colspan="2" >
            <table style="width: 98%; margin-left: 1%;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style=" background: #333d53; color: #fff; font-family: arial;">E-mail gerado automaticamente, favor não responder.</td>
                    <td style="text-align: center; background: #333d53; padding: 10px; color: #fff; font-family: arial;"><img width="80" src="http://imageshack.com/a/img923/747/a5B6Rg.png"></td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>