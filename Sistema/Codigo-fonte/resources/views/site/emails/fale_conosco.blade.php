Segmento: {!! $data['form']['segmento'] or '' !!}
Nome: {!! $data['form']['nome'] or '' !!}
E-mail: {!! $data['form']['email'] or '' !!}
CEP: {!! $data['form']['cep'] or '' !!}
Endereço: {!! $data['form']['endereco'] or '' !!}
Numero: {!! $data['form']['numero'] or '' !!}
Complemento: {!! $data['form']['complemento'] or '' !!}
Bairro: {!! $data['form']['bairro'] or '' !!}
Estado: {!! $data['form']['estado'] or '' !!}
Cidade: {!! $data['form']['cidade'] or '' !!}
Telefone: {!! $data['form']['telefone'] or '' !!}
Atividade: {!! $data['form']['atividade'] or '' !!}
Como conheceu a IMAB?: {!! $data['form']['conheceu'] or '' !!}
Mensagem: {!! $data['form']['mensagem'] or '' !!}