<table border="0" cellpadding="0" cellspacing="0" style="padding-left: 10px; padding-right: 10px;" bgcolor="#333d53">
    <thead>
        <tr>
            <th colspan="2" bgcolor="#333d53" style="color: #fff; font-family: arial; font-weight: normal; padding: 20px; font-size: 20px;">
                <p style="text-transform: uppercase; color: #fff; font-family: arial;">Formulário de contato</p>
            </th>
        </tr>
    </thead>
    <tbody style="font-family: arial;" >
        <tr>
            <td colspan="2" style=" border-bottom: 1px solid black;">
                <center>
                    <table style=" width: 100%; padding-left: 0px; font-size: 20px; background: #fff;" border="0" cellspacing="0" bgcolor="#fff" cellpadding="0">
                        <tr>
                            <td  style="padding: 10px; float: left; color: #333d53; text-transform: uppercase; font-weight: bold; vertical-align: middle; font-family: arial;">Dados pessoais</td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center>
                    <table style="width: 100%; background: #fff; " bgcolor="#fff">
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p >Nome: {{ $data['nome'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>E-mail: {{ $data['email'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial"><p>Telefone: {{ $data['telefone'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px; font-family: arial; padding-bottom: 5px;"><p>  Segmento: {{ $data['segmento'] }}</p> </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 100%; border-bottom: 1px solid black;">
                <center>
                    <table style="width: 100%; background: #fff; padding-left: 0px; font-size: 20px; border-top: 10px solid #333d53;" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td  style="padding: 10px; float: left; color: #333d53; text-transform: uppercase; font-weight: bold; vertical-align: middle; font-family: arial;">Endereço</td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center>
                    <table style="width: 100%; background: #fff;">
                        <tr>
                            <td style="padding-left: 10px; font-family: arial; padding-top: 5px;"><p>CEP: {{ $data['cep'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; padding-left: 10px;"><p>Endereço: {{ $data['endereco'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; padding-left: 10px;"><p>Numero: {{ $data['numero'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; padding-left: 10px;"><p>  Complemento: {{ $data['complemento'] }} </p> </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px;"><p>  Bairro: {{ $data['bairro'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; padding-left: 10px;"><p>  Cidade: {{ $data['cidade'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; padding-left: 10px;"><p>  Estado: {{ $data['estado'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; padding-left: 10px;"><p>  Atividade: {{ $data['atividade'] }}</p> </td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; padding-bottom: 5px; padding-left: 10px;"><p>  Como conheceu a IMAB?: {{ $data['conheceu'] }}</p> </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom: 1px solid black;">
                <center>
                    <table style="width: 100%; background: #fff; padding-left: 0px; font-size: 20px; border-top: 10px solid #333d53;" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td  style="padding: 10px; float: left; color: #333d53; text-transform: uppercase; font-weight: bold; vertical-align: middle; font-family: arial;">Mensagem</td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center>
                    <table style="width: 100%; background: #fff;">
                        <tr>
                            <td colspan="2" style="font-family: arial; padding: 5px;"><p >{{ $data['mensagem'] }}</p> </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <table style="width: 98%; margin-left: 1%;" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style=" background: #333d53; color: #fff; font-family: arial;">E-mail gerado automaticamente, favor não responder.</td>
                        <td style="text-align: center; background: #333d53; padding: 10px; color: #fff; font-family: arial;"><img width="80" src="http://imageshack.com/a/img923/747/a5B6Rg.png"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>