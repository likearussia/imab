@extends('layouts.site') @section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">home</a> </li>
            <li>produtos</li>
            <li>modelos</li>
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->

<!-- Produtos -->

<div class="container-fluid products">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 filtro">

                <div class="row person-f">
                    <div class="col-md-12">
                        <h4>Personalize sua maçaneta</h4></div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <p>Escolha o material, superfície e acabamento da fechadura que quiser!</p>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <img src="assets/imagens/icons/personalize-b.png" alt="">
                    </div>
                    <div class="col-md-12">
                        <a href="{{url('personalize')}}">
                            <button>COMECE A PERSONALIZAR</button>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Filtrar por ambiente</h4>
                        <ul class="list">
                            <li class="list__item">
                                <label class="label--checkbox">
                                    <input type="checkbox" class="checkbox"> Externo
                                </label>
                            </li>
                            <li class="list__item">
                                <label class="label--checkbox">
                                    <input type="checkbox" class="checkbox"> Interno
                                </label>
                            </li>
                            <li class="list__item">
                                <label class="label--checkbox">
                                    <input type="checkbox" class="checkbox" checked> Banheiro
                                </label>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Filtrar por material</h4>
                        <ul class="list">
                            <li class="list__item">
                                <label class="label--checkbox">
                                    <input type="checkbox" class="checkbox"> Latão
                                </label>
                            </li>
                            <li class="list__item">
                                <label class="label--checkbox">
                                    <input type="checkbox" class="checkbox" checked> Zamak
                                </label>
                            </li>


                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Filtrar por linha</h4>
                        <input type="text" name="" placeholder="colocar o nome" class="search-linha">
                        <div class="linha-filtro">

                            <ul class="list">
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> KYHE
                                    </label>
                                </li>
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> COSMUS
                                    </label>
                                </li>
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> BINE
                                    </label>
                                </li>
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> METRODIA
                                    </label>
                                </li>
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> VERT
                                    </label>
                                </li>
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> ORBIT
                                    </label>
                                </li>
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> HATO
                                    </label>
                                </li>
                                <li class="list__item">
                                    <label class="label--checkbox">
                                        <input type="checkbox" class="checkbox"> STEA
                                    </label>
                                </li>

                            </ul>


                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Categorias de produtos</h4>
                        <ul class="categoriaprod">
                            <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{url('fechaduras')}}">Fechaduras</a></li>
                            <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{url('componentes')}}">Componentes</a></li>
                            <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{url('puxadores')}}">Puxadores</a></li>
                            <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{url('dobradicas-pivots')}}">Dobradiças</a></li>


                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 content-pr">

                <div class="carousel-outrosp">

                    <div class="row">
                        <div class="col-md-12">
                            <div id="Carousel" class="carousel slide">

                                <h2>Linhas em destaque</h2>

                                <!-- Carousel items -->
                                <div class="carousel-inner">

                                    <div class="item active">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->

                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->

                                    <div class="item">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>

                                                </a>
                                            </div>
                                        </div>
                                        <!--.row-->
                                    </div>
                                    <!--.item-->

                                </div>
                                <!--.carousel-inner-->
                                <a data-slide="prev" href="#Carousel" class="left carousel-control"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                                <a data-slide="next" href="#Carousel" class="right carousel-control"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </div>
                            <!--.Carousel-->



                        </div>


                    </div>
                    <!--.container-->

                </div>
                <div class="show-pr">
                    <div class="row result">
                        <p>Filtrado por: <a href="#">Banheiro</a></p>
                        <button><i class="fa fa-times" aria-hidden="true"></i> Limpar filtros</button>
                    </div>
                    <div class="row result-p">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/1.png" alt="">

                            <h4>OSCAR</h4>
                            <p>Código: 141</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/5.png" alt="">

                            <h4>ORBIT</h4>
                            <p>Código: 142</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/3.png" alt="">

                            <h4>POLARA</h4>
                            <p>Código: 143</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/5.png" alt="">

                            <h4>HATO</h4>
                            <p>Código: 111</p>

                        </div>

                    </div>

                    <div class="row result-p">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/4.png" alt="">

                            <h4>KYHE</h4>
                            <p>Código: 111</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/3.png" alt="">

                            <h4>COSMUS</h4>
                            <p>Código: 111</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/5.png" alt="">

                            <h4>HATO</h4>
                            <p>Código: 111</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/2.png" alt="">

                            <h4>BINE</h4>
                            <p>Código: 111</p>

                        </div>

                    </div>

                    <div class="row result-p">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/2.png" alt="">

                            <h4>METRODIA</h4>
                            <p>Código: 111</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/4.png" alt="">

                            <h4>VERT</h4>
                            <p>Código: 111</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/5.png" alt="">

                            <h4>HATO</h4>
                            <p>Código: 111</p>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <img src="assets/imagens/produtos/5.png" alt="">

                            <h4>HATO</h4>
                            <p>Código: 111</p>

                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="paginacao">
                            <ul>
                                <li>1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>4</li>
                                <li>5</li>
                                <li>Última</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>










@endsection