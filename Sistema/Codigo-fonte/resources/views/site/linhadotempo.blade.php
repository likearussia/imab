@extends('layouts.site')


@section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">home</a> </li>
            <li><a href="#">imab</a></li> 							
            <li>linha do tempo</li>							
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-historia">
    <div class="container">
        <h2>Linha do tempo</h2>
        <p>
            Nossa trajetória conta com diversos momentos marcantes. Navegue pelas datas e conheça mais sobre nosso trabalho e nosso crescimento profissional.</p>
    </div>
</div>

<!-- Fim do Header -->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6" id="img_doismilequinze">Imagem</div>
        <div class="col-md-6 linhadotempo_um">
            <h1>2015</h1>
            <div class="text-box"> <h3>50 anos de sucesso</h3>
                <p>Mantendo sua Qualidade inicial, ampliando sua linha de produtos, e cada vez mais presente no mercado da construção, a IMAB completa seus 50 anos de Sucesso!</p>
                <img src="assets/imagens/50anos.png" alt="">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 linhadotempo_dois">
            <div class="text-box">
                <h3>Um novo conceito</h3>
                <p>A IMAB incorporou novos produtos ao seu mix, porém não abandonou o seu mercado tradicional de ferragens, nem o padrão e a qualidade empregados na primeira ponta de lança produzida na década de 1960.</p> </div>
            <h1>2010</h1>
        </div>
        <div class="col-md-6" id="img_doismilequinze">Imagem</div>
    </div>

    <div class="row">
        <div class="col-md-6" id="img_doismilequinze">Imagem</div>
        <div class="col-md-6 linhadotempo_um">
            <h1>2000</h1>
            <div class="text-box">
                <h3>Mais tecnologia</h3>
                <p>Em janeiro de 2002, a ABNT – Associação Brasileira de Normas, publica a norma nº 14.913, com exigências para a fabricação de fechaduras de tráfego intenso. A IMAB não precisou nem se adaptar à norma, pois os produtos já atendiam e ultrapassavam as exigências desde o lançamento de sua primeira fechadura, em 1994. </p>
                <img src="assets/imagens/abnt.png" alt="">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 linhadotempo_dois">

            <div class="text-box">
                <h3>Maçanetas brasileiras com jeito europeu</h3>
                <p>Foram empregados cinco anos em testes e análises para se produzir o produto ideal, que chegou ao mercado, em 1994: uma fechadura da mais alta qualidade, cujos padrões eram baseados em normativas europeias e americanas.  </p>
                <h1>1990</h1>
            </div>
        </div>
        <div class="col-md-6" id="img_doismilequinze">Imagem</div>
    </div>

    <div class="row">
        <div class="col-md-6" id="img_doismilequinze">Imagem</div>
        <div class="col-md-6 linhadotempo_um">
            <h1>1980</h1>
            <div class="text-box">
                <h3>Novidades</h3>
                <p>A IMAB entra em um novo mercado, o de maçanetas avulsas. A empresa passa a fabricar maçanetas, com design e acabamento diferenciado para ser acoplado na fechadura de outros fabricantes. No fim da década, contava 69 tipos de furação de rosetas e espelhos para acoplar com as fechaduras existentes no mercado. Neste momento, a empresa lança sua própria fechadura, produzida dentro dos mais altos padrões de qualidade.  </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 linhadotempo_dois">

            <div class="text-box">
                <h3>Ampliação</h3>
                <p>A IMAB cresce e amplia sua produção. Em 1973, muda-se para a sede própria em Embu-Guaçu-SP, local onde a empresa está até os dias atuais, em grande área construída.</p>
                <h1>1970</h1>
            </div>
        </div>
        <div class="col-md-6" id="img_doismilequinze">Imagem</div>
    </div>

    <div class="row">
        <div class="col-md-6" id="img_doismilequinze">Imagem</div>
        <div class="col-md-6 linhadotempo_um">
            <h1>1960</h1>
            <div class="text-box">
                <h3>O começo</h3>
                <p>A IMAB começa sua história fabricando pontas de lanças para portões, quase como obras de arte, feitas à mão com total cuidado. </p>
            </div>
        </div>
    </div>
</div>

@endsection