@extends('layouts.site')

@section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li>  <a href="{{url('/')}}">home</a> </li>
            <li>produtos</li>                           
            <li>personalize sua maçaneta</li>                       
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->



<!-- Fim do Header -->

<!-- Produto info -->
<div class="container-fluid products">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 custom-menu">

                <div class="row person-f">
                    <div class="col-md-12 col-sm-12 col-xs-12 personalize">
                        <h4>Personalize sua maçaneta</h4>
                         <p>Escolha o modelo, o material, o acabamento, o ambiente e a tecnologia que melhor combine com o seu projeto.</p>
                    </div>

                    <div class="col-md-12 col-sm-4 col-xs-12 modelo">
                        <h4>Modelo</h4>
                        <input type="text" name="" placeholder="nome do modelo" class="search-linha">
                        <div class="linha-filtro">

                            <ul class="list">
                                <li class="list__item">
                                    <input type="radio" class="radio" checked  name="modelo
                                    " id="kyhe">
                                    <label class="label--radio" for="kyhe">
                                        Kyhe
                                    </label>
                                </li>
                                <li class="list__item">
                                    <input type="radio" class="radio" name="modelo
                                    " id="cosmus">
                                    <label class="label--radio" for="cosmus">
                                        Cosmus
                                    </label>
                                </li>

                                <li class="list__item">
                                    <input type="radio" class="radio" name="modelo
                                    " id="bine">
                                    <label class="label--radio" for="bine">
                                        Bine
                                    </label>
                                </li>

                                <li class="list__item">
                                    <input type="radio" class="radio" name="modelo
                                    " id="metrodia">
                                    <label class="label--radio" for="metrodia">
                                        Metrodia
                                    </label>
                                </li>

                                <li class="list__item">
                                    <input type="radio" class="radio" name="modelo
                                    " id="vert">
                                    <label class="label--radio" for="vert">
                                        Vert
                                    </label>
                                </li>

                                <li class="list__item">
                                    <input type="radio" class="radio" name="modelo
                                    " id="orbit">
                                    <label class="label--radio" for="orbit">
                                        Orbit
                                    </label>
                                </li>

                                <li class="list__item">
                                    <input type="radio" class="radio" name="modelo
                                    " id="hato">
                                    <label class="label--radio" for="hato">
                                        Hato
                                    </label>
                                </li>

                                <li class="list__item">
                                    <input type="radio" class="radio" name="modelo
                                    " id="stea">
                                    <label class="label--radio" for="stea">
                                        Stea
                                    </label>
                                </li>

                            </ul>


                        </div>
                    </div>

                    <div class="col-md-12 col-sm-4 col-xs-6 material">
                        <h4>Material</h4>
                        <ul class="list">
                            <li class="list__item">
                                <input type="radio" class="radio" checked  name="material" id="latao">
                                <label class="label--radio" for="latao">
                                    Latão
                                </label>
                            </li>
                            <li class="list__item">
                                <input type="radio" class="radio" name="material" id="zamak">
                                <label class="label--radio" for="zamak">

                                    Zamak
                                </label>
                            </li>

                        </ul>
                    </div>

                    <div class="col-md-12 col-sm-4 col-xs-6 ambiente">
                        <h4>Ambiente</h4>
                        <ul class="list">
                            <li class="list__item">
                                <input type="radio" class="radio" checked  name="ambiente" id="externo">
                                <label class="label--radio" for="externo">
                                    Externo
                                </label>
                            </li>
                            <li class="list__item">
                                <input type="radio" class="radio" name="ambiente" id="interno">
                                <label class="label--radio" for="interno">
                                    Interno
                                </label>
                            </li>

                            <li class="list__item">
                                <input type="radio" class="radio" name="ambiente" id="banheiro">
                                <label class="label--radio" for="banheiro">
                                    Banheiro
                                </label>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-6 acabamento">
                        <h4>Acabamento</h4>
                        <img src="assets/imagens/acabamentos/1.png" alt="">
                        <img src="assets/imagens/acabamentos/2.png" alt="">
                        <img src="assets/imagens/acabamentos/3.png" alt="">
                    </div>

                    <div class="col-md-12 col-sm-6 superficie">
                        <h4>Superfície</h4>
                        <img src="assets/imagens/superficies/1.png" alt="">
                        <img src="assets/imagens/superficies/2.png" alt="">
                        <img src="assets/imagens/superficies/3.png" alt="">
                    </div>
                </div>



            </div>
            <div class="col-md-9 col-sm-7 content-pr">

                <div class="row">
                    <div class="col-md-12 campo-personalizacao">
                        <img src="personalizacao/ambiente/Interna_1.jpg" class="back" alt="">
                        <img src="personalizacao/portas/m_clara_17.png" class="door" alt="">
                        <img src="personalizacao/fechaduras/77E17CA.png" class="fechadura" alt="">
                        <img src="personalizacao/macanetas/MA0932000CA00.png" class="macaneta" alt="">

                    </div>
                </div>

                <div class="carousel-modelos">

                    <div class="row">
                        <div class="col-md-12">
                            <div id="Carousel" class="carousel slide">

                                <h2>Modelos disponíveis</h2>

                                <!-- Carousel items -->
                                <div class="carousel-inner">

                                    <div class="item active">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-3 maçaneta-dperso">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/49E16CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0938000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>orbit</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 maçaneta-dperso">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/69E13CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0978000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>oscar</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 maçaneta-dperso">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/81E17CR.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0932000CA00.png" alt="Image">
                                                    </div>
                                                    <h3>Polara</h3>    

                                                </a>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 maçaneta-dperso">
                                                <a href="#" class="prod-thumb">
                                                    <div class="macaneta-montada">
                                                        <img class="fechadura-thumb" src="personalizacao/fechaduras/77E17CA.png" alt="Image">
                                                        <img class="macaneta-thumb" src="personalizacao/macanetas/MA0960000CR00.png" alt="Image">
                                                    </div>
                                                    <h3>Portão</h3>    

                                                </a>
                                            </div>
                                        </div><!--.row-->
                                    </div><!--.item-->

                                    <div class="item">
                                        <div class="row">
                                            <!--<div class="col-md-3">
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/2.png" alt="Image">
                                                    <h3>orbit</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3"> 
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/3.png" alt="Image">
                                                    <h3>oscar</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/4.png">
                                                    <h3>Polara</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/5.png">
                                                    <h3>Quattro</h3>
                                                </a>
                                            </div>-->
                                        </div><!--.row-->
                                    </div><!--.item-->

                                    <div class="item">
                                        <!--<div class="row">
                                            <div class="col-md-3">
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/2.png" alt="Image">
                                                    <h3>orbit</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/3.png" alt="Image">
                                                    <h3>oscar</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/4.png">
                                                    <h3>Polara</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#" class="prod-thumb">
                                                    <img src="assets/imagens/produtos/5.png">
                                                    <h3>Quattro</h3>
                                                </a>
                                            </div>
                                        </div><!--.row-->
                                    </div><!--.item-->

                                </div><!--.carousel-inner-->
                                <a data-slide="prev" href="#Carousel" class="left carousel-control"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                                <a data-slide="next" href="#Carousel" class="right carousel-control"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </div><!--.Carousel-->



                        </div>


                    </div><!--.container-->

                </div>

            </div>
        </div>
    </div>
</div>



<div class="container desenhot">
    <div class="container ">
        <div class="col-md-6">
            <h3>Desenho técnico</h3>
            <img src="assets/imagens/desenhot.png" alt="">
        </div>
        <div class="col-md-6">
            <h3>Informações técnicas</h3> 
            <table class="table table-striped">

                <tbody>
                    <tr>
                        <td>Máquina</td>
                        <td>Todos</td>

                    </tr>
                    <tr>
                        <td>Ambientes</td>
                        <td>Todos</td>

                    </tr>
                    <tr>
                        <td>Acabamento</td>
                        <td>Todos</td>

                    </tr>
                    <tr>
                        <td>Material</td>
                        <td>Todos</td>
                    </tr>
                    <tr>
                        <td>Roseta ou Espelho</td>
                        <td>Todos</td>
                    </tr>
                    <tr>
                        <td>Desenho técnico</td>
                        <td>Download <i class="fa fa-download" aria-hidden="true"></i></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Fim dos Produtos info -->
@push('scripts')


@endpush

@endsection