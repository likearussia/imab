@extends('layouts.site')


@section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li> <a href="{{url('/')}}">home</a> </li>
            <li>a imab</li>                           
            <li>história</li>                           
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-historia">
    <div class="container">
        <h2>{{$historia->titulo}}</h2>

    </div>
</div>

<!-- Fim do Header -->

<div class="container-fluid historia">
    <div class="container">

        <div class="col-md-12">
            {!! $historia->texto !!}

        </div>
    </div>
</div>



<div class="container-fluid timeline">

    <div class="container">
        <div class="col-md-12">
            <h2 class="titulo-timeline">Linha do tempo</h2></div></div>


    <section id="cd-timeline" class="cd-container">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">

                @foreach ($timeline as $tm)

                @if($i % 2 == 0)
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-picture">
                        <p>{{$tm->ano}}</p>
                    </div> <!-- cd-timeline-img -->

                    <div class="cd-timeline-content">
                        <h2>{{ $tm->titulo}}</h2>
                        <p>{{$tm->texto}}</p>
                        <!-- <img src="assets/imagens/50anos.png" alt=""> -->

                    </div> <!-- cd-timeline-content -->

                    <div class="cd-timeline-thumb">
                        <img src="{{url('/')}}/imagens/upload/timeline/{{$tm->caminho}}" alt="">
                    </div>

                </div> <!-- cd-timeline-block -->
                @else
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-movie">
                        <p>{{$tm->ano}}</p>
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-thumb-2">
                        <img src="{{url('/')}}/imagens/upload/timeline/{{$tm->caminho}}" alt="">
                    </div>
                    <div class="cd-timeline-content">
                        <h2>{{ $tm->titulo }}</h2>
                        <p>{{$tm->texto}}</p>

                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->
                @endif
<?php $i++ ?>
                @endforeach
            </div>
        </div>
    </section> <!-- cd-timeline -->
</div>


<!-- Linha do tempo -->




<!-- Fim da Linha do tempo -->
@push('scripts')
<!--<script src="assets/js/modernizr.js"></script>-->
<script src="assets/js/main.js"></script>

@endpush



@endsection
<!-- Linha do tempo -->