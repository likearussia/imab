@extends('layouts.site')

@section('conteudo')
@include('layouts.menu.usuario')

{{ Form::open(['route' => ['usuario::editar', 'id' => $usuario['id']], 'class' => 'form-horizontal form-editar']) }}


    {{ csrf_field() }}
    <label for="#signup__email">Nome</label>
    <input type="text" name="name" class="form__control" id="signup__email" value="{{$usuario['nome']}}" />
    <label for="#signup__email">E-mail</label>
    <input type="text" name="username" class="form__control" id="signup__email" value="{{$usuario['email']}}" />

    <div class="form__group">
        <button class="btn btn__login btnCadastrar" >Cadastrar</button>
    </div>

{!! Form:close() !!}

@endsection