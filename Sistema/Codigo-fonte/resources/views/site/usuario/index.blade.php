@extends('layouts.site')
@section('conteudo')

<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/">home</a> </li>
            <li>Minha conta</li>
        </ul>
    </div>
</div>

@include('shared.valida')

<!--   header-->
<!--    acordeon-->
<div class="container-fluid products">
    <div class="container">
        <div class="row minha-conta">
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    @include('shared.erro-validacao')
                    @include('flash::message')
                </div>
                <div class="row">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
                                        <i class="fa fa-user" aria-hidden="true"></i>Meus Dados<i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            @if(Auth::guard('usuarios_site')->check())
                                                @if(Auth::guard('usuarios_site')->getUser()->tipo == 1)

                                                    <form class="" action="{{ route('usuario::editar', ['id' => Auth::guard('usuarios_site')->getUser()->id]) }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="tipo_usuario" value="{{ Auth::guard('usuarios_site')->getUser()->tipo }}"/>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="alterar-senha">
                                                                    <h4>Dados Pessoais:</h4>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6 col-sm-6 col-xs-12 fields-box" style="padding-bottom: 10px">
                                                            <label for="name">Nome:</label>
                                                            <input type="text" name="name" class="form__control_perfil col-md-12 col-sm-4" id="email" value="{{ Auth::guard('usuarios_site')->getUser()->nome }}" />
                                                        </div>

                                                        <div class="col-md-6 col-sm-6 col-xs-12 fields-box fields-margin" style="padding-bottom: 10px">
                                                            <label for="email">E-mail</label>
                                                            <input type="text" name="username" class="form__control_perfil col-md-12 col-sm-6 _64-email" id="email" value="{{ Auth::guard('usuarios_site')->getUser()->email }}" />
                                                        </div>

                                                        <div class="col-md-8 col-sm-6 col-xs-12 fields-box"  style="padding-bottom: 10px">
                                                            <label for="cidade">Cidade</label>
                                                            <input type="text" name="cidade" class="form__control_perfil col-md-12 col-sm-6" id="cidade" value="{{ Auth::guard('usuarios_site')->getUser()->cidade }}" />
                                                        </div>

                                                        <div class="col-md-4 col-sm-6 col-xs-12 fields-box fields-margin"  style="padding-bottom: 10px">
                                                            <label for="estado">Estado</label>
                                                            <select class="select-field select-perfil" name="estado" id="profissao">
                                                                <option value="estado">Selecione o Estado</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ac") ? "selected" :  "" }} value="ac">Acre</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "al") ? "selected" :  "" }} value="al">Alagoas</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "am") ? "selected" :  "" }} value="am">Amazonas</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ap") ? "selected" :  "" }} value="ap">Amapá</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ba") ? "selected" :  "" }} value="ba">Bahia</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ce") ? "selected" :  "" }} value="ce">Ceará</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "df") ? "selected" :  "" }} value="df">Distrito Federal</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "es") ? "selected" :  "" }} value="es">Espírito Santo</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "go") ? "selected" :  "" }} value="go">Goiás</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ma") ? "selected" :  "" }} value="ma">Maranhão</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "mt") ? "selected" :  "" }} value="mt">Mato Grosso</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ms") ? "selected" :  "" }} value="ms">Mato Grosso do Sul</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "mg") ? "selected" :  "" }} value="mg">Minas Gerais</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pa") ? "selected" :  "" }} value="pa">Pará</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pb") ? "selected" :  "" }} value="pb">Paraíba</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pr") ? "selected" :  "" }} value="pr">Paraná</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pe") ? "selected" :  "" }} value="pe">Pernambuco</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pi") ? "selected" :  "" }} value="pi">Piauí</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rj") ? "selected" :  "" }} value="rj">Rio de Janeiro</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rn") ? "selected" :  "" }} value="rn">Rio Grande do Norte</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ro") ? "selected" :  "" }} value="ro">Rondônia</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rs") ? "selected" :  "" }} value="rs">Rio Grande do Sul</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rr") ? "selected" :  "" }} value="rr">Roraima</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "sc") ? "selected" :  "" }} value="sc">Santa Catarina</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "se") ? "selected" :  "" }} value="se">Sergipe</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "sp") ? "selected" :  "" }} value="sp">São Paulo</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "to") ? "selected" :  "" }} value="to">Tocantins</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-12 col-sm-12 col-xs-12 fields-box" style="padding-bottom: 10px">

                                                            <label for="profissao">Profissão</label>
                                                            <select class="select-field select-perfil" name="profissao" id="profissao">
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->cargo == "Selecione") ? "selected" :  "" }} >Selecione</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->cargo == "Decorador") ? "selected" :  "" }} >Decorador</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->cargo == "Designer de interiores") ? "selected" :  "" }} >Designer de interiores</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->cargo == "Arquiteto") ? "selected" :  "" }} >Arquiteto</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->cargo == "Engenheiro") ? "selected" :  "" }} >Engenheiro</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->cargo == "Construtor") ? "selected" :  "" }} >Construtor</option>
                                                                <option {{ (Auth::guard('usuarios_site')->getUser()->cargo == "Outros") ? "selected" :  "" }} >Outros</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                                <div class="form__group">
                                                                    <input type="submit" value="Enviar"/>
                                                                    <a href="#" data-toggle="modal" data-target="#editpass">Alterar senha</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                @endif


                                                    @if(Auth::guard('usuarios_site')->getUser()->tipo == 3)
                                            <form action="{{ route('usuario::editar', ['id' => Auth::guard('usuarios_site')->getUser()->id]) }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="tipo_usuario" value="3"/>
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="alterar-senha">
                                                            <h4>Revenda:</h4>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 col-sm-4 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                    <label for="segmento">Segmento de atuação</label>
                                                    <select class="select-field select-perfil" id="segmento" name="segmento">
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Selecione") ? "selected" :  "" }}>Selecione</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Ferragem") ? "selected" :  "" }}>Ferragem</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Home Center") ? "selected" :  "" }}>Home Center</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Materiais de construção") ? "selected" :  "" }}>Materiais de construção</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Loja de decoração") ? "selected" :  "" }}>Loja de decoração</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                    <label for="cnpj">CNPJ</label>
                                                    <input type="text" name="cnpj" class="form__control_perfil col-md-12 col-sm-6 mask-cnpj" id="cnpj"  value="{{ Auth::guard('usuarios_site')->getUser()->cnpj}}"/>
                                                </div>

                                                <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                    <label for="razao_social">Razão Social</label>
                                                    <input type="text" name="razao_social" class="form__control_perfil col-md-12 col-sm-6" id="razao_social"  value="{{ Auth::guard('usuarios_site')->getUser()->razao_social}}"/>
                                                </div>

                                                <div class="col-md-8 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                    <label for="fantasia">Nome Fantasia</label>
                                                    <input type="text" name="fantasia" class="form__control_perfil col-md-12 col-sm-6" id="fantasia"  value="{{ Auth::guard('usuarios_site')->getUser()->nome_fantasia }}"/>
                                                </div>

                                                <div class="col-md-4 col-sm-4 fields-box fields-margin" >
                                                    <label for="telefone">Telefone:</label>
                                                    <input type="text" name="telefone" class="form__control_perfil col-md-12 col-sm-4 mask-telefone" id="telefone" value="{{ Auth::guard('usuarios_site')->getUser()->telefone }}" />
                                                </div>

                                                <div class="col-md-8 col-sm-6 fields-box">
                                                    <label for="cidade">Cidade</label>
                                                    <input type="text" name="cidade" class="form__control_perfil col-md-12 col-sm-6" id="cidade" value="{{ Auth::guard('usuarios_site')->getUser()->cidade }}" />
                                                </div>

                                                <div class="col-md-4 col-sm-6 fields-box fields-margin">
                                                    <label for="estado">Estado</label>
                                                    <select class="select-field select-perfil" name="estado" id="profissao">
                                                        <option value="estado">Selecione o Estado</option> 
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ac") ? "selected" :  "" }} value="ac">Acre</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "al") ? "selected" :  "" }} value="al">Alagoas</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "am") ? "selected" :  "" }} value="am">Amazonas</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ap") ? "selected" :  "" }} value="ap">Amapá</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ba") ? "selected" :  "" }} value="ba">Bahia</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ce") ? "selected" :  "" }} value="ce">Ceará</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "df") ? "selected" :  "" }} value="df">Distrito Federal</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "es") ? "selected" :  "" }} value="es">Espírito Santo</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "go") ? "selected" :  "" }} value="go">Goiás</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ma") ? "selected" :  "" }} value="ma">Maranhão</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "mt") ? "selected" :  "" }} value="mt">Mato Grosso</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ms") ? "selected" :  "" }} value="ms">Mato Grosso do Sul</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "mg") ? "selected" :  "" }} value="mg">Minas Gerais</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pa") ? "selected" :  "" }} value="pa">Pará</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pb") ? "selected" :  "" }} value="pb">Paraíba</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pr") ? "selected" :  "" }} value="pr">Paraná</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pe") ? "selected" :  "" }} value="pe">Pernambuco</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pi") ? "selected" :  "" }} value="pi">Piauí</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rj") ? "selected" :  "" }} value="rj">Rio de Janeiro</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rn") ? "selected" :  "" }} value="rn">Rio Grande do Norte</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ro") ? "selected" :  "" }} value="ro">Rondônia</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rs") ? "selected" :  "" }} value="rs">Rio Grande do Sul</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rr") ? "selected" :  "" }} value="rr">Roraima</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "sc") ? "selected" :  "" }} value="sc">Santa Catarina</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "se") ? "selected" :  "" }} value="se">Sergipe</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "sp") ? "selected" :  "" }} value="sp">São Paulo</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "to") ? "selected" :  "" }} value="to">Tocantins</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-12 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                    <label for="fantasia">Site</label>
                                                    <input type="text" name="site" class="form__control_perfil col-md-12 col-sm-6" id="site" value="{{ Auth::guard('usuarios_site')->getUser()->site }}" />
                                                </div>

                                                <div class="col-md-4 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                    <label for="fantasia">Nome do Responsável</label>
                                                    <input type="text" name="name" class="form__control_perfil col-md-12 col-sm-6" id="nomeresponsavel" value="{{ Auth::guard('usuarios_site')->getUser()->nome }}" />
                                                </div>

                                                <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                    <label for="fantasia">Cargo</label>
                                                    <input type="text" name="profissao" class="form__control_perfil col-md-12 col-sm-6" id="cargo" value="{{ Auth::guard('usuarios_site')->getUser()->cargo }}"  />
                                                </div>

                                                <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                    <label for="telefone">Telefone:</label>
                                                    <input type="text" name="celular" class="form__control_perfil col-md-12 col-sm-6 mask-telefone" id="telefone" value="{{ Auth::guard('usuarios_site')->getUser()->celular }}" />
                                                </div>

                                                <div class="col-md-6 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                    <label for="email">E-mail</label>
                                                    <input type="text" name="username" class="form__control_perfil col-md-12 col-sm-6 _64-email" id="email" value="{{ Auth::guard('usuarios_site')->getUser()->email }}" />
                                                </div>

                                                <div class="col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="form__group">
                                                            <input type="submit" value="enviar"/>
                                                            <a data-toggle="modal" data-target="#editpass">Alterar senha</a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>

@endif

                                                    @if(Auth::guard('usuarios_site')->getUser()->tipo == 2)
                                            <form action="{{ route('usuario::editar', ['id' => Auth::guard('usuarios_site')->getUser()->id]) }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="tipo_usuario" value="2"/>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="alterar-senha">
                                                        <h4>Imprensa:</h4>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-sm-4 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                <label for="#signup__email">Tipo de Veículo</label>
                                                <select class="select-field select-perfil" name="segmento">
                                                    <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Selecione") ? "selected" :  "" }}>Selecione</option>
                                                    <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Agência de Noticias") ? "selected" :  "" }}>Agência de Noticias</option>
                                                    <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Internet") ? "selected" :  "" }}>Internet</option>
                                                    <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Jornal / Revista") ? "selected" :  "" }}> Jornal / Revista</option>
                                                    <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Rádio") ? "selected" :  "" }}>Rádio</option>
                                                    <option {{ (Auth::guard('usuarios_site')->getUser()->segmento == "Televisão") ? "selected" :  "" }}>Televisão</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                <label for="razao_social">Empresa / Instituição</label>
                                                <input type="text" name="fantasia" class="form__control_perfil col-md-12 col-sm-6" id="razao_social" value="{{ Auth::guard('usuarios_site')->getUser()->nome_fantasia }}" />
                                            </div>

                                            <div class="col-md-3 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                <label for="telefone">Telefone:</label>
                                                <input type="text" name="telefone" class="form__control_perfil col-md-12 col-sm-6 mask-telefone" id="telefone" value="{{ Auth::guard('usuarios_site')->getUser()->telefone }}" />
                                            </div>


                                            <div class="col-md-6 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                <label for="cidade">Cidade</label>
                                                <input type="text" name="cidade" class="form__control_perfil col-md-12 col-sm-6" id="cidade" value="{{ Auth::guard('usuarios_site')->getUser()->cidade }}"/>
                                            </div>

                                                <div class="col-md-4 col-sm-6 fields-box fields-margin">
                                                    <label for="estado">Estado</label>
                                                    <select class="select-field select-perfil" name="estado" id="profissao">
                                                        <option value="estado">Selecione o Estado</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ac") ? "selected" :  "" }} value="ac">Acre</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "al") ? "selected" :  "" }} value="al">Alagoas</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "am") ? "selected" :  "" }} value="am">Amazonas</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ap") ? "selected" :  "" }} value="ap">Amapá</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ba") ? "selected" :  "" }} value="ba">Bahia</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ce") ? "selected" :  "" }} value="ce">Ceará</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "df") ? "selected" :  "" }} value="df">Distrito Federal</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "es") ? "selected" :  "" }} value="es">Espírito Santo</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "go") ? "selected" :  "" }} value="go">Goiás</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ma") ? "selected" :  "" }} value="ma">Maranhão</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "mt") ? "selected" :  "" }} value="mt">Mato Grosso</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ms") ? "selected" :  "" }} value="ms">Mato Grosso do Sul</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "mg") ? "selected" :  "" }} value="mg">Minas Gerais</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pa") ? "selected" :  "" }} value="pa">Pará</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pb") ? "selected" :  "" }} value="pb">Paraíba</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pr") ? "selected" :  "" }} value="pr">Paraná</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pe") ? "selected" :  "" }} value="pe">Pernambuco</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "pi") ? "selected" :  "" }} value="pi">Piauí</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rj") ? "selected" :  "" }} value="rj">Rio de Janeiro</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rn") ? "selected" :  "" }} value="rn">Rio Grande do Norte</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "ro") ? "selected" :  "" }} value="ro">Rondônia</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rs") ? "selected" :  "" }} value="rs">Rio Grande do Sul</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "rr") ? "selected" :  "" }} value="rr">Roraima</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "sc") ? "selected" :  "" }} value="sc">Santa Catarina</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "se") ? "selected" :  "" }} value="se">Sergipe</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "sp") ? "selected" :  "" }} value="sp">São Paulo</option>
                                                        <option {{ (Auth::guard('usuarios_site')->getUser()->estado == "to") ? "selected" :  "" }} value="to">Tocantins</option>
                                                    </select>
                                                </div>
                                            <div class="col-md-12 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                <label for="fantasia">Site</label>
                                                <input type="text" name="site" class="form__control_perfil col-md-12 col-sm-6" id="nomeresponsavel" value="{{ Auth::guard('usuarios_site')->getUser()->site }}" />
                                            </div>

                                            <div class="col-md-4 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                <label for="fantasia">Nome do Responsável</label>
                                                <input type="text" name="name" class="form__control_perfil col-md-12 col-sm-6" id="nomeresponsavel" value="{{ Auth::guard('usuarios_site')->getUser()->nome }}"/>
                                            </div>

                                            <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                <label for="fantasia">Cargo</label>
                                                <input type="text" name="profissao" class="form__control_perfil col-md-12 col-sm-6" id="cargo" value="{{ Auth::guard('usuarios_site')->getUser()->cargo }}"/>
                                            </div>profissao

                                            <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                <label for="telefone">Telefone:</label>
                                                <input type="text" name="celular" class="form__control_perfil col-md-12 col-sm-6 mask-telefone" id="telefone" value="{{ Auth::guard('usuarios_site')->getUser()->celular }}" />
                                            </div>
                                            <!--<div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="alterar-senha">
                                                        <h4>Trocar Senha:</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-6 fields-box" style="padding-bottom: 10px">
                                                <label for="senha">Nova Senha</label>
                                                <input type="password" name="password" class="form__control_perfil col-md-12 col-sm-6 _64-senha" id="senha" />    
                                            </div>
                                            <div class="col-md-4 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                <label for="conf-senha">Confirmar Nova Senha</label>
                                                <input type="password" name="password_confirmation" class="form__control_perfil col-md-12 col-sm-6 _64-senha" id="conf-senha" />    
                                            </div>-->
                                                <div class="col-md-6 col-sm-6 fields-box fields-margin" style="padding-bottom: 10px">
                                                    <label for="email">E-mail</label>
                                                    <input type="text" name="username" class="form__control_perfil col-md-12 col-sm-6 _64-email" id="email" value="{{ Auth::guard('usuarios_site')->getUser()->email }}" />
                                                </div>
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="form__group">
                                                            <button class="btn btnEnviar">Salvar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                                    @endif
                                                @endif
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fa fa-check" aria-hidden="true"></i><span class="txt-conjunto">Conjuntos </span>Personalizados<i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                <div class="tabela-pers">
                                    <table class="table table-bordered">
                                        <tbody><tr>
                                                <td width="2%" align="center" class="tb-numero"><span>#</span></td>
                                                <td width="30%" class="tb-modelo"><span>Modelo</span></td>
                                                <td width="12%" class="tb-codigo"><span>Código</span></td>
                                                <td width="12%" class="tb-material"><span>Material</span></td>
                                                <td width="12%" class="tb-ambiente"><span>Ambiente</span></td>
                                                <td width="12%" class="tb-acabamento"><span>Acabamento</span></td>
                                                <td width="12%" class="tb-superficie"><span>Superfície</span></td>
                                                <td width="4%" align="center" class="tb-down"><span><i class="fa fa-download" aria-hidden="true"></i></span></td>
                                                <td width="4%" align="center" class="tb-exlcuir"><span><i class="fa fa-ban" aria-hidden="true"></i></span></td>
                                            </tr>
                                            <tr>
                                                <td width="2%" align="center" class="tb-numero"><span>1</span></td>
                                                <td width="30%" class="tb-modelo"><span>Orbit</span></td>
                                                <td width="12%" class="tb-codigo"><span>60</span></td>
                                                <td width="12%" class="tb-material"><span>Latão</span></td>
                                                <td width="12%" class="tb-ambiente"><span>Banheiro</span></td>
                                                <td width="12%" class="tb-acabamento"><span>Prata</span></td>
                                                <td width="12%" class="tb-superficie"><span>Madeira</span></td>
                                                <td width="4%" align="center" class="tb-down"><span><i class="fa fa-download" aria-hidden="true"></i></span></td>
                                                <td width="4%" align="center" class="tb-exlcuir"><span><i class="fa fa-ban" aria-hidden="true"></i></span></td>
                                            </tr>
                                            <tr>
                                                <td width="2%" align="center" class="tb-numero"><span>2</span></td>
                                                <td width="30%" class="tb-modelo"><span>Cosmus</span></td>
                                                <td width="12%" class="tb-codigo"><span>90</span></td>
                                                <td width="12%" class="tb-material"><span>Zamak</span></td>
                                                <td width="12%" class="tb-ambiente"><span>Interno</span></td>
                                                <td width="12%" class="tb-acabamento"><span>Bronze</span></td>
                                                <td width="12%" class="tb-superficie"><span>Metal</span></td>
                                                <td width="4%" align="center" class="tb-down"><span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span></td>
                                                <td width="4%" align="center" class="tb-exlcuir"><span><i class="fa fa-ban" aria-hidden="true"></i></span></td>
                                            </tr>

                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fa fa-star" aria-hidden="true"></i>Lista de Desejos<i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-10 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-5 col-xs-12 campo-personalizacao lista-desejos">
                                                    <img src="personalizacao/portas/m_clara_17.png" class="door" alt="">
                                                    <img src="personalizacao/fechaduras/77E17CA.png" class="fechadura" alt="">
                                                    <img src="personalizacao/macanetas/MA0932000CA00.png" class="macaneta" alt="">

                                                </div>
                                                <div class="col-md-8 col-sm-6 col-xs-10 lista-dados">
                                                    <div class="row">
                                                        <span>Nome:&nbsp;</span>
                                                        <h3>Cosmus</h3>
                                                        <p>Código: 60</p> 
                                                    </div>
                                                    <div class="row">
                                                        <span>Material:&nbsp;</span>
                                                        <h4>Zamak</h4>
                                                    </div>
                                                    <div class="row">
                                                        <span>Ambiente:&nbsp;</span>
                                                        <h4>Interno</h4>
                                                    </div>
                                                    <div class="row">
                                                        <span>Acabamento / Superfície:&nbsp;</span><br>
                                                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/acabamentos/1.png" alt=""></button>
                                                        &nbsp;
                                                        <button type="button" class="superficie" data-toggle="tooltip" data-placement="top" title="Madeira"><img src="assets/imagens/superficies/1.png" alt=""></button>
                                                    </div>
                                                </div>
                                                <div class="col-md-1 col-sm-1 col-xs-2 excluir-lista">
                                                    <a href="#"><i class="fa fa-ban" aria-hidden="true"></i> </a>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>Contato e Ajuda<i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="row contato-conta">
                                                <h4>Entre em Contato</h4>
                                                <h4><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; (11) 4662 7500</h4>
                                                <h4><i class="fa fa-fax" aria-hidden="true"></i>&nbsp;(11) 4661 2149</h4>
                                                <h4><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;imab@imab.com.br</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="row duvida-conta">
                                                <a href="http://10.91.191.74:4000/duvidas">
                                                    <i class="fa fa-commenting" aria-hidden="true"></i>
                                                    <p>TIRE SUAS <br> DÚVIDAS</p></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="editpass">
    <div class="modal-dialog" role="document">
        <div class="modal-content senha-alt">
            <!-- Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title"> Alterar senha</h2>

            </div>

            <!-- Body -->
            <div class="modal-body">
                <div class="errors">
                    <ul>

                    </ul>
                </div>
                <div class="success">
                    <ul>

                    </ul>
                </div>
                {{ Form::open(['route' => ['usuario::alterar-senha', 'id' => $usuario['id']], 'class' => 'form-horizontal form-senha']) }}
                {{ csrf_field() }}
                <label for="#signup__password">Senha atual</label>
                <input type="password" name="password" class="form__control _64-senha" id="signup__password" />
                <label for="#signup__password">Nova senha</label>
                <input type="password" name="newpassword" class="form__control _64-senha" id="signup__password" />
                <label for="#signup__password">Confirme a nova senha</label>
                <input type="password" name="newpassword_confirmation" class="form__control _64-senha" id="signup__password" />

                <div class="form__group">
                    <button class="btn btn__login btnCadastrar" >Alterar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection