@extends('layouts.site')
@section('conteudo')

    <div class="container-fluid breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">home</a> </li>
                <li>Recuperar senha</li>
            </ul>
        </div>
    </div>



    <div class="container-fluid contact-form">
        <div class="container">
            @include('shared.erro-ajax')
            @include('shared.erro-validacao')
            @include('flash::message')
            <div class="errors">
                <ul>

                </ul>
            </div>
            <div class="success">
                <ul>

                </ul>
            </div>
            <form class="form-recuperarsenha" action="{{ route('site::novasenha') }}" method="post">
                {{ csrf_field() }}
                <div class="col-md-12 col-sm-12 col-xs-12 fields-box" style="padding-bottom: 10px">
                    <h4 class="new-senha">Crie uma nova senha para sua conta</h4>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 fields-box" style="padding-bottom: 10px">
                    <label for="name" class="white">Nova senha:</label>
                    <input type="password" name="password" class="form__control_perfil input-senha col-md-12 col-sm-4" id="email" value="" />
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 fields-box" style="padding-bottom: 10px">
                    <label for="name" class="white">Confirme a nova senha:</label>
                    <input type="password" name="password_confirmation" class="form__control_perfil input-senha col-md-12 col-sm-4" id="email" value="" />
                </div>
                <input type="hidden" name="id" value="{{ $id }}">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="form__group">
                            <input class="rec-senha" type="submit" value="Enviar"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


    @endsection
