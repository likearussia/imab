@extends('layouts.site')
@section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li>  <a href="{{url('/')}}">home</a></li>                         
            <li>Arquivos Digitais IMAB</li>                           
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-valores">
    <div class="container">
        <h2>Arquivos Digitais IMAB</h2>
        <p>
            Aqui estão disponíveis diversos arquivos para download.</p>
    </div>
</div>

<!-- Fim do Header -->

@include('shared.valida');

<!-- Início formulário de contato -->
<div class="container-fluid contact-form">
    <div class="container">
        <div class="tabs-down">


            @if(Auth::guard('usuarios_site')->check())
                @if(Auth::guard('usuarios_site')->getUser()->tipo == 2 || Auth::guard('usuarios_site')->getUser()->tipo == 3)
                    <button class="btn-down btn_um">Todos</button>
                    @if(Auth::guard('usuarios_site')->getUser()->tipo == 2)
                        <button class="btn-down btn_dois">Imprensa</button>
                    @elseif(Auth::guard('usuarios_site')->getUser()->tipo == 3)
                        <button class="btn-down btn_tres">Revendedor</button>
                    @endif
                @endif
            @endif
        </div>
        <div class="col-md-12">
            @foreach($downloads as $download)
            <?php $extensao = explode('.', $download->nome) ?>
                <?php
                $type = json_decode($download->usuarios);

                    if($type[0] == 1){
                        $class = "d_um";
                    }elseif($type[0] == 2 and empty($type[1])){
                        $class = "d_dois";
                    }elseif($type[0] == 3){
                        $class = "d_tres";
                    }elseif($type[0] == 2 and $type[1] == 3){
                        $class = "d_combo";
                    }

                ?>
            <div class="box-download {{ $class }} col-md-3 col-sm-6 col-xs-12">
                <div class="download-topo col-md-12 col-sm-12 col-xs-12">
                    <img src="/assets/imagens/icones/{{ end($extensao) }}.svg" />
                    <p class="col-md-8 col-sm-6 col-xs-8">

                        <span class="span-dados"><b>Nome:</b> {{ $download->titulo }}</span>
                        <span class="span-dados"><b>Tipo:</b> {{ end($extensao) }}</span>
                    </p>
                    <p class="txt-download">{{ $download->descricao }}</p>
                </div>
                <a href="{{ route('baixar', ['arquivo' => $download->caminho]) }}">
                    <div class="btn-download col-md-12">
                        <img src="/assets/imagens/icones/down-arrow.svg"/>
                        <span>download</span>
                    </div>    
                </a>

            </div>



            @endforeach
        </div>
    </div>
</div>

<!-- Fim do formulário -->

@push('scripts')
<script src="js/site.js"></script>

@endpush

@endsection
