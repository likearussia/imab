@extends('layouts.site')

@section('conteudo')

  <!-- Breadcrumbs -->
    <div class="container-fluid breadcrumb">
       <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">home</a> </li>							
            <li><a href="{{url('dicas-e-solucoes')}}">dicas e soluções</a></li>	
            <li>single dica 2</li>						
        </ul>
        </div>
    </div>
    
    <!-- Fim do breadcrumb -->
    <!-- Inicio Header -->
    <div class="container-fluid header-valores">
        <div class="container">
            <h2>IMAB lança dobradiças convencionais em inox</h2>
         
        </div>
    </div>
    
    <!-- Fim do Header -->
     <!-- Masonry Grid -->
     <div class="container-fluid single">
        <div class="container">
          <div class="row">
             <div class="col-md-9 single-dica-post">

				<p>Dobradiças frágeis ou com problemas podem ser um incômodo, além de representar perigo para a segurança, por isso é importante investir neste item indispensável o qual é uma parte essencial das portas e janelas. E para auxiliar nos projetos, a IMAB amplia o catálogo de produtos com o lançamento das dobradiças em inox que podem ser aplicadas em portas de madeira internas e externas.</p>

        <img src="assets/imagens/porta2.jpg" alt=""> 

				<p>As dobradiças de duas abas do tipo convencional 208, 209 e 242 são fabricadas em 100% aço inox, têm cabeças chatas e estão disponíveis com cantos retos ou arredondados. Os modelos 208 e 209 suportam o peso de até 25 kg levando em conta o jogo com três peças, já o 242 aguenta até 35 kg. Todas são encontradas nos acabamentos de inox polido e escovado.</p>

				<p>Engana-se quem pensa que as dobradiças estão restritas somente às portas e janelas. Elas também são aplicadas em armários de cozinhas e baús, entre outros objetos, e as dobradiças em inox da IMAB se encaixam perfeitamente em vários projetos, pois são duráveis e estilosas.</p>

				<p>Caso a dobradiça seja instalada em portas externas ou acessos para garagens, pátios ou varandas, o aço inoxidável é indicado, pois resistente bem à corrosão. Outro detalhe importante a ser analisado, é o tamanho da dobradiça, deve-se levar em conta os seguintes aspectos: altura, peso, largura e a espessura da porta.</p>

				<p>E como descobrir que tipo de dobradiça é a mais adequada para a aplicação? A IMAB sugere que o cliente final efetue pesquisas a respeito, lembrando que as dobradiças brasileiras são normatizadas pela ABNT - Associação Brasileira de Normas Técnicas, onde o consumidor pode ver qual o peso que a associação solicita por medida, tornando assim mais fácil a utilização do produto correto. Também é válido contar com a opinião de um profissional instalador para obter um melhor desempenho.</p>

				<p>Além disso, certifique-se de lubrificar todas as partes móveis algumas vezes por ano, isso irá garantir que elas continuem a mover-se suavemente. A lubrificação deve ocorrer de acordo com o local que foi instalado o produto, se tiver muita poeira ou exposição direta ao tempo a redução de atrito deve ser mais constante, a fim de preservar o objeto e manter o leve deslizar no funcionamento.  Caso a dobradiça seja instalada em área residencial, a cada seis meses pode ser feita a lubrificação, sempre através de grafite em pó, em último caso passar óleo anticorrosivo.</p>

				<p>Se não forem lubrificadas, as dobradiças podem deteriorar-se e tornar-se difícil de operar, eventualmente, conduzindo à possibilidade de parar de trabalhar. Isso é muito importante, principalmente em áreas costeiras, pois o sal do mar auxilia no acúmulo de ferrugem.</p>


 
             </div>

        <div class="col-md-3">
               <div class="row tags">
                    <h3><i class="fa fa-tags" aria-hidden="true"></i> Tags</h3>
                    <div class="result-tag">
                    <ul>
                      <li>Maçanetas,</li>
                      <li>Ambiente</li>
                    </ul>
                      
                    </div>
               </div>
               <div class="row date">
                    <h3><i class="fa fa-clock-o" aria-hidden="true"></i> Postado em</h3>
                    <div class="result-data">
                    <p>Dia 10 de Agosto de 2016</p>
                    </div>
               </div>
               <div class="row share">
                    <h3><i class="fa fa-share-alt" aria-hidden="true"></i> Compartilhar</h3>
                    <div class="result-share">
                    <ul>
                      <li><a href="https://www.facebook.com/sharer.php?u=http://www.imab.com.br/">
                      <i class="fa fa-facebook" aria-hidden="true"></i> Compartilhar no facebook</a> </li>
                      <li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium&original_referer=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium"><i class="fa fa-twitter" aria-hidden="true"></i> Compartilhar no twitter</a>  </li>
                      <li><a href="https://www.instagram.com/imabfechaduras/"><i class="fa fa-instagram" aria-hidden="true"></i> Compartilhar no instagra,</a>  </li>
                      <li><a href="https://br.pinterest.com/imabfechaduras/"><i class="fa fa-pinterest" aria-hidden="true"></i> Compartilhar no pinterest</a>  </li>
                      <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Enviar por email</a> </li>
                    </ul>
                    </div>
               </div>
   </div>
   </div>


   </div>
   </div>

@endsection