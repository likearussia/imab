@extends('layouts.site')


@section('conteudo')

<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li>  <a href="{{url('/')}}">home</a></li>                         
            <li>fale conosco</li>                           
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-valores">
    <div class="container">
        <h2>Fale conosco</h2>
        <p>
            Entre em contato diretamente com a IMAB enviando suas dúvidas, sugestões e opiniões sobre nossos produtos e serviços.</p>
    </div>
</div>

<!-- Fim do Header -->

<!-- Início formulário de contato -->
<div class="container-fluid contact-form">
    @include('shared.erro-validacao')
@include('shared.mensagem')
@include('flash::message')
    <div class="container">
        <div class="col-md-8 col-sm-12 col-xs-12 formulario">

            <div class="row"><p>Preencha o formulário abaixo e mande sua mensagem<br>
                    Todos os campos marcados com asterisco <font>(*)</font> são de preenchimento obrigatório.</p></div>
            {{ Form::open(['route' => 'site::faleconosco']) }}

            <div class="row">
                {{ csrf_field() }}
                <div class="col-md-6 col-sm-6 col-xs-10">
                    <label><font>* </font>Segmento</label>
                    <label class="lab">
                        <select name="segmento" title="Segmento">
                            <option>Selecione</option>
                            <option>Construtora</option>
                            <option>Consumidor final</option>
                            <option>Indústria</option>
                            <option>Representante</option>
                            <option>Revenda</option>
                        </select>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <label for="" class="lab2"><font>* </font> Nome</label>
                    <input name="nome" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12"><label for="" class="lab2"><font>* </font>Telefone</label><input name="telefone"class="mask-telefone"  type="tel"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label for="" class="lab2"><font>* </font> Email</label>
                    <input name="email"  type="email">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <label for="" class="lab2"><font>* </font>CEP</label>
                    <input name="cep" type="text" maxlength="8" class="cep">
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <label for="" class="lab2 end-fc"><font >* </font>Endereço</label>
                    <input name="endereco" type="text" class="endereco">
                </div>

            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-4">
                    <label for="" class="lab2"><font>* </font>Número</label><input name="numero" type="text">
                </div>
                <div class="col-md-3 col-sm-3 col-xs-8">
                    <label for="" class="lab2">Complemento</label><input type="text" name="complemento">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label for="" class="lab2"><font>* </font>Bairro</label><input name="bairro" type="text" class="bairro">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label for="" class="lab2"><font>* </font>Estado</label>
                    <input type="text" name="estado" class="estado">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label for="" class="lab2"><font>* </font>Cidade</label>
                    <input type="text" name="cidade" class="cidade">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12"><label for="" class="lab2"><font>* </font>Atividade</label>
                    <label class="lab">
                        <select name="atividade">
                            <option value="">Selecione</option>
                            <option>Contrutora</option>
                            <option>Consumidor final</option>
                            <option>Indústria</option>
                            <option>Representante</option>
                            <option>Revenda</option>
                        </select>
                    </label>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label for="" class="lab2">Como conheceu a IMAB?</label>
                    <label class="lab">
                        <select name="conheceu">
                            <option value="">Selecione</option>
                            <option value="feiras">Feiras e Eventos</option>
                            <option value="indicacao">Indicação de Amigos</option>
                            <option value="internet">Internet</option>
                            <option value="revista">Revista</option>
                        </select>
                    </label>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12"><label for="" class="lab2">Mensagem</label><textarea name="mensagem" id="" cols="30" rows="10"></textarea></div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 email-label">
                    <label for="">Deseja receber notícias por e-mail?</label>
                    <br>
                    <ul class="subscribe">
                        <li class="list__item">
                            <input type="radio" class="radio" checked  name="news" id="yes">
                            <label class="label--radio" for="yes">
                                Sim
                            </label>
                        </li>
                        <li class="list__item">
                            <input type="radio" class="radio" name="news" id="no">
                            <label class="label--radio" for="no">
                                Não
                            </label>
                        </li>



                    </ul>
                </div>
            </div>
            <p> </p>
            <div class="row">
                <div class="col-md-12 col-sm-12"><input type="submit" value="Enviar"></div>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div id="faleconosco-right">
                <div class="faleconosco-right">
                    <h3>TRABALHE CONOSCO</h3>
                    <p>Faça parte da nossa equipe, <a href="{{ route('trabalhe::index') }}">clique aqui</a> e conheça nossas vagas.</p>
                </div>
                <div>
                    <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14598.31331374716!2d-46.8464326!3d-23.8335891!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xca5410506318d005!2sImab+Fechaduras!5e0!3m2!1spt-BR!2sbr!4v1473952198753" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12 form-contact">
                    <h3>Contato</h3>
                    <h4><i class="fa fa-phone" aria-hidden="true"></i> &ensp; (11) 4662 7500</h4>
                    <h4><i class="fa fa-fax" aria-hidden="true"></i> &ensp;(11) 4661 2149</h4>
                    <h4><i class="fa fa-envelope-o" aria-hidden="true"></i> &ensp;imab@imab.com.br</h4>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12 form-end">
                    <h3>Endereço</h3>
                    <h4>Estrada Mina de Ouro, 280 - Itararé
                    <span>Embu Guaçu/SP - Brasil - CEP 06900.000</span></h4>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Fim do formulário -->

@push('scripts')
<script src="js/site.js"></script>

@endpush

@endsection

