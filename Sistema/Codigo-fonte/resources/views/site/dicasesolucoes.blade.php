@extends('layouts.site')

@section('conteudo')


<!-- Breadcrumbs -->
<div class="container-fluid breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li>  <a href="{{url('/')}}">home</a> </li>                         
            <li>dicas e soluções</li>                           
        </ul>
    </div>
</div>

<!-- Fim do breadcrumb -->
<!-- Inicio Header -->
<div class="container-fluid header-valores">
    <div class="container">
        <h2>Dicas e soluções</h2>
        <p>
            Inspire-se e escolha os melhores produtos e as soluções para o seu projeto.</p>
    </div>
</div>

<!-- Fim do Header -->
<!-- Masonry Grid -->
<div class="container-fluid grid-dicas">
    <div class="container">
        <div class="row">

            <div class="col-md-3 col-sm-4 col-xs-12 filtro-dica" >

                <div class="row busca-dicas">
                    <div class="col-md-12 col-xs-12">
                        <h4>Pesquisar</h4>
                        <form method="post" action="{{ route('dicas::buscar') }}">
                            {{ csrf_field() }}
                            <input type="text" name="busca">
                        </form>
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12" >
                           <a role="button" data-toggle="collapse" href="#collapseCategoria" aria-expanded="false" aria-controls="collapseCategoria"><h4><i class="fa fa-expand" aria-hidden="true"></i> Categorias</h4></a>
                        <ul class="categoriaprod collapse" id="collapseCategoria">
                            @foreach($categorias as $categoria)
                            <li><a href="">{{$categoria->nome}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <a role="button" data-toggle="collapse" href="#collapseRecentes" aria-expanded="false" aria-controls="collapseRecentes"><h4><i class="fa fa-expand" aria-hidden="true"></i> Mais Recentes</h4></a>
                        <ul class="categoriaprod collapse" id="collapseRecentes">
                            @foreach($recentes as $recente)
                            <li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="{{route('dicas::visualizar',['slug' => $recente->slug, 'id' => $recente->id])}}">{{$recente->titulo}}</a></li>
                            @endforeach

                        </ul>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <h4>Redes sociais</h4>
                        <ul class="sociais">
                            <li><a href="https://www.facebook.com/sharer.php?u=http://www.imab.com.br/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium&original_referer=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/imabfechaduras/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                            <li><a href="https://br.pinterest.com/imabfechaduras/" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>

                        </ul>
                    </div>
                </div>



            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                @if($quantidade == 0)
                    <p style="color: #fff">não foram encontrados resultados para sua pesquisa.</p>
                @else
                    @foreach($dicas as $dica)
                        @if($dica->categoriastatus != 0)
                            @if($dica->status != 0)
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 dicas">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a href="{{route('dicas::visualizar',['slug' => $dica->slug, 'id' => $dica->id])}}">
                                                <img src="{{$upDir}}/{{$dica->miniatura}}" alt="">
                                            </a>
                                        </div>
                                        <div class="panel-body">
                                            <h3>{{$dica->nomecategoria}}</h3>
                                            <a href="{{route('dicas::visualizar',['slug' => $dica->slug, 'id' => $dica->id])}}"><h2>{{$dica->titulo}}</h2></a>

                                                <p> {{  $dica->resumo}} </p>

                                            <a href="{{route('dicas::visualizar',['slug' => $dica->slug, 'id' => $dica->id])}}" class="bto-dicas">Leia mais</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
        <div class="paginacao text-right paginaca-dicas">
            {!! $dicas->links() !!}
        </div>
    </div>
</div>

<style>
    .paginaca-dicas ul li a, .paginaca-dicas ul li span {
        padding: 0;
        background-color: transparent!important;
        border: 0;
        color: #bcbcbc!important;
        width: 15px
    }

    .panel-heading {
        min-height: 166px;
    }
</style>
<!-- FIM DO MASONRY -->
@endsection