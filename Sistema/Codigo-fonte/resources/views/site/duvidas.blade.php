@extends('layouts.site')


@section('conteudo')

<!-- Breadcrumbs -->
    <div class="container-fluid breadcrumb">
       <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">home</a> </li>                         
            <li>dúvidas frequentes</li>                         
        </ul>
        </div>
    </div>
    
    <!-- Fim do breadcrumb -->
    <!-- Inicio Header -->
    <div class="container-fluid header-valores">
        <div class="container">
            <h2>Dúvidas frequentes</h2>
            <p>Reunimos aqui algumas dúvidas mais comuns de nossos clientes. Se não encontrar a sua resposta, mande um e-mail para assistencia@imab.com.br. Teremos prazer em atendê-lo.</p>
        </div>
    </div>
    
    <!-- Fim do Header -->
     <!-- Box de dúvidas -->
    
        <!-- Produto info -->
    <div class="container-fluid duvidas-info">
        <div class="infos">
            <div class="container">
            <div class="box-duvida">
            <div class="custom-interna">
            <div class="panel-group" id="accordion_reg" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne_reg">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseOne_reg" aria-expanded="true" aria-controls="collapseOne_reg">
                     Escolhendo a melhor fechadura para sua casa.
                    </a>
                  </h4>
                </div>
                <div id="collapseOne_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_reg">
                  <div class="panel-body">
                  <p> A espessura da porta escolhida determinará o limite máximo da espessura da fechadura.</p>
                  <p> Normalmente, a diferença entre a espessura da porta e a largura da chapa-testa deve ser em torno de 10 mm (5mm para cada lado), uma diferença menor enfraquecerá a porta. </p>
                  <p>  Algumas medidas devem ser consideradas para definir a instalação e, em caso de reposição, considerar também o tamanho da antiga fechadura. </p>  

                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseTwo_reg" aria-expanded="false" aria-controls="collapseTwo_reg">
                      O que é chapa-testa?
                    </a>
                  </h4>
                </div>
                <div id="collapseTwo_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_reg">
                  <div class="panel-body">
                    Disposta frontalmente à máquina da fechadura, é esta medida que indica a altura e largura da caixa da fechadura.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseThree_reg" aria-expanded="false" aria-controls="collapseThree_reg">
                      O que é distância de broca?
                    </a>
                  </h4>
                </div>
                <div id="collapseThree_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_reg">
                  <div class="panel-body">
                    <p>É a medida correspondente entre o centro do cilindro e a chapa testa da fechadura. As medidas mais usadas são: 40 mm, 45 mm, 55 mm e 70 mm.</p>
                    
                    <p><strong>Atenção:</strong> só recomendamos o uso de maçanetas bola em fechaduras com distância de broca a partir de 55 mm, pois, numa distância menor, a maçaneta ficará muito encostada no batente da porta, dificultando o acionamento.</p>
                  </div>
                </div>
              </div>
              
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseFour_reg" aria-expanded="false" aria-controls="collapseFour_reg">
                      Como verifico o lado de abertura da porta? 
                    </a>
                  </h4>
                </div>
                <div id="collapseFour_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_reg">
                  <div class="panel-body">
                 O lado de abertura é sempre determinado olhando-se a porta pelo lado de fora. Se a dobradiça ficar do lado esquerdo da porta, ela se abrirá para a esquerda. Se a dobradiça ficar do lado direito da porta, ela se abrirá para a direita. Esta informação é imprescindível para determinar se será necessário realizar a reversão do trinco da fechadura.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseFive_reg" aria-expanded="false" aria-controls="collapseFive_reg">
                     Como realizar a limpeza e manutenção das fechaduras IMAB?
                    </a>
                  </h4>
                </div>
                <div id="collapseFive_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive_reg">
                  <div class="panel-body">
                   Para limpeza das maçanetas e guarnições, use flanela limpa e umedecida em água e sabão neutro, e seque com flanela seca e limpa. É aconselhável o uso de grafite em pó para lubrificação do cilindro a cada 6 (seis) meses, e, para melhor performance, lubrificar a parte interna da fechadura com graxa a cada 1 (um) ano. Em caso de instalação em locais com pouco uso ou regiões de maresia, é necessária limpeza a cada 4 (quatro) semanas, em razão do acúmulo de sujeira e sal, que prejudicam a durabilidade do revestimento.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSix_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseSix_reg" aria-expanded="false" aria-controls="collapseSix_reg">
                     Como proceder em caso de reclamações sobre as fechaduras IMAB?
                    </a>
                  </h4>
                </div>
                <div id="collapseSix_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix_reg">
                  <div class="panel-body">
                   Recomendamos que guarde o certificado de garantia e a nota fiscal de compra em um lugar seguro. Uma cópia da nota fiscal será necessária quando você solicitar um serviço de assistência técnica. Para mais informações, entre em contato com a Assistência Técnica IMAB no e-mail assistencia@imab.com.br.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSeven_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseSeven_reg" aria-expanded="false" aria-controls="collapseSeven_reg">
                     Como faço a instalação da fechadura?
                    </a>
                  </h4>
                </div>
                <div id="collapseSeven_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven_reg">
                  <div class="panel-body">
                   As fechaduras IMAB são projetadas de modo que qualquer pessoa possa instalá-las com facilidade, desde que respeitadas as instruções contidas nas embalagens de nossos produtos.
Ao instalar uma fechadura, busque sempre alinhar bem as peças correspondentes, de modo que o trinco feche livremente quando a porta é fechada.
                      <h5>Ferramentas necessárias:</h5>
Na maioria das fechaduras, para fazer a furação na porta, será necessária uma furadeira (e acessórios), broca e formão para acabamento, e um conjunto de chaves de fenda, Philips e Allen para instalação. Consulte sempre as instruções de instalação na embalagem do produto para se certificar de que dispõe das ferramentas necessárias.
As portas que já vem com a furação própria facilitam a instalação. 
Observação: indicamos sempre que a instalação seja feita por um profissional habilitado!

                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingEight_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseEight_reg" aria-expanded="false" aria-controls="collapseEight_reg">
                    É possível usar somente uma única chave para abrir todas as minhas portas?
                    </a>
                  </h4>
                </div>
                <div id="collapseEight_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight_reg">
                  <div class="panel-body">
                      <p>Sim, na maioria dos casos, é possível usar somente uma chave em todas as fechaduras. Esse processo chama-se Mestragem. Para isso, é necessário que as fechaduras sejam do mesmo tipo (todas externas), e o cilindro precisa ter o mesmo número de pinos.</p>
                      <p>O Sistema de Mestragem é indicado para hotéis, residências, lojas, edifícios, escolas, hospitais, empresas, bancos ou onde mais for necessário restringir ou facilitar o acesso.</p>
                      <p>O Planejamento de um Sistema de Mestragem define quais e quantas chaves acionam determinados cilindros dentro deste sistema.</p>

                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingNine_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseNine_reg" aria-expanded="false" aria-controls="collapseNine_reg">
                     Garantia IMAB.
                    </a>
                  </h4>
                </div>
                <div id="collapseNine_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine_reg">
                  <div class="panel-body">
                      <p>A IMAB fabrica produtos de alta segurança, qualidade e sofisticação, garantindo o perfeito funcionamento dos produtos por ela produzidos contra defeitos de fabricação. Desta forma, você deve ficar atento aos prazos de garantia indicados no Termo que acompanha cada embalagem.</p>
<p>A garantia não se aplica a quaisquer defeitos alheios à fabricação e, especialmente, quando tais defeitos forem provenientes de:</p>
 <ul class="garantia">
     <li>- Não cumprimento das instruções de instalação e conservação;</li>
     <li>- Tentativas de arrombamento;</li>
     <li>- Reparos ou intervenções de terceiros;</li>
     <li>- Uso de peças não originais adaptadas ao produto;</li>
     <li>- Uso de chaves não originais IMAB;</li>
     <li>- Desgaste natural em razão do uso.</li>
 </ul>






                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTen_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseTen_reg" aria-expanded="false" aria-controls="collapseTen_reg">
                     Grau de resistência à corrosão, frequência de uso e grau de segurança.
                    </a>
                  </h4>
                </div>
                <div id="collapseTen_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen_reg">
                  <div class="panel-body">
                  <p>Antes de especificar um produto, verifique sua classificação segundo as tabelas abaixo: </p>
{SOLICITAR TABELA}
                  <p>Segundo a norma brasileira ABNT nº 14.913, peças que resistem a mais de 144 horas de câmara de névoa salina estão aptas ao uso em áreas litorâneas, como mostra a tabela acima. Para o melhor entendimento da norma NBR 14.913, todas as embalagens apresentam níveis específicos de cada conjunto quanto a estes fatores.</p>
                  
<p>Os diversos tipos de matérias-primas apresentam as seguintes requisições no ensaio de salt spray:</p>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Latão</th>
            <th>Zamak</th>
            <th>Aço Inox</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Cromado 96 h</td>
            <td>Cromado 48 h</td>
            <td>Inox Polido 24 h</td>
        </tr>
        <tr>
            <td>Cromado acetinado 72 h</td>
            <td>Cromado acetinado 24 h</td>
            <td>Inox Escovado 24 h</td>
        </tr>
        <tr>
            <td>Antique 72 h</td>
            <td>Antique 48 h</td>
            <td></td>
        </tr>
        <tr>
            <td>Latão Polido 24 h</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Latão Envernizado 72 h</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
Existe relação entre metal base utilizado e acabamentos superficiais.

                </div>
                  </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingEleven_reg">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_reg" href="#collapseEleven_reg" aria-expanded="false" aria-controls="collapseEleven_reg">
                    Como escolher fechaduras para regiões litorâneas?
                    </a>
                  </h4>
                </div>
                <div id="collapseEleven_reg" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven_reg">
                  <div class="panel-body">
                  Para regiões litorâneas, indicamos os produtos com base em latão nos acabamentos Cromado (CR) e Cromado Acetinado (CA), ou em Aço Inox nos acabamentos Polido (XP) ou Escovado (XE). Indicamos também que seja feita limpeza periódica nos produtos.
                  </div>
                </div>
              </div>
            </div>
           
        </div>
            </div>
            
            </div>
            </div>
        </div>
    
    <!-- Fim do dúvidas -->
@endsection