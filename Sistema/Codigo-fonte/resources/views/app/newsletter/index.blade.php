@extends('layouts.app')

@section('conteudo')


    <section class="container">
        <article class="form-banner forms">

            <section class="form-outer">
                <h1 class="titulo-listagem">Newsletter</h1>

                <table class="table table-white table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-5">Título</th>
                        <th class="text-center col-md-4">Imagem</th>
                        <th class="text-center col-md-1">Excluir</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($newsletter as $news)
                        <tr>
                            <td class="text-center">{{$news->email}}</td>
                            <td class="text-center">{{$news->nome}}</td>
                            <td class="text-center">
                                {{$news->telefone}}
                            </td>
                            <td>
                                <a href="{{route('app::news::deletar', ['id' => $news->id])}}" title="Deletar" class="dialogo-deletar btn btn-primary bt-listagem">
                                    <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="row banner-list col-md-12">
                    <div class="col-md-12">
                        @if ($newsletter->total() > 0)
                            Exibindo {{ $newsletter->count() }} de {{ $newsletter->total() }} contatos encontrados.
                        @else
                            Nenhum contato encontrada.
                        @endif
                    </div>
                </div>

                <div class="paginacao text-center">
                    {!! $newsletter->links() !!}
                </div>

                @include('shared.erro-validacao')
                @include('flash::message')
            </section>
        </article>
    </section>

@endsection
