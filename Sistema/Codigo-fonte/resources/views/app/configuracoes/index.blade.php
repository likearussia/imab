@extends('layouts.app')

@section('conteudo')
    <section class="container">
        <article class="form-banner forms">
            <section class="form-outer">
                <h1 class="titulo-listagem">Configuração de e-mails</h1>


                <table class="table table-white table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-6">E-mail para envio</th>
                        <th class="text-center col-md-3">E-mail</th>
                        <th class="text-center col-md-1">Editar</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($configuracoes as $conf)
                            <tr>
                                <td class="text-center">
                                    {{ $conf->created_at->format("d/m/Y H:i:s")  }}
                                </td>
                                <td class="text-center">
                                    {{ $conf->configuracao }}
                                </td>
                                <td class="text-center">
                                    {{ $conf->email }}
                                </td>
                                <td class="text-center">
                                    <a href="{{route('app::configuracoes::editar', ['id' => $conf->id])}}" class="btn btn-primary bt-listagem btn-edit" title="Editar">
                                        <i class="icone-editar material-icons" title="Editar">edit</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>

                <div class="row banner-list col-md-12">
                    <div class="col-md-12">
                        @if ($configuracoes->total() > 0)
                            Exibindo {{ $configuracoes->count() }} de {{ $configuracoes->total() }} configurações encontradas.
                        @else
                            Nenhuma configuração encontrada.
                        @endif
                    </div>
                </div>

                <div class="paginacao text-center">
                    {!! $configuracoes->links() !!}
                </div>


                @include('shared.erro-validacao')
                @include('flash::message')
            </section>
        </article>
    </section>
@endsection