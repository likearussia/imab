@extends('layouts.app')

@section("conteudo")
    <section class="container">
        <article class="form-banner forms">
            <section class="form-outer">
                <h1>Configuração de e-mails</h1>
                @include('shared.erro-validacao')
                @include('shared.erro-ajax')

                {{ Form::open(['route' => ['app::configuracoes::editar', $configuracao->id], 'class' => 'form-horizontal col-md-12 form-ajax semImagem']) }}

                <div class="form-group">
                    <label for="titulo" class="control-label">{{$configuracao->configuracao}}</label>
                    {{ Form::text('email', $configuracao->email , ['class' => 'form-control']) }}
                    <span class="help-block">Insira aqui o email</span>
                </div>

                <div class="form-group btn-save right">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary right">Salvar</button>
                    </div>
                </div>

                {!! Form::close() !!}
            </section>
        </article>
    </section>
@endsection