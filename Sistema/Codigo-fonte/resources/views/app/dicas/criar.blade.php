@extends('layouts.app')

@section('conteudo')

<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1>Dicas e soluções</h1>

            <form enctype="multipart/form-data" class="form-image">
                <div class="form-group">
                    <div class="col-md-3">
                        <label for="banner" class="control-label">Miniatura</label>
                        <div class="col-md-12 grupo-upload-imagem">
                            <button class="btn btn-primary btn-upLoad">Subir Imagem</button>
                            <input readonly="" class="form-control" placeholder="Procurar..." style="display: none" type="text">
                            <input id="banner" name="caminho" class="image-upload"  multiple="" type="file">
                        </div>
                    </div>

                    <div class="popup-crop col-md-12">


                        <div class='canvas-result'>

                        </div>
                        <div class="imagem_crop col-md-12" style="display: none">
                            <img src="" class="image-uploaded col-md-11"/>
                            <input class="uploaded-image" type="hidden" name="caminho-uploaded"/>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary cropp"  data-method="crop" title="Crop">
                                    <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.crop()">
                                        <span class="fa fa-check"></span>
                                        cortar imagem
                                    </span>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

            @include('shared.erro-validacao')
            @include('shared.erro-ajax')

            {{ Form::open(['route' => ['app::dicas::criar'], 'files' => true, 'class' => 'form-horizontal col-md-12 form-ajax']) }}

            <div class="form-group col-md-12 ">
                <label for="titulo" class="control-label required">Título</label>
                {{ Form::text('titulo', null, ['class' => 'form-control']) }}
                <span class="help-block">Insira aqui um título para o conteúdo.</span>
            </div>

            <div class="form-group col-md-12 ">
                <label for="titulo" class="control-label required">Resumo</label>
                {{ Form::textarea('resumo', null, ['class' => 'form-control resumo', 'maxlength' => '280']) }}
                <span class="help-block">Resumo para aparecer junto com a Thumbnail.</span>
            </div>
            
            
            <div class="form-group col-md-5">
                <label for="banner" class="control-label required">Imagem</label>
                <div class="grupo-upload-imagem">
                    <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                    <input id="banner" name="caminho" multiple="" type="file">
                    <span class="help-block">Selecione uma imagem para o conteúdo.</span>
                </div>
            </div>

            <div class="image-def">
                <input type="hidden" name="simage" class="simage"/>
                <input type="hidden" name="ximage" class="ximage"/>
                <input type="hidden" name="yimage" class="yimage"/>
                <input type="hidden" name="wimage" class="wimage"/>
                <input type="hidden" name="himage" class="himage"/>
            </div>


            <div class="form-group form-select right col-md-5">
                <label for="select111" class="col-md-12 control-label required">Categoria</label>

                <div class="">

                    {{ Form::select('categoria', $categorias, null,
                                                ['placeholder' => 'Selecione', 'id' => 'categoria',
                                                'class' => 'form-control', 'required']) }}
                    <span class="help-block">Selecione uma categoria para o conteúdo.</span>
                </div>
            </div>

            <div class="form-group col-md-12 col-lg-12">
                <label for="texto" class="control-label left required" style="text-align: left">Texto</label>
                {{ Form::textarea('conteudo', null, ['id' => 'texto',
                                        'class' => 'summernote', 'style'=>  'margin-top: 40px']) }}
            </div>

            <div class="col-md-12">
                <div class="form-group btn-save right col-md-12">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary right">Salvar</button>
                    </div>
                </div>
            </div>
            
            {!! Form::close() !!}
            

        </section>
    </article>
</section>

@endsection

@push('css')
<link href="dist/summernote/summernote.css" rel="stylesheet" type="text/css">
<link href="dist/selectize/css/selectize.css" rel="stylesheet" type="text/css">
@endpush


@push('scripts')
<script src="dist/summernote/summernote.min.js"></script>
<script src="dist/summernote/lang/summernote-pt-BR.min.js"></script>
<script src="dist/selectize/js/standalone/selectize.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $('.resumo').next().append("<p class='qtdChar'></p>");
    $('.resumo').keydown(function(){
        number = 280 - $(this).val().length;

        $('.qtdChar').html(" ").text(number);

        if(number < 50){
            $('.qtdChar').css('color', 'red');
        }else{

            $('.qtdChar').css('color', 'black');

        }
    });

    $('.resumo').keyup(function(){
        number = 280 - $(this).val().length;

        $('.qtdChar').html(" ").text(number);

        if(number < 50){
            $('.qtdChar').css('color', 'red');
        }else{

            $('.qtdChar').css('color', 'black');

        }
    });


    $('.cropp').click(function () {
        $('.canvas-result').html(" ");

        $('.canvas-result').append($('.image-uploaded').cropper("getCroppedCanvas"));

        $('.cropper-container').hide();
    });

    $('.image-upload').change(function () {

        //var data = new FormData($(this).val());

        $('.form-image').ajaxForm({
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route('app::imagem')}}',
            success: function (resp) {
                console.log(resp);
                setTimeout(function () {
                    $('.txt-image').text(" ");
                    $('.image-uploaded').attr('src', '');

                    $('.image-uploaded').attr('src', 'tmp_uploads/' + resp);
                    $('.simage').val('tmp_uploads/' + resp);
                    $('.uploaded-image').val('tmp_uploads/' + resp);

                    $('.imagem_crop').fadeIn();

                    $('.image-uploaded').cropper({
                        zoomable: false,
                        aspectRatio: 10 / 10,
                        crop: function (e) {
                            $('.ximage').val(e.x);
                            $('.yimage').val(e.y);
                            $('.wimage').val(e.width);
                            $('.himage').val(e.height);
                        }
                    });
                }, 300)
            },
            error: function (resp) {
                console.log(resp);
            },
            beforeSend: function () {
                $('.popup-crop').append('<p class="txt-image">Carregando...</p>');
            }
        }).submit();
    });



opcoes_summernote = {
        minHeight: 100,
        focus: false,
        placeholder: 'Escreva aqui...',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['link', 'video']],
            ['misc', ['fullscreen']]
        ],
        fontNames: [''],
        lang: 'pt-BR',
        callbacks: {
            // workaround para habilitar o foco que é normalmente feito pelo material.js
            onFocus: function () {
                $(this).parent().addClass('is-focused');
            },
            onBlur: function () {
                $(this).parent().removeClass('is-focused');
            }
        }
    };

    var $elementoSummernote = $('.summernote');
    $elementoSummernote.summernote(opcoes_summernote);


});
</script>
@endpush

