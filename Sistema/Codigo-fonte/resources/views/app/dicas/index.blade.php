@extends('layouts.app')

@section('conteudo')

<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1 class="titulo-listagem">Dicas e soluções</h1>
            <div class="btn-criar">
                <a href="{{route('app::dicas::criar')}}" class="btn btn-primary btn-menu right btn-add-new">
                    <i class="icone-menu pad-white material-icons">add_circle</i> 
                    Novo
                </a>
            </div>
            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-4">Titulo</th>
                        <th class="text-center col-md-3">Imagem</th>
                        <th class="text-center col-md-1">Status</th>
                        <th class="text-center col-md-1">Editar</th>
                        <th class="text-center col-md-1">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dicas as $dica)
                    <tr>
                        <td class="text-center">
                            {{$dica->created_at->format('d/m/y - H:i:s')}}
                        </td>
                        <td class="text-center">
                            {{$dica->titulo}}
                        </td>
                        
                        
                        <td>
                            <img class="col-md-12" src="{{ url('/imagens/upload/posts') .'/'. $dica->imagem_destaque }}" />
                        </td>
                        
                        
                        <td class="text-center">
                            @if($dica->status == 1)
                            <a href="{{route('app::dicas::status', ['id' => $dica->id, 'status' => 0])}}" class="_status _ativo"></a>
                            @else
                            <a href="{{route('app::dicas::status', ['id' => $dica->id, 'status' => 1])}}" class="_status _inativo"></a>
                            @endif
                        </td>

                        <td class="text-center">
                            <a href="{{route('app::dicas::editar', ['id' => $dica->id])}}" class="btn btn-primary bt-listagem" title="Editar">
                                <i class="icone-editar material-icons" title="Editar">edit</i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('app::dicas::deletar', ['id' => $dica->id])}}" class="dialogo-deletar btn btn-primary bt-listagem" title="Editar">
                                <i class="icone-editar material-icons" title="Deletar">delete</i>
                            </a>
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row banner-list col-md-12">
                <div class="col-md-12">
                    @if ($dicas->total() > 0)
                    Exibindo {{ $dicas->count() }} de {{ $dicas->total() }} dicas encontradas.
                    @else
                    Nenhuma dica encontrada.
                    @endif
                </div>
            </div>
            
            <div class="paginacao text-center">
                {!! $dicas->links() !!}
            </div>
            
            @include('shared.erro-validacao')
            @include('flash::message')

        </section>
    </article>
</section>

@endsection