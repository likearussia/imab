@extends('layouts.app')


@section('conteudo')
<section class="container">
    <article class="form-banner forms">
        <section class="form-outer">
            <h1 class="titulo-listagem">Categorias</h1>

            <div class="btn-criar">
                <a href="{{route('app::dicas::categoria::criar')}}" class="btn btn-primary btn-menu right btn-add-new">

                    <i class="icone-menu pad-white material-icons">add_circle</i> 
                    Novo

                </a>
            </div>

            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-8">Nome</th>
                        <th class="text-center col-md-1">Status</th>
                        <th class="text-center col-md-1">Editar</th>
                    </tr>
                </thead>
                @foreach($lista as $categoria)
                <tr>
                    <td class="text-center">
                        {{$categoria->created_at->format('d/m/y - H:i:s')}}
                    </td>
                    <td class="text-center">
                        {{$categoria->nome}}
                    </td>
                    <td class="text-center">
                        @if($categoria->status == 1)
                        <a href="{{route('app::dicas::categoria::status', ['id' => $categoria->id, 'status' => 0])}}" class="_status _ativo btn-edit-status0"></a>
                        @else
                        <a href="{{route('app::dicas::categoria::status', ['id' => $categoria->id, 'status' => 1])}}" class="_status _inativo btn-edit-status1"></a>
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{route('app::dicas::categoria::editar', ['id' => $categoria->id])}}" class="btn btn-primary bt-listagem right btn-edit" title="Editar">
                            <i class="icone-editar material-icons" title="Editar">edit</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
            
            <div class="row banner-list col-md-12">
                <div class="col-md-12">
                    @if ($lista->total() > 0)
                    Exibindo {{ $lista->count() }} de {{ $lista->total() }} categorias encontradas.
                    @else
                    Nenhuma categoria encontrada.
                    @endif
                </div>
            </div>
            
            <div class="paginacao text-center">
                {!! $lista->links() !!}
            </div>
            
            
            @include('shared.erro-validacao')
            @include('flash::message')
        </section>
    </article>
</section>
@endsection