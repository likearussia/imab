@extends('layouts.app')


@section('conteudo')
<section class="container">
    <article class="form-banner forms">
        <section class="form-outer">
            <h1>Categorias</h1>
            @include('shared.erro-validacao')
            @include('shared.erro-ajax')

            {{ Form::open(['route' => ['app::dicas::categoria::editar', $lista->id], 'class' => 'form-horizontal col-md-12 form-ajax semImagem']) }}

            <div class="form-group">
                <label for="titulo" class="control-label">Nome da Categoria</label>
                {{ Form::text('nome', $lista->nome, ['class' => 'form-control']) }}
                <span class="help-block">Insira aqui o nome da categoria.</span>
            </div>

            <div class="form-group btn-save right">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary right">Salvar</button>
                </div>
            </div>

            {!! Form::close() !!}
        </section>
    </article>
</section>
@endsection