@extends('layouts.app')
@section('conteudo')
<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1 class="titulo-listagem">Banner</h1>
            <div class="btn-criar">
                <a href="{{route('app::banner::criar')}}" class="btn btn-primary btn-menu right btn-add-new">
                    <i class="icone-menu pad-white material-icons">add_circle</i> 
                    Novo
                </a>
            </div>
            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-4">Título</th>
                        <th class="text-center col-md-3"></th>
                        <th class="text-center col-md-1">Status</th>
                        <th class="text-center col-md-1">Editar</th>
                        <th class="text-center col-md-1">Deletar</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($banners as $banner)
                    <tr>
                        <td class="text-center">{{ $banner->created_at->format('d/m/y - H:i:s') }}</td>

                        <td class="text-center">
                            {{ $banner->titulo }}
                        </td>

                        <td class="text-center td-image">
                            <img src="{{url('/') ."/imagens/upload/banners/". $banner->caminho}}"/>
                        </td>

                        <td class="text-center">
                            @if($banner->status == 1)
                            <a href="{{route('app::banner::status', ['id' => $banner->id, 'status' => 0])}}" class="_status _ativo"></a>
                            @else
                            <a href="{{route('app::banner::status', ['id' => $banner->id, 'status' => 1])}}" class="_status _inativo"></a>
                            @endif

                        </td>

                        <td class="text-right">
                            <a href="{{route('app::banner::editar', ['id' => $banner->id])}}" class="btn btn-primary bt-listagem" title="Editar">
                                <i class="icone-editar material-icons" title="Editar">edit</i>
                            </a>
                        </td>

                        <td class="text-left">
                            <a href="{{route('app::banner::deletar', ['id' => $banner->id])}}" title="Deletar" class="btn btn-primary bt-listagem dialogo-deletar">
                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                            </a>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="row banner-list col-md-12">
                <div class="col-md-12">
                    @if ($banners->total() > 0)
                    Exibindo {{ $banners->count() }} de {{ $banners->total() }} banners encontrados.
                    @else
                    Nenhum banner encontrado.
                    @endif
                </div>
            </div>

            <div class="paginacao text-center">
                {!! $banners->links() !!}
            </div>

            @include('shared.erro-validacao')
            @include('flash::message')
        </section>
    </article>
</section>
@endsection
