@extends('layouts.app')

@section('conteudo')
<meta name="csrf-token" content="{{ csrf_token() }}">

<section class="container">
    <article class="form-banner forms">


        <section class="form-outer">
            <h1>Banner</h1>
            @include('shared.erro-validacao')
            @include('shared.erro-ajax')

            <form enctype="multipart/form-data" class="form-image">
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="banner" class="control-label required">Imagem</label>
                        <div class="col-md-12 grupo-upload-imagem">
                            <button class="btn btn-primary btn-upLoad">Subir Imagem</button>
                            <input readonly="" class="form-control" placeholder="Procurar..." style="display: none" type="text">
                            <input id="banner" name="caminho" class="image-upload"  multiple="" type="file">
                        </div>
                    </div>

                    <div class="popup-crop col-md-12">

                        <div class='canvas-result'>

                        </div>
                        <div class="imagem_crop col-md-12" style="display: none">
                            <img src="" class="image-uploaded col-md-11"/>
                            <input class="uploaded-image" type="hidden" name="caminho-uploaded"/>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary cropp"  data-method="crop" title="Crop">
                                    <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.crop()">
                                        <span class="fa fa-check"></span>
                                        cortar imagem
                                    </span>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

            {{ Form::open(['route' => ['app::banner::salvar', $banner->id], 'files' => true, 'class' => 'form-horizontal form-banner col-md-12 form-ajax']) }}

            <div class="form-group col-md-6">
                <label for="titulo" class="control-label required">Titulo</label>
                {{ Form::text('titulo', $banner->titulo, ['class' => 'form-control']) }}
                <span class="help-block">Insira aqui um titulo para o banner.</span>
            </div>

            {{ Form::hidden('status', $banner->status, ['class' => 'form-control']) }}


            <div class="form-group right col-md-6">
                <div class="image-def">
                    <input type="hidden" name="simage" class="simage"/>
                    <input type="hidden" name="ximage" class="ximage"/>
                    <input type="hidden" name="yimage" class="yimage"/>
                    <input type="hidden" name="wimage" class="wimage"/>
                    <input type="hidden" name="himage" class="himage"/>
                </div>
                <div class="col-md-10 right">
                    <div class="col-md-12">
                        <img class="miniatura col-md-12" src="{{ $diretorio_imagens . '/' . $banner->caminho }}"/>
                    </div>
                </div>
            </div>


            <div class="form-group col-md-12">
                <label for="link_url" class="control-label">Link</label>
                {{ Form::text('link', $banner->link, ['class' => 'form-control']) }}
            </div>

            <div class="col-md-12">
                <div class="form-group left">
                    <div class="togglebutton">
                        <label>


                            @if($banner->nova_aba == 1)
                            <input checked="checked" name="nova_aba" type="checkbox"> Link abre em nova aba ?
                            @else
                            <input name="nova_aba" type="checkbox"> Link abre em nova aba ?
                            @endif
                        </label>
                    </div>
                </div>

                <div class="col-md-3 right">
                    <div class="form-group btn-save right">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                        </div>
                    </div>

                </div>

            </div>


            <div class="col-md-12 form-group">
                <p class="txt-alerta">
                    Os campos com um <span class="required"></span> são de preenchimento obrigatório.
                </p>
            </div>
            {!! Form::close() !!}

        </section>
    </article>
</section>


@push('scripts')

<script src="http://malsup.github.com/jquery.form.js"></script> 
<link  href="/assets/cropper/cropper.css" rel="stylesheet">
<script src="/assets/cropper/cropper.js"></script>


<script type="text/javascript">
$(document).ready(function () {


    $('.cropp').click(function () {
        $('.canvas-result').html(" ");

        $('.canvas-result').append($('.image-uploaded').cropper("getCroppedCanvas"));

        $('.cropper-container').hide();
    });


    $('.image-upload').change(function () {

        //var data = new FormData($(this).val());

        $('.form-image').ajaxForm({
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route('app::imagem')}}',
            success: function (resp) {
                console.log(resp);
                setTimeout(function () {
                    $('.txt-image').text(" ");
                    $('.image-uploaded').attr('src', '');

                    $('.image-uploaded').attr('src', 'tmp_uploads/' + resp);
                    $('.simage').val('tmp_uploads/' + resp);
                    $('.uploaded-image').val('tmp_uploads/' + resp);

                    $('.imagem_crop').fadeIn();

                    $('.image-uploaded').cropper({
                        zoomable: false,
                        aspectRatio: 17.3 / 5,
                        crop: function (e) {
                            $('.ximage').val(e.x);
                            $('.yimage').val(e.y);
                            $('.wimage').val(e.width);
                            $('.himage').val(e.height);
                        }
                    });
                }, 300)
            },
            error: function (resp) {
                console.log(resp);
            },
            beforeSend: function () {
                $('.popup-crop').append('<p class="txt-image">Carregando...</p>');
            }
        }).submit();
    });

});
</script>
@endpush

@endsection