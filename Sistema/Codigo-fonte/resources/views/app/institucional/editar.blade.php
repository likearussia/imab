@extends('layouts.app')

@section('conteudo')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="container">
    <article class="form-banner forms">

        <div class="box-timeline-clone" style="display: none">

            <div class="box-timeline">
                <p class="remove-time">X</p>
                <div class="form-group form-timeline col-md-6 col-lg-6">
                    <label for="titulo" class="control-label">Titulo</label>
                    {{ Form::text('linha[titulo][]', "-", ['class' => 'form-control']) }}
                    <span class="help-block">Insira aqui um titulo para o texto.</span>

                </div>

                <div class="form-group col-md-5 col-lg-5 form-right form-timeline">
                    <label for="titulo" class="control-label">Ano</label>
                    {{ Form::text('linha[ano][]', "-", ['class' => 'form-control']) }}
                    <span class="help-block">Insira aqui um titulo para o texto.</span>
                </div>

                <div class="form-group col-md-12 col-lg-12 form-timeline">
                    <label for="texto" class="control-label left" style="text-align: left">Texto</label>
                    {{ Form::textarea('linha[texto][]', "-", ['id' => 'texto', 'class' => 'col-md-12 textarea-historia form-control']) }}
                </div>

                <div class="col-md-3">
                    <label for="banner" class="control-label">Imagem</label>
                    <div class="col-md-12 grupo-upload-imagem">
                        <input readonly="" class="form-control" placeholder="Procurar..." style="display: none" type="text">
                        <input id="banner" name="timeImage[]" class="image-upload"  multiple="" type="file">
                        <input type="hidden" name="timeUpdate[]" value="-"/>
                    </div>
                </div>

            </div>
        </div>

        <section class="form-outer">
            <h1>Institucional</h1>
            @include('shared.erro-validacao')
            @include('shared.erro-ajax')

            @if($institucional->id != 4)
            <form enctype="multipart/form-data" class="form-image">
                <div class="form-group">
                    <div class="col-md-3">
                        <label for="banner" class="control-label">Imagem</label>
                        <div class="col-md-12 grupo-upload-imagem">
                            <button class="btn btn-primary btn-upLoad">Subir Imagem</button>
                            <input readonly="" class="form-control" placeholder="Procurar..." style="display: none" type="text">
                            <input id="banner" name="caminho" class="image-upload"  multiple="" type="file">
                        </div>
                    </div>

                    <div class="popup-crop col-md-12">


                        <div class='canvas-result'>

                        </div>
                        <div class="imagem_crop col-md-12" style="display: none">
                            <img src="" class="image-uploaded col-md-11"/>
                            <input class="uploaded-image" type="hidden" name="caminho-uploaded"/>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary cropp"  data-method="crop" title="Crop">
                                    <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.crop()">
                                        <span class="fa fa-check"></span>
                                        cortar imagem
                                    </span>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            @endif

            @if($institucional->id != 4)
            {{ Form::open(['route' => ['app::institucional::editar', $institucional->id], 'files' => true, 'class' => 'form-horizontal col-md-12']) }}
            @else
                {{ Form::open(['route' => ['app::institucional::editar', $institucional->id], 'files' => true, 'class' => 'form-horizontal col-md-12']) }}
            @endif
            <div class="form-group">
                <label for="titulo" class="control-label">Título</label>
                {{ Form::text('titulo', $institucional->titulo, ['class' => 'form-control']) }}
                <span class="help-block">Insira aqui um titulo para o conteúdo.</span>
            </div>

            <div class="form-group">
                <label for="titulo" class="control-label">Subtítulo</label>
                {{ Form::text('subtitulo', $institucional->subtitulo, ['class' => 'form-control']) }}
                <span class="help-block">Insira aqui um titulo para o conteúdo.</span>
            </div>

            <div class="image-def">
                <input type="hidden" name="simage" class="simage"/>
                <input type="hidden" name="ximage" class="ximage"/>
                <input type="hidden" name="yimage" class="yimage"/>
                <input type="hidden" name="wimage" class="wimage"/>
                <input type="hidden" name="himage" class="himage"/>
            </div>


            <div class="form-group col-md-12 col-lg-12">
                <label for="texto" class="control-label left" style="text-align: left">Texto</label>
                {{ Form::textarea('texto', $institucional->texto, ['id' => 'texto',
                                        'class' => 'summernote']) }}
            </div>

            @if($institucional->id == 4)
            <h1>Linha do tempo</h1>
            <div class="timeline">

                @foreach ($timeline as $tm)

                <div class="box-timeline">

                    <p class="remove-time">X</p>
                    <div class="form-group form-timeline col-md-6 col-lg-6">
                        <label for="titulo" class="control-label">Titulo</label>
                        {{ Form::text('linha[titulo][]', $tm->titulo, ['class' => 'form-control']) }}
                        <span class="help-block">Insira aqui um titulo para o texto.</span>
                    </div>

                    <div class="form-group col-md-5 col-lg-5 form-right form-timeline">
                        <label for="titulo" class="control-label">Ano</label>
                        {{ Form::text('linha[ano][]', $tm->ano, ['class' => 'form-control']) }}
                        <span class="help-block">Insira aqui um titulo para o texto.</span>
                    </div>

                    <div class="form-group col-md-12 col-lg-12 form-timeline">
                        <label for="texto" class="control-label left" style="text-align: left">Texto</label>
                        {{ Form::textarea('linha[texto][]', $tm->texto, ['id' => 'texto', 'class' => 'col-md-12 textarea-historia form-control']) }}
                    </div>

                    <div class="col-md-3">
                        <label for="banner" class="control-label">Imagem</label>
                        <div class="col-md-12 grupo-upload-imagem">
                            <input readonly="" class="form-control" placeholder="Procurar..." style="display: none" type="text">
                            <input id="banner" name="timeImage[]" class="image-upload" value="{{$tm->caminho}}" type="file">
                            
                            <input type="hidden" name="timeUpdate[]" value="{{$tm->caminho}}"/>
                        </div>
                    </div>

                </div>
                @endforeach
            </div>

            <div class="form-group btn-save col-md-12">
                <div class="col-md-12">
                    <a href="" class="addAno"><i class="material-icons">add_circle_outline</i>Adicionar</a>
                </div>
            </div>

            @endif


            <div class="form-group btn-save col-md-12">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary right">Salvar</button>
                </div>
            </div>
            {!! Form::close() !!}
        </section>
    </article>
</section>
@endsection


@push('css')
<link href="dist/summernote/summernote.css" rel="stylesheet" type="text/css">
<link href="dist/selectize/css/selectize.css" rel="stylesheet" type="text/css">
@endpush


@push('scripts')
<script src="dist/summernote/summernote.min.js"></script>
<script src="dist/summernote/lang/summernote-pt-BR.min.js"></script>
<script src="dist/selectize/js/standalone/selectize.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 

<link  href="/assets/cropper/cropper.css" rel="stylesheet">
<script src="/assets/cropper/cropper.js"></script>


<script type="text/javascript">
$(document).ready(function () {

    $('.addAno').click(function () {
        var clone = $('.box-timeline-clone .box-timeline').clone();
        $('.timeline').append(clone);
        remove();
        return false;

    });

    $('.cropp').click(function () {
        $('.canvas-result').html(" ");

        $('.canvas-result').append($('.image-uploaded').cropper("getCroppedCanvas"));

        $('.cropper-container').hide();
    });


    $('.image-upload').change(function () {

        //var data = new FormData($(this).val());

        $('.form-image').ajaxForm({
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route('app::imagem')}}',
            success: function (resp) {
                console.log(resp);
                setTimeout(function () {
                    $('.txt-image').text(" ");
                    $('.image-uploaded').attr('src', '');

                    $('.image-uploaded').attr('src', 'tmp_uploads/' + resp);
                    $('.simage').val('tmp_uploads/' + resp);
                    $('.uploaded-image').val('tmp_uploads/' + resp);

                    $('.imagem_crop').fadeIn();

                    $('.image-uploaded').cropper({
                        zoomable: false,
                        aspectRatio: 15 / 7,
                        crop: function (e) {
                            $('.ximage').val(e.x);
                            $('.yimage').val(e.y);
                            $('.wimage').val(e.width);
                            $('.himage').val(e.height);
                        }
                    });
                }, 300)
            },
            error: function (resp) {
                console.log(resp);
            },
            beforeSend: function () {
                $('.popup-crop').append('<p class="txt-image">Carregando...</p>');
            }
        }).submit();
    });

    opcoes_summernote = {
        minHeight: 100,
        focus: false,
        placeholder: 'Escreva aqui...',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['video']],
            ['misc', ['fullscreen']]
        ],
        fontNames: [''],
        lang: 'pt-BR',
        callbacks: {
            // workaround para habilitar o foco que é normalmente feito pelo material.js
            onFocus: function () {
                $(this).parent().addClass('is-focused');
            },
            onBlur: function () {
                $(this).parent().removeClass('is-focused');
            }
        }
    };

    var $elementoSummernote = $('.summernote');
    $elementoSummernote.summernote(opcoes_summernote);

    remove();

});

    function remove(){
        $(".remove-time").on('click', function (e) {
            //e.preventDefault();
            var objeto = $(this);
            swal({
                title: 'Você tem certeza que deseja deletar o item?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0fb2fc',
                cancelButtonColor: '#d41414',
                confirmButtonText: 'Sim',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                objeto.parent().html(" ").html("<p>O item será excluido ao salvar.</p>").addClass("deleted");
            });
        });

    }
</script>
@endpush
