@extends('layouts.app')

@section('conteudo')


<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1 class="titulo-listagem">Institucionais</h1>

            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-5">Título</th>
                        <th class="text-center col-md-4">Imagem</th>
                        <th class="text-center col-md-1">Editar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($institucionais as $institucional)
                    <tr>
                        <td class="text-center">{{$institucional->created_at}}</td>
                        <td class="text-center">{{$institucional->titulo}}</td>
                        <td class="text-center">
                            @if($institucional->id == 4)
                                -
                            @else
                                <img class="col-md-12" src="{{url('imagens/upload/institucionais') .'/'.$institucional->imagem_destaque}}"/>
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="{{route('app::institucional::editar', ['id' => $institucional->id])}}" class="btn btn-primary bt-listagem btn-edit" title="Editar">
                                <i class="icone-editar material-icons" title="Editar">edit</i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            <div class="row banner-list col-md-12">
                <div class="col-md-12">
                    @if ($institucionais->total() > 0)
                    Exibindo {{ $institucionais->count() }} de {{ $institucionais->total() }} institucionais encontradas.
                    @else
                    Nenhuma institucional encontrada.
                    @endif
                </div>
            </div>
            
            @include('shared.erro-validacao')
            @include('flash::message')
        </section>
    </article>
</section>

@endsection
