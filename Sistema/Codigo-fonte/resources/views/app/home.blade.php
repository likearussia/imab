@extends('layouts.app')

@section('conteudo')

<div class="box-icons container-fluid">
    <div class="icons-inner container">

        <div class="box-icons-atalho col-md-12 col-lg-8">
            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">image</i>
                    </div>
                    <div class="desc-box">
                        Banners
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::banner::criar')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu menu-home pad-white material-icons">add_circle</i> 
                        <span class="txt-buttons-show">Novo</span>
                    </a>
                    <a href="{{route('app::banner::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu  menu-home pad-white material-icons">list</i> 
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>

            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">location_city</i>
                    </div>
                    <div class="desc-box">
                        Institucionais
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::institucional::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i>
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>

            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">lightbulb_outline</i>
                    </div>
                    <div class="desc-box">
                        Dicas e soluções
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::dicas::criar')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">add_circle</i> 
                        <span class="txt-buttons-show">Novo</span>
                    </a>
                    <a href="{{route('app::dicas::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i> 
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>

            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">file_download</i>
                    </div>
                    <div class="desc-box">
                        Downloads
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::download::criar')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">add_circle</i>
                        <span class="txt-buttons-show">Novo</span>
                    </a>
                    <a href="{{route('app::download::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i>
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>

            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">people</i>
                    </div>
                    <div class="desc-box">
                        Usuários site
                    </div>
                </div>
                <div class="box-buttons">

                    <a href="{{route('app::usuarios-site::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i>
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>


            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">content_paste</i>
                    </div>
                    <div class="desc-box">
                        Currículos
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::curriculo::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i>
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>


            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">account_circle</i>
                    </div>
                    <div class="desc-box">
                        Administradores
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::usuario::criar')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">add_circle</i> 
                        <span class="txt-buttons-show">Novo</span>
                    </a>
                    <a href="{{route('app::usuario::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i> 
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>



            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">settings</i>
                    </div>
                    <div class="desc-box">
                        Configurações
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::configuracoes::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i>
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>

            <div class="box-icon col-md-6 col-sm-6 col-lg-4">
                <div class="first-box">
                    <div class="icon-box">
                        <i class="icone-menu icon-inicio material-icons">description</i>
                    </div>
                    <div class="desc-box">
                        Newsletter
                    </div>
                </div>
                <div class="box-buttons">
                    <a href="{{route('app::news::index')}}" class="btn btn-primary btn-menu right">
                        <i class="icone-menu pad-white menu-home material-icons">list</i>
                        <span class="txt-buttons-show">Listar</span>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection