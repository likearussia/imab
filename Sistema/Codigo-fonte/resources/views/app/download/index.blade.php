@extends('layouts.app')

@section('conteudo')

<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1 class="titulo-listagem">Downloads</h1>
            <div class="btn-criar">
                <a href="{{route('app::download::criar')}}" class="btn btn-primary btn-menu right btn-add-new">
                    <i class="icone-menu pad-white material-icons">add_circle</i> 
                    Novo
                </a>
            </div>

            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-7">Título</th>
                        <th class="text-center col-md-1">Arquivo</th>
                        <th class="text-center col-md-1">Editar</th>
                        <th class="text-center col-md-1">Deletar</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($downloads as $download)
                    <tr>
                        <td class="text-center">{{ $download->created_at->format('d/m/Y H:i:s') }}</td>

                        <td class="text-center"> {{ $download->titulo }} </td>

                        <td class="text-center"> 
                            <a href="{{ route('app::download::baixar', ['arquivo' => $download->caminho]) }}" class="btn btn-primary bt-listagem" title="Editar">
                                <i class="icone-editar material-icons" title="Baixar">file_download</i>
                            </a>
                        </td>

                        <td class="text-center"> 
                            <a href="{{route('app::download::editar', ['id' => $download->id])}}" class="btn btn-primary bt-listagem" title="Editar">
                                <i class="icone-editar material-icons" title="Editar">edit</i>
                            </a>
                        </td>

                        <td class="text-center">
                            <a href="{{route('app::download::deletar', ['id' => $download->id])}}" class="btn btn-primary bt-listagem dialogo-deletar" title="Editar">
                                <i class="icone-editar material-icons" title="Editar">delete</i>
                            </a>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="row banner-list col-md-12">
                <div class="col-md-12">
                    @if ($downloads->total() > 0)
                    Exibindo {{ $downloads->count() }} de {{ $downloads->total() }} arquivos encontrados.
                    @else
                    Nenhum arquivo encontrado.
                    @endif
                </div>
            </div>


            <div class="paginacao text-center">
                {!! $downloads->links() !!}
            </div>



            @include('shared.erro-validacao')
            @include('flash::message')
        </section>
    </article>
</section>


@endsection
