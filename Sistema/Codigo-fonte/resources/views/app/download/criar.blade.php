@extends('layouts.app')

@section('conteudo')

<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1>Downloads</h1>

            @include('shared.erro-validacao')
            @include('shared.erro-ajax')
            {{ Form::open(['route' => ['app::download::criar'], 'files' => true, 'class' => 'form-horizontal col-md-12 form-ajax']) }}
            <div class="form-group col-md-12">
                <label for="titulo" class="control-label required">Título</label>
                {{ Form::text('titulo', null, ['class' => 'form-control']) }}
                <span class="help-block">Insira aqui um titulo para o conteúdo.</span>
            </div>

            <div class="form-group col-md-12">
                <label for="descricao" class="control-label required">Descrição</label>
                {{ Form::text('descricao', null, ['class' => 'form-control']) }}
                <span class="help-block">Insira aqui um titulo para o conteúdo.</span>
            </div>

            <div class="form-group col-md-3">
                <label for="banner" class="control-label required">Arquivo</label>
                <div class="grupo-upload-imagem">
                    <input readonly="" class="form-control hide" placeholder="Procurar..." type="text">
                    <input id="banner" name="caminho" multiple="" type="file">
                    <button class="btn btn-primary btn-upLoad">Subir Arquivo</button>
                </div>
            </div>
            <div class="form-group col-md-8 right">

                <label for="" class="control-label required col-md-12" style="text-align: left">Quais usuários podem realizar este download?</label>
                <div class="checkbox col-md-4">
                    <label>
                        <input type="checkbox" class="cm" name="type[]" value="1"> Todos
                    </label>
                </div>

                <div class="checkbox col-md-4">
                    <label>
                        <input type="checkbox" class="adc" name="type[]" value="2"> Imprensa
                    </label>
                </div>

                <div class="checkbox col-md-4">
                    <label>
                        <input type="checkbox" class="adc1" name="type[]" value="3"> Representante
                    </label>
                </div>
            </div>


            <div class="btn-save right col-md-12">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary right">Salvar</button>
                </div>
            </div>


            <div class="col-md-12" style="float: left;padding-left: 0;">
                <div class="barr-progress col-md-12" style="background-color: #c1c1c1;height: 10px; padding: 0; visibility: hidden">
                </div>
            </div>

            {!! Form::close() !!}
        </section>
    </article>
</section>

@endsection

@push('css')
<link href="dist/summernote/summernote.css" rel="stylesheet" type="text/css">
<link href="dist/selectize/css/selectize.css" rel="stylesheet" type="text/css">
@endpush


@push('scripts')
<script src="dist/summernote/summernote.min.js"></script>
<script src="dist/summernote/lang/summernote-pt-BR.min.js"></script>
<script src="dist/selectize/js/standalone/selectize.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    opcoes_summernote = {
        minHeight: 100,
        focus: false,
        placeholder: 'Escreva aqui...',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['link', 'video']],
            ['misc', ['fullscreen']]
        ],
        fontNames: [''],
        lang: 'pt-BR',
        callbacks: {
            // workaround para habilitar o foco que é normalmente feito pelo material.js
            onFocus: function () {
                $(this).parent().addClass('is-focused');
            },
            onBlur: function () {
                $(this).parent().removeClass('is-focused');
            }
        }
    };

    var $elementoSummernote = $('.summernote');
    $elementoSummernote.summernote(opcoes_summernote);


});
</script>
@endpush

