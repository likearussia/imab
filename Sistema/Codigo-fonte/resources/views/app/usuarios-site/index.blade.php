@extends('layouts.app')

@section('conteudo')
<section class="container">
    <article class="form-banner forms">
        <section class="form-outer">
            <h1 class="titulo-listagem">Usuários site</h1>
            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-2">Nome</th>
                        <th class="text-center col-md-2">E-mail</th>
                        <th class="text-center col-md-2">Tipo</th>
                        <th class="text-center col-md-2">Status</th>
                        <th class="text-center col-md-1">Excluir</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usuarios as $usuario)
                    <tr>
                        <td class="text-center">{{ $usuario->created_at->format('d/m/Y H:i:s') }}</td>
                        <td class="text-center">{{ $usuario->nome }}</td>
                        <td class="text-center">{{ $usuario->email }}</td>
                        <td class="text-center">
                            @if($usuario->tipo == 1)
                                Consumidor
                            @elseif($usuario->tipo == 2)
                                Imprensa
                            @else
                                Revenda
                            @endif

                        </td>
                        <td>
                            @if($usuario->status == 1)
                            <a href="{{ route('app::usuarios-site::status', ['id' => $usuario->id, 'novoStatus' => '0']) }}" class="_status _ativo"></a>
                            @else
                            <a href="{{ route('app::usuarios-site::status', ['id' => $usuario->id, 'novoStatus' => '1']) }}" class="_status _inativo"></a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('app::usuarios-site::deletar', ['id' => $usuario->id]) }}" class="btn btn-primary bt-listagem dialogo-deletar" title="Deletar">
                                <i class="icone-editar material-icons" title="Editar">delete</i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            
            @include('flash::message')
        </section>
        
        
        
        <div class="paginacao text-center">
                {!! $usuarios->links() !!}
            </div>
    </article>
</section>
@endsection