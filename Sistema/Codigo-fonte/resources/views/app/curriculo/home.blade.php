@extends('layouts.app')

@section('conteudo')

<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1 class="titulo-listagem">Currículos</h1>

            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-1">Área</th>
                        <th class="text-center col-md-4">Nome</th>
                        <th class="text-center col-md-4">E-mail</th>
                        <th class="text-center col-md-2">Telefone</th>
                        <th class="text-center col-md-1">Ver +</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($curriculo as $cr)
                    <tr>
                        <td class="text-center">{{ json_decode($cr->dadospessoais)->area }}</td>
                        <td class="text-center">
                            {{ json_decode($cr->dadospessoais)->nome }}
                        </td>
                        <td class="text-center">
                            {{ json_decode($cr->dadospessoais)->email }}
                        </td>
                        <td class="text-center">
                            {{ json_decode($cr->dadospessoais)->telefone }}
                        </td>
                        <td class="text-left">
                            <a href="{{route('app::curriculo::visualizar', ['id' => $cr->id])}}"><i class="material-icons">fullscreen</i></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

            <div class="row banner-list col-md-12">
                <div class="col-md-12">
                    @if ($curriculo->total() > 0)
                    Exibindo {{ $curriculo->count() }} de {{ $curriculo->total() }} currículos encontrados.
                    @else
                    Nenhum currículo encontrado.
                    @endif
                </div>
            </div>

            <div class="paginacao text-center">
                {!! $curriculo->links() !!}
            </div>
            
            
            @include('shared.erro-validacao')
            @include('flash::message')

        </section>
    </article>
</section>


@endsection
