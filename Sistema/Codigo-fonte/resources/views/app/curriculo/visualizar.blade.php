@extends('layouts.app')

@section('conteudo')

<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1 class="titulo-listagem">Currículo - {{ json_decode($dadospessoais->dadospessoais)->nome }}</h1>
            <div class="box-data-table">
                <div class="box-left-data-table">
                    <table class="table table-white table-striped table-hover data-table">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center">Dados Pessoais</th>
                            </tr>
                        </thead>
                        <tbody>


                            <tr>
                                <th class="text-center">Nome</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->nome }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Nascimento</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->nascimento }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">E-mail</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->email }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">CEP</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->cep }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Endereço</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->endereco }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Número</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->numero }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Complemento</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->complemento }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Bairro</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->bairro }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Cidade</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->cidade }}</td>
                            </tr>


                            <tr>
                                <th class="text-center">UF</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->uf }}</td>
                            </tr>
                            <tr>
                                <th class="text-center">DDD/Telefone</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->telefone }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">DDD/Celular</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->celular }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Estado Civil</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->estado_civil }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Nº de Filhos</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->filhos }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Como conheceu a Imab</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->conheceu }}</td>
                            </tr>

                            <tr>
                                <th colspan="2"></th>
                            </tr>

                            <tr>
                                <th colspan="2" class="text-center">Formação Acadêmica</th>
                            </tr>

                            <tr>
                                <th class="text-center">Instituição</th>
                                <td class="text-center">{{ json_decode($dadospessoais->formacao)->instituicao[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Curso</th>
                                <td class="text-center">{{ json_decode($dadospessoais->formacao)->curso[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Conclusão</th>
                                <td class="text-center">{{ json_decode($dadospessoais->formacao)->conclusao[0] }}</td>
                            </tr>

                            <tr>
                                <td colspan="2" class="text-center"></td>
                            </tr>

                            <tr>
                                <th class="text-center">Instituição</th>
                                <td class="text-center">{{ json_decode($dadospessoais->formacao)->instituicao[1] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Curso</th>
                                <td class="text-center">{{ json_decode($dadospessoais->formacao)->curso[1] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Conclusão</th>
                                <td class="text-center">{{ json_decode($dadospessoais->formacao)->conclusao[1] }}</td>
                            </tr>

                            <tr>
                                <th colspan="2"></th>
                            </tr>


                            <tr>
                                <th colspan="2" class="text-center">Espanhol</th>
                            </tr>
                            <tr>
                                <th class="text-center">Leitura</th>
                                <td class="text-center">{{ $espanhol->leitura[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Escrita</th>
                                <td class="text-center">{{ $espanhol->escrita[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Conversação</th>
                                <td class="text-center">{{ $espanhol->fala[0] }}</td>
                            </tr>

                            <tr>
                                <th colspan="2"></th>
                            </tr>

                            <tr>
                                <th colspan="2" class="text-center">Inglês</th>
                            </tr>

                            <tr>
                                <th class="text-center">Leitura</th>
                                <td class="text-center">{{ $ingles->leitura[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Escrita</th>
                                <td class="text-center">{{ $ingles->escrita[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Conversação</th>
                                <td class="text-center">{{ $ingles->fala[0] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="box-right-data-table">
                    <table class="table table-white table-striped table-hover data-table">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center">Aplicativos</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <th class="text-center col-md-5">Windows</th>
                                <td class="text-center">{{ json_decode($dadospessoais->aplicativos)->windows }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Internet</th>
                                <td class="text-center">{{ json_decode($dadospessoais->aplicativos)->internet }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Excel</th>
                                <td class="text-center">{{ json_decode($dadospessoais->aplicativos)->excel }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">PowerPoint</th>
                                <td class="text-center">{{ json_decode($dadospessoais->aplicativos)->powerpoint }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Word</th>
                                <td class="text-center">{{ json_decode($dadospessoais->aplicativos)->word }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Outros</th>
                                <td class="text-center">{{ json_decode($dadospessoais->aplicativos)->outros }}</td>
                            </tr>

                            <tr>
                                <th colspan="2"></th>
                            </tr>

                            <tr>
                                <th colspan="2" class="text-center">Experiência Profissional</th>
                            </tr>

                            <tr>
                                <th class="text-center">Empresa</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->empresa[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Cargo</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->cargo[0] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Início</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->inicio[0] }}</td>
                            </tr>
                            
                            <tr>
                                <th class="text-center">Término</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->termino[0] }}</td>
                            </tr>

                            <tr>
                                <td colspan="2" class="text-center"></td>
                            </tr>

                            <tr>
                                <th class="text-center">Empresa</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->empresa[1] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Cargo</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->cargo[1] }}</td>
                            </tr>

                            <tr>
                                <th class="text-center">Início</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->inicio[1] }}</td>
                            </tr>
                            
                            <tr>
                                <th class="text-center">Término</th>
                                <td class="text-center">{{ json_decode($dadospessoais->experiencia)->termino[1] }}</td>
                            </tr>

                            <tr>
                                <th colspan="2"></th>
                            </tr>


                            <tr>
                                <th colspan="2" class="text-center">Informação Complementar</th>
                            </tr>
                            <tr>
                                <th class="text-center">Pretensão Salarial</th>
                                <td class="text-center">{{ $dadospessoais->pretensao }}</td>
                            </tr>

                            <tr>
                                <th class="text-center col-md-5">Área de Interesse</th>
                                <td class="text-center">{{ json_decode($dadospessoais->dadospessoais)->area }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </article>
</section>


@endsection
