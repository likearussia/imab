@extends('layouts.app')


@section('conteudo')
<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1 class="titulo-listagem">Administradores</h1>
            <div class="btn-criar">
                <a href="{{route('app::usuario::criar')}}" class="btn btn-primary btn-menu right btn-add-new">
                    <i class="icone-menu pad-white material-icons">add_circle</i> 
                    Novo
                </a>
            </div>

            <table class="table table-white table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center col-md-2">Criado em</th>
                        <th class="text-center col-md-4">Nome</th>
                        <th class="text-center col-md-4">E-mail</th>
                        <th class="text-center col-md-1">Editar</th>
                        <th class="text-center col-md-1">Deletar</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($usuarios as $usuario)

                    <tr>
                        <td class="text-center">
                            {{$usuario->created_at->format('d/m/Y - H:i:s')}}
                        </td>
                        <td class="text-center">
                            {{$usuario->nome}}
                        </td>
                        <td class="text-center">
                            {{$usuario->email}}
                        </td>

                        <td class="text-left">
                            <a href="{{route('app::usuario::editar', ['id' => $usuario->id])}}" title="Editar" class="btn btn-primary bt-listagem btn-edit">
                                <i class="icone-deletar material-icons " title="Editar">edit</i>
                            </a>
                        </td>
                        <td class="text-left">
                            <a href="{{route('app::usuario::deletar', ['id' => $usuario->id])}}" title="Deletar" class="dialogo-deletar btn btn-primary bt-listagem btn-edit">
                                <i class="icone-deletar material-icons " title="Deletar">delete</i>
                            </a>
                        </td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
            
            <div class="row banner-list col-md-12">
                <div class="col-md-12">
                    @if ($usuarios->total() > 0)
                    Exibindo {{ $usuarios->count() }} de {{ $usuarios->total() }} usuários encontrados.
                    @else
                    Nenhum usuário encontrado.
                    @endif
                </div>
            </div>
            
            <div class="paginacao text-center">
                {!! $usuarios->links() !!}
            </div>
            
            @include('shared.erro-validacao')
            @include('flash::message')
        </section>
    </article>
</section>
@endsection