@extends('layouts.app')


@section('conteudo')

<section class="container">
    <article class="form-banner forms">

        <section class="form-outer">
            <h1>Administradores</h1>

            @include('shared.erro-validacao')
            @include('shared.erro-ajax')

            {{ Form::open(['route' => ['app::usuario::editar', 'id' => $usuario->id], 'class' => 'form-horizontal col-md-12 form-ajax semImagem']) }}
            <div class="form-group">
                <div class="col-md-12">
                    <label for="nome" class="control-label">Nome</label>
                    {{ Form::text('nome', $usuario->nome, ['placeholder' => 'Nome', 'id' => 'nome',
                                                    'required', 'class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="email" class="control-label">E-mail</label>
                    {{ Form::email('email', $usuario->email, ['placeholder' => 'E-mail', 'id' => 'email',
                                                    'required', 'class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label for="senha" class="control-label">Senha</label>
                    {{ Form::password('senha', ['placeholder' => 'Senha', 'id' => 'senha',
                                                        'class' => 'form-control']) }}
                </div>
                <div class="col-md-6">
                    <label for="senha_confirmation" class="control-label">Confirmação de Senha</label>
                    {{ Form::password('senha_confirmation', ['placeholder' => 'Senha', 'id' => 'senha_confirmation',
                                                        'class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group btn-save right">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary right">Salvar</button>
                </div>
            </div>
            {!! Form::close() !!}
        </section>
    </article>
</section>
@endsection
