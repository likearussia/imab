
@if(Auth::guard('usuarios_site')->check())
    @if(Auth::guard('usuarios_site')->getUser()->status == 0)
        <div class="modal-status-fundo"></div>
        <div class="modal"  id="LoginModal" style="display: block; visibility: visible">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Header -->
                    <div class="modal-header">
                        <h2 class="modal-title"> Área restrita</h2>
                        <p>Tenha vantagens e conteúdos exclusivos</p>
                    </div>
                    <!-- Body -->
                    <div class="modal-body" style="display: table;">
                        <div class="errors" style="display: block;">
                            <ul>
                                Aguarde a aprovação do seu cadastro para poder acessar esta área.
                            </ul>
                        </div>
                        <div class="right">
                            <a href="{{ route('site::index') }}"><button class="voltar btn-acao">Início</button></a>
                            <a href="{{ route('site::logout') }}"><button class="sair btn-acao" >Sair</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif