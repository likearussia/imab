<a href="{{ route('usuario::perfil') }}" class="{{ ($subpage == 'Perfil') ? 'active-user' : '' }}">perfil</a>
<a href="" data-toggle="modal" class="{{ ($subpage == 'Editar') ? 'active-user' : '' }}" data-target="#editModal">editar perfil</a>
<a href="" data-toggle="modal" class="{{ ($subpage == 'Editar') ? 'active-user' : '' }}" data-target="#editpass">Aterar senha</a>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title"> Editar perfil</h2>

            </div>

            <!-- Body -->
            <div class="modal-body">
                <div class="errors">
                    <ul>

                    </ul>
                </div>
                <div class="success">
                    <ul>

                    </ul>
                </div>
                {{ Form::open(['route' => ['usuario::editar', 'id' => $usuario['id']], 'class' => 'form-horizontal form-editar']) }}
                {{ csrf_field() }}
                <label for="#signup__email">Nome</label>
                <input type="text" name="name" class="form__control" id="signup__email"  value="{{$usuario['nome']}}"/>
                <label for="#signup__email">E-mail</label>
                <input type="text" name="username" class="form__control" id="signup__email" value="{{ $usuario['email'] }}"/>

                <div class="form__group">
                    <button class="btn btn__login btnCadastrar" >Alterar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="editpass">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title"> Alterar senha</h2>

            </div>

            <!-- Body -->
            <div class="modal-body">
                <div class="errors">
                    <ul>

                    </ul>
                </div>
                <div class="success">
                    <ul>

                    </ul>
                </div>
                {{ Form::open(['route' => ['usuario::alterar-senha', 'id' => $usuario['id']], 'class' => 'form-horizontal form-senha']) }}
                {{ csrf_field() }}
                <label for="#signup__password">Senha atual</label>
                <input type="password" name="password" class="form__control" id="signup__password" />
                <label for="#signup__password">Nova senha</label>
                <input type="password" name="newpassword" class="form__control" id="signup__password" />
                <label for="#signup__password">Confirme a nova senha</label>
                <input type="password" name="newpassword_confirmation" class="form__control" id="signup__password" />

                <div class="form__group">
                    <button class="btn btn__login btnCadastrar" >Alterar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>