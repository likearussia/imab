<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>IMAB Fechaduras</title>
        <META charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <base href="{{url('/')}}/">
        <!-- o stack ira puxar css's internos das páginas caso exista a chamada. -->
        @stack('css')
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.typekit.net/c/7640b5/1w;myriad-pro,7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191,ftg:W:i4,ftc:W:n3,ftf:W:n4,fth:W:n6,ftk:W:n7/l" media="all">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <!-- Login header --> 

        <!-- Fim do Login Header -->
        <!-- fim menu --> 
        <!-- Static navbar principal -->
        <div class="nav-top container-fluid">
            <div class="container">

            </div>
        </div>
        <nav class="navbar navbar-default yamm">
            <div class="container">

            </div>

            <div class="container">
                <div class="col-md-2 col-sm-3">
                    <div class="navbar-header">
                        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="{{url('/')}}" class="navbar-brand">
                            <img src="assets/imagens/logo-imab.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-9">
                    <div id="navbar-collapse-grid" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <!-- Grid 12 Menu -->
                            <li><a href="{{url('/')}}">Home</a>

                            </li>
                            <!--
                            <With>Offsets </With>
                            -->
                            <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">A IMAB<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="grid-demo imab">
                                        <div class="row">
                                            <div class="menu-imab col-md-3 col-sm-3 col-xs-6">
                                                <a href="{{url('historia')}}"><h3>História</h3><img src="assets/imagens/icons/historia.png" alt=""></a>
                                            </div>
                                            <div class="menu-imab col-md-3 col-sm-3 col-xs-6">
                                                <a href="{{url('valores-humanos')}}"><h3>Valores humanos</h3><img src="assets/imagens/icons/valores.png" alt=""></a>
                                            </div>
                                            <div class="menu-imab col-md-3 col-sm-3 col-xs-6">
                                                <a href="{{url('inovacao-e-qualidade')}}"><h3>Inovação e qualidade</h3><img src="assets/imagens/icons/inovacao.png" alt=""></a>
                                            </div>
                                            <div class="menu-imab col-md-3 col-sm-3 col-xs-6">
                                                <a href="{{url('cidadania-corporativa')}}"><h3>Cidadania corporativa</h3> <img src="assets/imagens/icons/cidadania.png" alt=""></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!--
                            <Aside>Menu </Aside>
                            -->
                        <li class="dropdown yamm-fw"><a href="produtos.html" data-toggle="dropdown" class="dropdown-toggle">Produtos<b class="caret"></b></a>
                                <ul class="dropdown-menu withc">
                                    <li class="custom"> 
                                    </li>
                                    <li class="grid-demo menu-produtos">
                                        <div class="row">
                                            <div class="item-menu custom">
                                                <a href="{{url('personalize')}}">
                                                    <h3>Personalize</h3>
                                                    <img src="assets/imagens/icons/personalize.png" alt="">
                                                    <p>Crie sua maçaneta <span class="txt-personalizada">personalizada</span></p>
                                                </a>
                                            </div>
                                            <div class="item-menu menu-prod">
                                                <a href="{{url('modelos')}}"><h3>Modelos</h3> <img src="assets/imagens/icons/linhas.png" alt=""></a>
                                            </div>
                                            <div class="item-menu menu-prod">
                                                <a href="{{url('fechaduras')}}"><h3>Fechaduras</h3> <img src="assets/imagens/icons/fechadura.png" alt=""></a>
                                            </div>
                                            <div class="item-menu menu-prod">
                                                <a href="{{url('puxadores')}}"><h3>Puxadores</h3> <img src="assets/imagens/icons/puxadores.png" alt=""></a>
                                            </div>
                                            <div class="item-menu menu-prod">
                                                <a href="{{url('dobradicas-pivots')}}"><h3>Dobradiças e Pivots</h3> <img src="assets/imagens/icons/dobradicas.png" alt=""></a>
                                            </div>
                                            <div class="item-menu menu-prod">
                                                <a href="{{url('ferragens')}}"><h3>Ferragens</h3> <img src="assets/imagens/icons/ferragens.png" alt=""></a>
                                            </div>
                                            <div class="item-menu menu-prod">
                                                <a href="{{url('componentes')}}"><h3>Componentes</h3> <img src="assets/imagens/icons/componentes.png" alt=""></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            
                            
                            
                            
                            
                            <li><a href="{{url('dicas-e-solucoes')}}">Dicas e soluções</a></li>
                            <li><a href="{{url('fale-conosco')}}">Fale conosco</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">

                    <div class="search-top">
                        <form method="post" class="form-search" action="{{ route('site::buscar') }}">
                            {{ csrf_field() }}
                            <input type="search" class="field-search" name="busca"/>
                            <button class="search-submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <div class="row login-user"> 
                        @if(Auth::guard('usuarios_site')->check()) 
                        <div class="btn-user-home">
                            <p class="msg-user"><span class="msg-index">Olá, {{ Auth::guard('usuarios_site')->getUser()->nome }} <i class="fa fa-caret-down" aria-hidden="true"></i> </span></p>
                            <div class="has-dropdown">
                                <a href="{{ route('usuario::perfil') }}">Meu perfil</a>
                                <a href="{{ route('site::logout') }}">sair</a>
                            </div>
                        </div>
                        @else
                        <div class="topo-painel"> 
                            <ul class="top-login">
                                <li><a href="#" data-toggle="modal" data-target="#LoginModal" class="entrar">Entrar</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#SignModal" class="cadastrar" >Cadastrar</a></li>
                            </ul> 
                        </div>
                        @endif
                    </div>    
                </div>
            </div>
        </nav>

        <!-- Login modal -->

        <div class="modal fade ModalEntrar" tabindex="1" role="dialog" id="LoginModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2 class="modal-title"> Seja Bem-Vindo(a) à IMAB</h2>
                        <p>Tenha vantagens e conteúdos exclusivos</p>
                    </div>

                    <!-- Body -->
                    <div class="modal-body">
                        <div class="errors">
                            <ul>

                            </ul>
                        </div>
                        <div class="success">
                            <ul>

                            </ul>
                        </div>
                        <form action="{{ route('site::logar') }}" class="form-logar" method="post">
                            {{ csrf_field() }}
                            <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                <label for="#signup__email">Email</label>
                                <input type="text" name="username" class="form__control _64-email col-md-12 col-sm-12 col-xs-12" id="signup__email" />
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                <label for="#signup__password">Senha</label>
                                <input type="password" name="password" class="form__control _64-senha col-md-12 col-sm-12 col-xs-12" id="signup__password" />
                            </div>
<!--
                            <div class="login__options">

                                <label class="label--checkbox">
                                    <input type="checkbox" class="checkbox">
                                    Me lembrar
                                </label>
                                
                            </div>
-->

                            <div class="form__group">
                                <button class="btn btn__login">FAZER LOGIN</button>
                            </div>
                            <div class="signup__link">
                                <span>Não tem cadastro? <a href="#" data-toggle="modal" data-target="#SignModal">Cadastrar</a></span>
                                <a href="#" data-toggle="modal" data-target="#ForgotModal" class="forgot__password">Esqueceu sua senha?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade ModalCadastrar" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="SignModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2 class="modal-title"> CADASTRE-SE AGORA</h2>
                        <p>Tenha vantagens e conteúdos exclusivos</p>

                        <div class="perfis-cadastro">
                            <p class="consumidor active">Consumidor</p>
                            <p class="revendedor" >Revendedor</p>
                            <p class="imprensa">Imprensa</p>
                        </div>
                    </div>

                    <!-- Body -->
                    <div class="modal-body">
                        <div class="errors">
                            <ul>

                            </ul>
                        </div>
                        <div class="success">
                            <ul>

                            </ul>
                        </div>

                        <form class="form-cadastrar fields-consumidor fields-hide" action="{{route('site::cadastrar')}}" method="post">
                            {{ csrf_field() }}

                            @include('shared.erro-validacao')
                            <div class="">
                                <input type="hidden" name="tipo_usuario" value="1"/>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="name" class="form__control col-md-12 col-sm-12 col-xs-12" id="nome" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="email">E-mail</label>
                                    <input type="text" name="username" class="form__control col-md-12 col-sm-12 col-xs-12 _64-email" id="email" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box fields-margin">
                                    <label for="conf-email">Confirmar E-mail</label>
                                    <input type="text" name="username_confirmation" class="form__control col-md-12 col-sm-12 col-xs-12 _64-email" id="conf-email" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="senha">Senha</label>
                                    <input type="password" name="password" class="form__control col-md-12 col-sm-12 col-xs-12 _64-senha" id="senha" />    
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box fields-margin">
                                    <label for="conf-senha">Confirmar Senha</label>
                                    <input type="password" name="password_confirmation" class="form__control col-md-12 col-sm-12 col-xs-12 _64-senha" id="conf-senha" />    
                                </div>

                                <div class="col-md-8 col-sm-7 col-xs-12 fields-box">
                                    <label for="cidade">Cidade</label>
                                    <input type="text" name="cidade" class="form__control col-md-12 col-sm-12 col-xs-12" id="cidade" />
                                </div>

                                <div class="col-md-4 col-sm-5 fields-box fields-margin">
                                    <label for="estado">Estado</label>
                                    <select class="select-field" name="estado" id="profissao">
                                        <option value="estado">Selecione o Estado</option> 
                                        <option value="ac">Acre</option> 
                                        <option value="al">Alagoas</option> 
                                        <option value="am">Amazonas</option> 
                                        <option value="ap">Amapá</option> 
                                        <option value="ba">Bahia</option> 
                                        <option value="ce">Ceará</option> 
                                        <option value="df">Distrito Federal</option> 
                                        <option value="es">Espírito Santo</option> 
                                        <option value="go">Goiás</option> 
                                        <option value="ma">Maranhão</option> 
                                        <option value="mt">Mato Grosso</option> 
                                        <option value="ms">Mato Grosso do Sul</option> 
                                        <option value="mg">Minas Gerais</option> 
                                        <option value="pa">Pará</option> 
                                        <option value="pb">Paraíba</option> 
                                        <option value="pr">Paraná</option> 
                                        <option value="pe">Pernambuco</option> 
                                        <option value="pi">Piauí</option> 
                                        <option value="rj">Rio de Janeiro</option> 
                                        <option value="rn">Rio Grande do Norte</option> 
                                        <option value="ro">Rondônia</option> 
                                        <option value="rs">Rio Grande do Sul</option> 
                                        <option value="rr">Roraima</option> 
                                        <option value="sc">Santa Catarina</option> 
                                        <option value="se">Sergipe</option> 
                                        <option value="sp">São Paulo</option> 
                                        <option value="to">Tocantins</option> 
                                    </select>
                                </div>

                                <div class="col-md-12 col-sm-12 fields-box" style="padding-bottom: 10px">
                                    <label for="profissao">Profissão</label>
                                    <select class="select-field" name="profissao" id="profissao">
                                        <option>Decorador</option>
                                        <option>Designer de interiores</option>
                                        <option>Arquiteto</option>
                                        <option>Engenheiro</option>
                                        <option>Construtor</option>
                                        <option>Outros</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form__group">
                                <button class="btn btn__login btnCadastrar">Cadastrar</button>
                            </div>

                        </form>

                        <form class="form-cadastrar fields-revendedor fields-hide" action="{{route('site::cadastrar')}}" method="post">
                            {{ csrf_field() }}
                            @include('shared.erro-validacao')
                            <div class="">
                                <input type="hidden" name="tipo_usuario" value="3"/>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="email">E-mail</label>
                                    <input type="text" name="username" class="form__control col-md-12 col-sm-12 col-xs-12 _64-email" id="email" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box fields-margin">
                                    <label for="conf-email">Confirmar E-mail</label>
                                    <input type="text" name="username_confirmation" class="form__control col-md-12 col-sm-12 col-xs-12 _64-email" id="conf-email" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="senha">Senha</label>
                                    <input type="password" name="password" class="form__control col-md-12 col-sm-12 col-xs-12 _64-senha" id="senha" />    
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box fields-margin">
                                    <label for="conf-senha">Confirmar Senha</label>
                                    <input type="password" name="password_confirmation" class="form__control col-md-12 col-sm-12 col-xs-12 _64-senha" id="conf-senha" />    
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box" style="padding-bottom: 10px">
                                    <label for="segmento">Segmento de Atuação</label>
                                    <select class="select-field" id="segmento" name="segmento">
                                        <option>Ferragem</option>
                                        <option>Home Center</option>
                                        <option>Materiais de Construção</option>
                                        <option>Loja de Decoração</option>
                                    </select>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="cnpj">CNPJ</label>
                                    <input type="text" name="cnpj" class="form__control col-md-12 col-sm-12 col-xs-12 mask-cnpj" id="cnpj" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box fields-margin">
                                    <label for="razao_social">Razão Social</label>
                                    <input type="text" name="razao_social" class="form__control col-md-12 col-sm-12 col-xs-12" id="razao_social" />    
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="fantasia">Nome Fantasia</label>
                                    <input type="text" name="fantasia" class="form__control col-md-12 col-sm-12 col-xs-12" id="fantasia" />    
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="telefone">Telefone</label>
                                    <input type="text" name="telefone" class="form__control col-md-12 col-sm-12 col-xs-12 mask-telefone" id="telefone" />
                                </div>

                                <div class="col-md-8 col-sm-7 col-xs-12 fields-box">
                                    <label for="cidade_revendedor">Cidade</label>
                                    <input type="text" name="cidade" class="form__control col-md-12 col-sm-12 col-xs-12" id="cidade_revendedor" />    
                                </div>

                                <div class="col-md-4 col-sm-5 col-xs-12 fields-box fields-margin">
                                    <label for="estado_revendedor">Estado</label>
                                    <select class="select-field" name="estado" id="profissao">
                                        <option value="estado">Selecione o Estado</option> 
                                        <option value="ac">Acre</option> 
                                        <option value="al">Alagoas</option> 
                                        <option value="am">Amazonas</option> 
                                        <option value="ap">Amapá</option> 
                                        <option value="ba">Bahia</option> 
                                        <option value="ce">Ceará</option> 
                                        <option value="df">Distrito Federal</option> 
                                        <option value="es">Espírito Santo</option> 
                                        <option value="go">Goiás</option> 
                                        <option value="ma">Maranhão</option> 
                                        <option value="mt">Mato Grosso</option> 
                                        <option value="ms">Mato Grosso do Sul</option> 
                                        <option value="mg">Minas Gerais</option> 
                                        <option value="pa">Pará</option> 
                                        <option value="pb">Paraíba</option> 
                                        <option value="pr">Paraná</option> 
                                        <option value="pe">Pernambuco</option> 
                                        <option value="pi">Piauí</option> 
                                        <option value="rj">Rio de Janeiro</option> 
                                        <option value="rn">Rio Grande do Norte</option> 
                                        <option value="ro">Rondônia</option> 
                                        <option value="rs">Rio Grande do Sul</option> 
                                        <option value="rr">Roraima</option> 
                                        <option value="sc">Santa Catarina</option> 
                                        <option value="se">Sergipe</option> 
                                        <option value="sp">São Paulo</option> 
                                        <option value="to">Tocantins</option> 
                                    </select>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="estado_revendedor">Site</label>
                                    <input type="text" name="site" class="form__control col-md-12 col-sm-12 col-xs-12 mask-site" id="estado_revendedor" />
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 divisoria">
                                    <label for="#signup__email">Informações do Responsável</label>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="nome_responsavel">Nome</label>
                                    <input type="text" name="name" class="form__control col-md-12 col-sm-12 col-xs-12" id="nome_responsavel" />    
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="cargo_responsavel">Cargo</label>
                                    <input type="text" name="profissao" class="form__control col-md-12 col-sm-12 col-xs-12" id="cargo_responsavel" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-margin fields-box">
                                    <label for="cargo_responsavel">Telefone / Celular</label>
                                    <input type="text" name="celular" class="form__control col-md-12 col-sm-12 col-xs-12 mask-telefone" id="cargo_responsavel" />
                                </div>

                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 fields-box form__group" style="padding-bottom: 10px; margin-top: 5px;">
                                <button class="btn btn__login btnCadastrar">Cadastrar</button>
                            </div>

                        </form>


                        <form class="form-cadastrar fields-imprensa fields-hide" action="{{route('site::cadastrar')}}" method="post">
                            {{ csrf_field() }}

                            @include('shared.erro-validacao')
                            <div class="">
                                <input type="hidden" name="tipo_usuario" value="2"/>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="email">E-mail</label>
                                    <input type="text" name="username" class="form__control col-md-12 col-sm-12 col-xs-12 _64-email" id="email" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box fields-margin">
                                    <label for="conf-email">Confirmar E-mail</label>
                                    <input type="text" name="username_confirmation" class="form__control col-md-12 col-sm-12 col-xs-12 _64-email" id="conf-email" />
                                </div>

                                <div class="col-md-6 col-sm-6 fields-box">
                                    <label for="senha">Senha</label>
                                    <input type="password" name="password" class="form__control col-md-12 col-xs-12 _64-senha" id="senha" />    
                                </div>

                                <div class="col-md-6 col-sm-6 fields-box fields-margin">
                                    <label for="conf-senha">Confirmar Senha</label>
                                    <input type="password" name="password_confirmation" class="form__control col-md-12 col-xs-12 _64-senha" id="conf-senha" />    
                                </div>

                               <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="empresa">Empresa / Instituição</label>
                                    <input type="text" name="fantasia" class="form__control col-md-12 col-sm-12 col-xs-12" id="empresa" />    
                                </div>
                               
                                <div class="col-md-6 col-sm-6 fields-box" style="padding-bottom: 10px">
                                    <label for="#signup__email">Tipo de Veículo</label>
                                    <select class="select-field" name="segmento">
                                        <option>Agência de Notícias</option>
                                        <option>Internet</option>
                                        <option>Jornal / Revista</option>
                                        <option>Rádio</option>
                                        <option>Televisão</option>
                                    </select>
                                </div>


                                <div class="col-md-6 col-sm-6 fields-box fields-margin">
                                    <label for="telefone_imprensa">Telefone</label>
                                    <input type="text" name="telefone" class="form__control col-md-12 col-xs-12 mask-telefone" id="telefone_imprensa" />
                                </div>

                                <div class="col-md-8 col-sm-7 col-xs-12 fields-box">
                                    <label for="cidade_imprensa">Cidade</label>
                                    <input type="text" name="cidade" class="form__control col-md-12 col-sm-12 col-xs-12" id="cidade_imprensa" />    
                                </div>

                                <div class="col-md-4 col-sm-5 col-xs-12 fields-box fields-margin">
                                    <label for="estado_imprensa">Estado</label>
                                    <select class="select-field" name="estado" id="profissao">
                                        <option value="estado">Selecione o Estado</option> 
                                        <option value="ac">Acre</option> 
                                        <option value="al">Alagoas</option> 
                                        <option value="am">Amazonas</option> 
                                        <option value="ap">Amapá</option> 
                                        <option value="ba">Bahia</option> 
                                        <option value="ce">Ceará</option> 
                                        <option value="df">Distrito Federal</option> 
                                        <option value="es">Espírito Santo</option> 
                                        <option value="go">Goiás</option> 
                                        <option value="ma">Maranhão</option> 
                                        <option value="mt">Mato Grosso</option> 
                                        <option value="ms">Mato Grosso do Sul</option> 
                                        <option value="mg">Minas Gerais</option> 
                                        <option value="pa">Pará</option> 
                                        <option value="pb">Paraíba</option> 
                                        <option value="pr">Paraná</option> 
                                        <option value="pe">Pernambuco</option> 
                                        <option value="pi">Piauí</option> 
                                        <option value="rj">Rio de Janeiro</option> 
                                        <option value="rn">Rio Grande do Norte</option> 
                                        <option value="ro">Rondônia</option> 
                                        <option value="rs">Rio Grande do Sul</option> 
                                        <option value="rr">Roraima</option> 
                                        <option value="sc">Santa Catarina</option> 
                                        <option value="se">Sergipe</option> 
                                        <option value="sp">São Paulo</option> 
                                        <option value="to">Tocantins</option> 
                                    </select>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="site">Site</label>
                                    <input type="text" name="site" class="form__control col-md-12 col-sm-12 col-xs-12" id="site" />    
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 divisoria">
                                    <label for="#signup__email">Informações do Profissional de Imprensa</label>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 fields-box">
                                    <label for="nome_imprensa">Nome</label>
                                    <input type="text" name="name" class="form__control col-md-12 col-sm-12 col-xs-12" id="nome_imprensa" />    
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-box">
                                    <label for="cargo_imprensa">Cargo</label>
                                    <input type="text" name="profissao" class="form__control col-md-12 col-sm-12 col-xs-12" id="cargo_imprensa" />
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 fields-margin">
                                    <label for="telefone_imprensa">Telefone/Cellular</label>
                                    <input type="text" name="celular" class="form__control col-md-12 col-sm-12 col-xs-12 mask-telefone" id="telefone_imprensa" />
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 fields-box form__group" style="padding-bottom: 10px; margin-top: 5px;">
                                <button class="btn btn__login btnCadastrar">Cadastrar</button>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>


        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="ForgotModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2 class="modal-title"> ESQUECEU A SENHA?</h2>
                        <p>Coloque seu email cadastrado</p>
                    </div>

                    <!-- Body -->
                    <div class="modal-body">
                        <div class="errors">
                            <ul>

                            </ul>
                        </div>
                        <div class="success">
                            <ul>

                            </ul>
                        </div>
                        <form class="form-recuperar" method="post" action="{{ route('site::recuperar') }}">
                            {{ csrf_field() }}
                            <label for="#signup__email">Email</label>
                            <input type="text" name="username" class="form__control" id="signup__email" />


                            <div class="form__group">
                                <button class="btn btn__login">ENVIAR</button>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>

        <!-- FIM DOS MODAIS DE LOGIN -->

        <!-- @yeld() é responsável por carregar o conteudo colocados nos templates que extemdem layouts.site, acima e abaixo encontramos o cabeçalho e o rodapé. -->

        @yield('conteudo')

        <!-- Inicio Duvidas -->
        <div class="container-fluid box-duvida-contato-social">
            <div class="container duvida">
                <div class="col-md-3 col-sm-4 col-xs-12 box-duvidas">
                    <a href="{{url('duvidas')}}"><img src="http://www.ph2propaganda.com.br/clientes/imab/images/icon-faleconosco.png"  align="left" alt="duvidas">
                        <div class="tire-duvidas">TIRE SUAS <br> DÚVIDAS</div></a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 box-fale-conosco">
                    <img src="http://www.ph2propaganda.com.br/clientes/imab/images/icon-telefone.png" align="left" alt="Fale-Conosco">
                    <div>FALE CONOSCO <br> (11)4662-7500</div>
                </div>
                <div class="col-md-6 col-sm-4 col-xs-12  box-rede-social">
                    <div>ACOMPANHE-NOS <br>NAS REDES SOCIAIS</div>
                    <ul class="redes">
                        <li><a href="https://www.facebook.com/sharer.php?u=http://www.imab.com.br/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium&original_referer=http%3A%2F%2Fwww.imab.com.br%2Fconjuntos%2Flinha-premium" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/imabfechaduras/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://br.pinterest.com/imabfechaduras/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Fim  Duvidas-->

        <!-- Inicio footer -->
        <div class="container-fluid box-footer">
            <div class="container box-footer-conteudo">

                <div class="col-md-3 col-sm-6 col-xs-12 box-footer-institucional">
                    <ul id="menu-footer">
                        <li>A IMAB</li>
                        <li><a class="link-footer" href="{{url('historia')}}">História</a></li>
                        <li><a class="link-footer" href="{{url('valores-humanos')}}">Valores Humanos</a></li>
                        <li><a class="link-footer" href="{{url('inovacao-e-qualidade')}}">Inovação e Qualidade</a></li>
                        <li><a class="link-footer" href="{{url('cidadania-corporativa')}}">Cidadania Corporativa</a></li>
                        <li><a class="link-footer" href="{{url('dicas-e-solucoes')}}">Dicas e Soluções</a></li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 box-footer-produtos">

                    <ul class="box-produto1-ul">
                            <li>Produtos</li>
                            <li><a class="link-footer" href="{{url('modelos')}}">Modelos</a></li>
                            <li><a class="link-footer" href="{{url('fechaduras')}}">Fechaduras</a></li>
                            <li><a class="link-footer" href="{{url('puxadores')}}">Puxadores</a></li>
                            <li><a class="link-footer" href="{{url('dobradicas-pivots')}}">Dobradiças e Pivots</a></li>
                            <li><a class="link-footer" href="{{url('ferragens')}}">Ferragens</a></li>
                            <li><a class="link-footer" href="{{url('componentes')}}">Componentes</a></li>

                        </ul>  
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-12 box-footer-personalize">
                    <ul class="box-produto2-ul">
                            <li>Personalize</li>
                            <li>
                                <a class="link-footer" href="{{url('personalize')}}">
                                    <p>Escolha o material a superfície e o acabamento da fechadura que quiser!</p>
                                </a>
                            </li>
                        </ul>
                </div>                

                <div class="col-md-3 col-sm-6 col-xs-12 box-footer-Fale-conosco">
                    <ul>
                        <li>Fale conosco</li>
                        <li><a class="link-footer" href="{{url('fale-conosco')}}">Atendimento Comercial</a></li>
                        <li><a class="link-footer" href="{{ route('trabalhe::index') }}">Trabalhe Conosco</a></li>
                        <li><a class="link-footer" href="{{ route('arquivos') }}">Downloads</a></li>
                    </ul>
                </div>
                
                
                
                
                <!-- Copyright -->
                <br>
            </div>
            <div class="container box-copyright">
                <span class="texto-copyright">Copyright © 2016 - IMAB Fechaduras todos os direitos reservados. </span>
                <span class="texto-create-by"><a  class="link-footer" href="#">Ph2 Full Creativity</a></span>
            </div>
        </div>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
        <script src="https://use.fontawesome.com/1d57cf7334.js"></script>
        <script src="js/site.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>

        <script>
            $(document).ready(function(){
                $('.mask-telefone').mask('(99) 9999-99999');
                $('.mask-cnpj').mask("99.999.999/9999-99");
                $('.mask-ano').mask("9999");
                $('.mask-nascimento').mask("99/99/9999");
                $('.mask-mesano').mask("99/9999");
                $('.mask-valor').mask('00.000,00', {reverse: true});
            })
        </script>
        
<script>
// Get the modal
var modal = document.getElementById('ModalEntrar');
var modal = document.getElementById('ModalCadastrar');

// Get the button that opens the modal
var btn = document.getElementById("entrar");
var btn = document.getElementById("cadastrar");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

        
        <!-- o stack ira puxar scripts internos das páginas caso exista a chamada. -->
        @stack('scripts')

        <div class="hide">

        </div>
        
        

    </body>
    
</html>


