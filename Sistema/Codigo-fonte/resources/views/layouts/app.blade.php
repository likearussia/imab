<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @stack('meta')

        <title>Imab @yield('title')</title>
        <base href="{{ url('/') }}/" />

        <!-- Fontes -->
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

        <!-- Estilos -->
        <link href="dist/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="dist/bootstrap-material-design/css/bootstrap-material-design.min.css" rel="stylesheet" type="text/css">
        <link href="dist/bootstrap-material-design/css/ripples.min.css" rel="stylesheet" type="text/css">
        <link href="css/app/login.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="css/app.css"/>
        <script src="dist/sweetalert2/sweetalert2.min.js"></script> 
        <link rel="stylesheet" type="text/css" href="dist/sweetalert2/sweetalert2.css">

        @stack('css')

        <!-- Scripts -->
        @stack('scripts-cabecalho')
    </head>
    <body id="app-layout">
        <section class='conteudo'>
            <header class="header-adm container-fluid">
                <section class="container">
                    <div class="logo-adm" >
                        <img src="assets/imagens/logo-imab.png"/ class="logo-ori">
                        <img src="assets/imagens/logo-imab_2.png"/ class="logo-print">
                    </div>
                    <ul class="nav nav-app nav-tabs" style="margin-bottom: 15px;">
                        <li class="dropdown first">
                            <a class="dropdown-toggle first" href="{{ route('app::inicio') }}">
                                <i class="icone-menu material-icons">home</i>
                            </a>
                        </li>

                        <li class="dropdown first">
                            <a class="dropdown-toggle first" data-toggle="dropdown" href="" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">image</i> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('app::banner::criar') }}"><i class="icone-menu pad-white material-icons">add_circle</i> Novo</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{route('app::banner::index')}}"><i class="icone-menu pad-white material-icons">list</i>  Listar</a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown first">
                            <a class="dropdown-toggle first"href="{{route('app::institucional::index')}}" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">location_city</i>
                            </a>
                        </li>

                        <li class="dropdown first">
                            <a class="dropdown-toggle first" data-toggle="dropdown" href="" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">lightbulb_outline</i> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('app::dicas::criar') }}"><i class="icone-menu pad-white material-icons">add_circle</i>Novo</a>
                                </li>

                                <li>
                                    <a href="{{route('app::dicas::index')}}"><i class="icone-menu pad-white material-icons">list</i> Listar</a>
                                </li>

                                <li class="divider"></li>

                                <span style="padding-left: 5px;">Categorias</span>

                                <li>
                                    <a href="{{ route('app::dicas::categoria::criar') }}"><i class="icone-menu pad-white material-icons">add_circle</i>Nova</a>
                                </li>

                                <li>
                                    <a href="{{route('app::dicas::categoria::index')}}"><i class="icone-menu pad-white material-icons">list</i> Listar</a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown first">
                            <a class="dropdown-toggle first"  data-toggle="dropdown" href="" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">file_download</i> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('app::download::criar') }}"><i class="icone-menu pad-white material-icons">add_circle</i>Novo</a>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <a href="{{route('app::download::index')}}"><i class="icone-menu pad-white material-icons">list</i> Listar</a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown first">
                            <a class="dropdown-toggle first"href="{{route('app::usuarios-site::index')}}" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">people</i>
                            </a>
                        </li>

                        <li class="dropdown first">
                            <a class="dropdown-toggle first"href="{{route('app::curriculo::index')}}" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">content_paste</i>
                            </a>
                        </li>


                        <li class="dropdown first">
                            <a class="dropdown-toggle first" data-toggle="dropdown" href="" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">account_circle</i> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('app::usuario::criar') }}"><i class="icone-menu pad-white material-icons">add_circle</i>Novo</a>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <a href="{{route('app::usuario::index')}}"><i class="icone-menu pad-white material-icons">list</i> Listar</a>
                                </li>
                            </ul>
                        </li>


                        <li class="dropdown first">
                            <a class="dropdown-toggle first"href="{{route('app::configuracoes::index')}}" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">settings</i>
                            </a>
                        </li>

                        <li class="dropdown first">
                            <a class="dropdown-toggle first"href="{{route('app::news::index')}}" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">description</i>
                            </a>
                        </li>

                        <li class="dropdown first user">
                            <a class="dropdown-toggle first" data-toggle="dropdown" href="bootstrap-elements.html" data-target="#" aria-expanded="true">
                                <i class="icone-menu material-icons">account_circle</i> {{ Auth::user()->nome }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('app::logout')}}">Sair</a></li>
                                <!--<li class="divider"></li>-->
                            </ul>
                        </li>

                    </ul>
                </section>
            </header>
            <section class="breadcrumb-adm container-fluid">
                <article class="breadcrumb-content container">
                    <a href="{{route('app::inicio')}}"><i class="material-icons breadicon">home</i></a><span class="text-bread" >

                        <?php (!isset($page)) ? $page = '' : ''; ?>
                        @if($page != '')    
                        >  {{ $page }}
                        @endif
                        <?php (!isset($subpage)) ? $subpage = '' : ''; ?>
                        @if($subpage != '')
                        > {{ $subpage }}
                        @endif

                    </span>
                </article>
            </section>
            <section class="content">
                @yield('conteudo')
            </section>
        </section>

        <!-- JavaScripts -->
        <script src="dist/jquery/jquery.min.js"></script>
        <script src="dist/bootstrap/js/bootstrap.min.js"></script>
        <script src="dist/bootstrap-material-design/js/material.min.js"></script>
        <script src="dist/bootstrap-material-design/js/ripples.min.js"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script>
        <script src="js/app/app.js"></script>
        <script src="http://malsup.github.com/jquery.form.js"></script>
        <link  href="assets/cropper/cropper.css" rel="stylesheet">
        <script src="assets/cropper/cropper.js"></script>
        @stack('scripts')
    </body>
</html>