/**
 * Lógica global para a aplicação
 */

$(document).ready(function () {


    $('.cm').change(function(){
        $(this).toggleClass("some_adc");
        if($(this).hasClass("some_adc")){
            $('.adc').attr("disabled", true);
            $('.adc1').attr("disabled", true);
        }else{
            $('.adc').attr("disabled", false);
            $('.adc1').attr("disabled", false);
        }
    });

    $('.adc').change(function(){

        $(this).toggleClass("some_cm");

        if($(this).hasClass("some_cm")){
            $('.cm').attr("disabled", true);
        }else if($('.adc1').hasClass("some_cm")){
            $('.cm').attr("disabled", true);
        }else{
            $('.cm').attr("disabled", false);
        }

    });

    $('.adc1').change(function(){

        $(this).toggleClass("some_cm");

        if($(this).hasClass("some_cm")){
            $('.cm').attr("disabled", true);
        }else if($('.adc').hasClass("some_cm")){
            $('.cm').attr("disabled", true);
        }else{
            $('.cm').attr("disabled", false);
        }


    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    /*
    * Requisição ajax para cadastro e edicao de arquivos para download
    *
    *
    * */
    $('.form-ajax').submit(function () {
        var dataForm = $(this).serialize();
        var url = $(this).attr('action');

        if(!$(this).hasClass("semImagem")){
            $.ajaxSetup({
                processData: false,
                contentType: false
            })
        }else{
        }

        $(this).ajaxSubmit({
            type: "POST",
            data: dataForm,
            uploadProgress: function (event, position, total, percentComplete) {
                console.log(percentComplete);
            },
            url: url,
            success: function(data){

                if(data == 1){
                    var dadosJSON = {"nome":["Essa categoria ja esta cadastrada."]}

                    erroSistema(dadosJSON);

                    return false;
                }

                if(data != 0){
                    console.log(data);
                    var dados = JSON.parse(data);
                    sucesso(dados.mensagem, dados.url_to);
                }
            },
            error: function (err) {
                //$('body').append(err.responseText);
                console.log(err);
                erro(err);
            },
            beforeSend: function () {
                before();
            }
        });
        return false;
    });



    centralizaAltura(".center-child", window);


    $('.changeUserType').change(function () {

        $user = $(this);
        //alert($(this).prev().val());
        swal({
            title: 'Você tem certeza que deseja mudar o status do usuário?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0fb2fc',
            cancelButtonColor: '#d41414',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            $.ajax({
                url: $user.prev().prev().val() + '/' + $user.val(),
                type: 'GET',
                beforeSend: function (xhr) {

                },
                error: function (jqXHR, textStatus, errorThrown) {

                },
                success: function (data, textStatus, jqXHR) {
                    if (data == 1) {
                        swal({
                            title: "Tipo de usuário alterado com sucesso!",
                            text: "",
                            timer: 1000,
                            showConfirmButton: false
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1000); 
                    }else{
                        swal({
                            title: "Erro ao alterar, tente novamente!",
                            text: "",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }

                }
            });
        });
    });



    $.material.init();

    // Ativa o efeito criado pelo material.js de uma maneira que torna
    // mais fácil para o usuário ver a descrição do campo
    // Markup: form-group grupo-upload-imagem
    $('.grupo-upload-imagem')
            .mouseenter(function () {
                $(this).closest('.form-group').addClass('is-focused');
            })
            .mouseout(function () {
                $(this).closest('.form-group').removeClass('is-focused');
            });

    $('.dialogo-deletar').on('click', function (e) {
        e.preventDefault();

        href = $(this).attr('href');

        swal({
            title: 'Você tem certeza que deseja deletar o item selecionado?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0fb2fc',
            cancelButtonColor: '#d41414',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            if (href) {
                window.location.href = href;
            }
        });
    });




    $('.btn-edit-status0').on('click', function (e) {
        e.preventDefault();

        href = $(this).attr('href');

        swal({
            title: 'Todas as postagens dessa categoria serão desativadas, continuar?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0fb2fc',
            cancelButtonColor: '#d41414',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Cancelar'
        }).then(function () {
            if (href) {
                window.location.href = href;
            }
        });
    });


});

function centralizaAltura(Child, Father){
    
    var childHeight = $(Child).height();
    var fatherHeight = $(Father).height();
    
    var ChildMargin = (fatherHeight / 2) - (childHeight / 2);
    $(Child).css('margin-top', ChildMargin);
    
    
    
}

function sucesso(mensagem, urlTo){
    $('.load').hide();
    swal("Sucesso!", mensagem, "success").then(function () {
        location.href= urlTo;
    })
    $('.swal2-confirm').addClass("sucesso");
}

function erro(err){
    $('.load').hide();
    $('.alert-danger').show();
    $('.alert-danger').find('ul').html("");
    $.each(err.responseJSON, function (index, error) {
        $('.alert-danger').find('ul').append("<li>" + error + "</li>");
    });
}

function before(){
    $('.load').hide();
    $('.alert-danger').hide();
    $(".form-ajax").append("<img class='load' src='imagens/25.gif'/>");
}

function erroSistema(err){
    $('.load').hide();
    $('.alert-danger').show();
    $('.alert-danger').find('ul').html("");
    $.each(err, function (index, error) {
        $('.alert-danger').find('ul').append("<li>" + error + "</li>");
    });
}