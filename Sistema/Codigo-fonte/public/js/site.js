$(document).ready(function () {


    centralizaPopup();


    $('.btn-down').click(function(){

        if($(this).hasClass("btn_um")){
            $(".d_um").show();
            $(".d_dois").show();
            $(".d_tres").show();
            $(".d_combo").show();
        }else if($(this).hasClass("btn_dois")){
            $(".d_um").hide();
            $(".d_combo").show();
            $(".d_dois").show();
            $(".d_tres").hide();
        }else if ($(this).hasClass("btn_tres")){
            $(".d_um").hide();
            $(".d_combo").show();
            $(".d_dois").hide();
            $(".d_tres").show();
        }

    })


    $(".campo-personalizacao").append("<div class='load-personalizacao'>Carregando...</div>");

    setTimeout(function () {
        $('.load-personalizacao').remove();
        $('.campo-personalizacao').css("visibility","visible");
    },500);


    $('.fields-consumidor').fadeIn();

    $('.perfis-cadastro p').click(function () {
        $('.perfis-cadastro p').removeClass('active');

        $('.errors').hide();
        $('.errors').find('ul').html('');
        $('.fields-hide').hide();
        $('.fields-' + $(this).attr('class')).fadeIn();
        $(this).addClass('active');
    });


    $(".btn-user-home").hover(
            function () {
                $(this).find('.msg-user').addClass("active");
                $(this).find('.msg-user').next().slideDown();
            },
            function () {

                $(this).find('.msg-user').removeClass("active");
                $(this).find('.msg-user').next().slideUp();

            });
    $('.macaneta-montada').click(function () {

        $(".campo-personalizacao").append("<div class='load-personalizacao'>Carregando...</div>");


        var fechadura = $(this).find('.fechadura-thumb').attr('src');
        var macaneta = $(this).find('.macaneta-thumb').attr('src');
        $('.fechadura').attr('src', fechadura);
        $('.macaneta').attr('src', macaneta);
        var idFechadura = fechadura.replace('personalizacao/fechaduras/', '').replace('.png', '').substring(3);
        idFechadura = idFechadura.substring(0, idFechadura.length - 2);
        $('.door').attr('src', 'personalizacao/portas/m_clara_' + idFechadura + '.png');

        setTimeout(function () {
            $('.load-personalizacao').remove();
        },500);
        return false;

    });

    $('.cep').keyup(function () {
        if ($(this).val().length == 8) {
            var cep = "//viacep.com.br/ws/" + $(this).val() + "/json/?callback=?";
            $.getJSON(cep, function (dados) {
                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $(".endereco").val(dados.logradouro);
                    $(".bairro").val(dados.bairro);
                    $(".cidade").val(dados.localidade);
                    $(".estado").val(dados.uf);
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                }
            });
        }
    });

   $('.form-cadastrar').submit(function () {
        data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('.btnCadastrar').after('<img src="imagens/spin.svg" class="loading"/>');
            },
            error: function (err) {
                console.log(err);
                $('.errors').show();
                $('.errors').find('ul').html('');
                $.each(err.responseJSON, function (index, error) {
                    $('.errors').find('ul').append('<li>' + error + '</li>');
                });
                $('.loading').remove();
            },
            success: function (success) {

                console.log(success);

                $('.errors').hide();
                $('.errors').find('ul').html('');
                $('.errors').show();
                switch (success) {
                    case '1':
                        $('.errors').hide();
                        $('.success').show();
                        $('.success').find('ul').append('<li>Usuário cadastrado com sucesso</li>');
                        
                        setTimeout(function () {
                            $('.form__control').each(function () {
                                $(this).val('');
                                $('.close').click();
                                $('.success').html('');
                                $('.success').hide();
                            });
                            
                            location.reload();
                        }, 1000);
                        break;
                    case '2':
                        $('.errors').find('ul').append('<li>Erro ao cadastrar usuário</li>');
                        break;
                    case '3':
                        $('.errors').find('ul').append('<li>Este e-mail já está cadastrado em nosso sistema</li>');
                        break;
                    default :
                        break;
                }
                $('.loading').remove();
            }
        });
        return false;
    });

    $('.form-logar').submit(function () {
        data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            error: function () {},
            success: function (success) {
                if (success.mensagem == "Usuário não encontrado.") {
                    $('.errors').find('ul').html('');
                    $('.errors').show();
                    $('.errors').find('ul').append('<li>' + success.mensagem + '</li>');
                } else if (success.mensagem == "sucesso") {
                    $('.success').show();
                    $('.success').find('ul').append('<li>Seja bem-vindo</li>');
                    setInterval(function () {
                        $('.success').find('ul').html('');
                        $('.success').hide();
                        location.reload();
                    }, 1500);
                }
            },
            beforeSubmit: function () {}
        });
        return false;
    });

    $('.form-recuperarsenha').submit(function(){
        data = $(this).serialize();

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            error: function (err) {
                console.log(err);
                $('.errors').show();
                $('.errors').find('ul').html('');
                $.each(err.responseJSON, function (index, error) {
                    $('.errors').find('ul').append('<li>' + error + '</li>');
                });
                $('.loading').remove();
            },
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $('.errors').hide();
                $('.errors').find('ul').html('');
                $('.errors').show();
                switch (data) {
                    case '1':
                        $('.errors').hide();
                        $('.success').show();
                        $('.success').find('ul').append('<li>Senha alterada com sucesso</li>');
                        setTimeout(function () {
                            $('.form__control').each(function () {
                                $(this).val('');
                                $('.close').click();
                                $('.success').html('');
                                $('.success').hide();
                            });

                            urlTo = $('.navbar-brand').attr("href");

                            location.href=urlTo;
                        }, 1000);
                        break;
                    case '2':
                        $('.errors').find('ul').append('<li>Erro ao cadastrar usuário</li>');
                        break;
                    case '3':
                        $('.errors').find('ul').append('<li>Este e-mail já está cadastrado em nosso sistema</li>');
                        break;
                    default :
                        break;
                }
            },
            beforeSend: function (xhr) {
                console.log(xhr);
            }
        });

        return false;
    });

    $('.form-editar').submit(function () {
        data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            },
            success: function (data, textStatus, jqXHR) {
                $('.errors').hide();
                $('.errors').find('ul').html('');
                $('.errors').show();
                switch (data) {
                    case '1':
                        $('.errors').hide();
                        $('.success').show();
                        $('.success').find('ul').append('<li>Usuário cadastrado com sucesso</li>');
                        setTimeout(function () {
                            $('.form__control').each(function () {
                                $(this).val('');
                                $('.close').click();
                                $('.success').html('');
                                $('.success').hide();
                            });
                            location.reload();
                        }, 1000);
                        break;
                    case '2':
                        $('.errors').find('ul').append('<li>Erro ao cadastrar usuário</li>');
                        break;
                    case '3':
                        $('.errors').find('ul').append('<li>Este e-mail já está cadastrado em nosso sistema</li>');
                        break;
                    default :
                        break;
                }
            },
            beforeSend: function (xhr) {
                console.log(xhr);
            }
        });
        return false;
    });
    
    $(".form-recuperar").submit(function () {
        data = $(this).serialize();

        $.ajax({
            url: $(this).attr('action'),
            data: data,
            type: 'POST',
            error: function (err) {
                $('.errors').show();
                $('.errors').find('ul').html('');
                $('.errors').find('ul').append('<li>E-mail não encontrado</li>');
                $.each(err.responseJSON, function (index, error) {
                    $('.errors').find('ul').append('<li>' + error + '</li>');
                });
            },
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                if(data == 1){

                    $('.errors').hide();
                    $('.success').show();
                    $('.success').find('ul').append('<li>Foi enviado um e-mail com o link de redefinição.</li>');
                    setTimeout(function () {
                        $('.form__control').each(function () {

                            $('.close').click();
                        });
                        location.reload();
                    }, 1000);

                }else if(data == 2){

                    $('.errors').find('ul').append('<li>E-mail não encontrado.</li>');
                }
            },
            beforeSend: function (xhr) {
                console.log(xhr);
            }
        });
        return false;

    });

    /*$('.form-senha').submit(function () {
        data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            data: data,
            type: 'POST',
            error: function (err) {
                $('.errors').show();
                $('.errors').find('ul').html('');
                $.each(err.responseJSON, function (index, error) {
                    $('.errors').find('ul').append('<li>' + error + '</li>');
                });
            },
            success: function (data, textStatus, jqXHR) {
                if (data == "n") {
                    $('.errors').find('ul').html('');
                    $('.errors').show();
                    $('.errors').find('ul').append('<li>A senha que você digitou está incorreta.</li>');
                } else if (data == "sucesso") {
                    $('.success').show();
                    $('.success').find('ul').append('<li>Seja bem vindo</li>');
                    setInterval(function () {
                        $('.success').find('ul').html('');
                        $('.success').hide();
                        location.reload();
                    }, 1500);
                }
            },
            beforeSend: function (xhr) {
                console.log(xhr);
            }
        });
        return false;
    });*/

    $('.active-user').click(function () {
        return false;
    });
});

function centralizaPopup(){
    var largura = $(".modal-status").width();
    var altura = $('.modal-status').height();

    var winWidth = $(window).width();
    var winHeight = $(window).height();

    $(".modal-status").css({
        "margin-top" : (winHeight / 2) - (altura / 2),
        "margin-left" : (winWidth / 2) - (largura / 2)
    });

}