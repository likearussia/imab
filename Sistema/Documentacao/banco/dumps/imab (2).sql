-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Tempo de geração: 12/09/2016 às 20:17
-- Versão do servidor: 10.1.13-MariaDB
-- Versão do PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `imab`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `caminho` varchar(200) NOT NULL,
  `link` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `nova_aba` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `caminho`, `link`, `titulo`, `nova_aba`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, '97b2dc3e44b835fcd38395ac4cb60d301508f876.jpeg', '', 'Página inicial 01', 0, '2016-08-18 12:40:41', '2016-08-18 17:14:43', NULL, 1),
(2, '014b6c452b5516ce019e8fbd3ef7311fcf13a7d0.jpeg', '', 'TESTE KEYLA', 1, '2016-08-18 13:28:32', '2016-08-19 21:03:00', NULL, 0),
(4, 'c611ea0c26e6edcfe62530422efc7f5b5c2ec4cb.png', 'aaaa', 'Banner teste', 1, '2016-08-24 19:28:35', '2016-08-24 20:44:27', NULL, 0),
(5, '0521d88f74ebd8b021406dd2ac13d3cc3803f31d.png', 'aaaa', 'ACESSÓRIOS PARA PORTA DA IMAB', 1, '2016-08-24 19:37:11', '2016-08-24 20:44:28', NULL, 0),
(7, 'a838ff1f1fc210239e6af7bff934cde06539ad27.jpeg', 'aaa', 'Banner teste', 1, '2016-08-24 19:55:05', '2016-08-24 20:44:29', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `categoria_dicas`
--

CREATE TABLE `categoria_dicas` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categoria_dicas`
--

INSERT INTO `categoria_dicas` (`id`, `nome`, `slug`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ferrugem', 'ferrugem', 1, '2016-08-18 13:52:50', '2016-08-18 13:52:50', NULL),
(2, 'OXIDAÇÃO ', 'oxidacao', 1, '2016-08-18 14:43:55', '2016-08-18 14:43:55', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `dicas`
--

CREATE TABLE `dicas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `conteudo` text NOT NULL,
  `imagem_destaque` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `categoria` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `dicas`
--

INSERT INTO `dicas` (`id`, `titulo`, `conteudo`, `imagem_destaque`, `slug`, `categoria`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'TESTE KEYLA', '<p class="MsoNormal">Lorem ipsum dolor sit amet,\r\nconsectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere,\r\nmagna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo\r\nmagna eros quis urna. Nunc viverra imperdiet enim. Fusce est.<o:p></o:p></p><p class="MsoNormal"><i>Vivamus a tellus.\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac\r\nturpis egestas. </i><span lang="EN-US"><i>Proin pharetra nonummy pede. Mauris et orci. Aenean nec\r\nlorem.</i><o:p></o:p></span></p><p class="MsoNormal"><b><span lang="EN-US">In porttitor. Donec laoreet nonummy augue. </span>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis,\r\nnunc. </b><span lang="EN-US"><b>Mauris\r\neget neque at sem venenatis eleifend. Ut nonummy.</b><o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n</p><p class="MsoNormal"><u><span lang="EN-US">Fusce aliquet pede non pede. </span>Suspendisse\r\ndapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula.\r\nDonec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in\r\nlacinia nulla nisl eget sapien.</u></p><p class="MsoNormal"><strike>Lorem ipsum dolor sit amet,\r\nconsectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere,\r\nmagna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo\r\nmagna eros quis urna. Nunc viverra imperdiet enim. Fusce est.</strike><o:p></o:p></p><p class="MsoNormal"><span style="font-size: 18px;">Vivamus a tellus.\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac\r\nturpis egestas. </span><span lang="EN-US"><span style="font-size: 18px;">Proin pharetra nonummy pede. Mauris et orci. Aenean nec\r\nlorem.</span><o:p></o:p></span></p><p class="MsoNormal"><sub><span lang="EN-US">In porttitor. Donec laoreet nonummy augue. </span>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis,\r\nnunc. <span lang="EN-US">Mauris\r\neget neque at sem venenatis eleifend. Ut nonummy.<o:p></o:p></span></sub></p><p class="MsoNormal">\r\n\r\n\r\n\r\n\r\n\r\n</p><p class="MsoNormal"><sub><span lang="EN-US">Fusce aliquet pede non pede. </span>Suspendisse\r\ndapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula.\r\nDonec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in\r\nlacinia nulla nisl eget sapien.</sub></p><p class="MsoNormal" style="line-height: 1.6;"><span style="background-color: rgb(255, 255, 0);">Lorem ipsum dolor sit amet,\r\nconsectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere,\r\nmagna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo\r\nmagna eros quis urna. Nunc viverra imperdiet enim. Fusce est.</span></p><p class="MsoNormal" style="line-height: 1.6;"><iframe frameborder="0" src="//www.youtube.com/embed/tntOCGkgt98" width="640" height="360" class="note-video-clip"></iframe><span style="background-color: rgb(255, 255, 0);"><br></span><o:p></o:p></p><p class="MsoNormal"><span style="background-color: rgb(156, 198, 239);">Vivamus a tellus.\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac\r\nturpis egestas. </span><span lang="EN-US"><span style="background-color: rgb(156, 198, 239);">Proin pharetra nonummy pede. Mauris et orci. Aenean nec\r\nlorem.</span><o:p></o:p></span></p><ol><li><span lang="EN-US">In porttitor. Donec laoreet nonummy augue. </span>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis,\r\nnunc. <span lang="EN-US">Mauris\r\neget neque at sem venenatis eleifend. Ut nonummy.<o:p></o:p></span></li><li>\r\n\r\n\r\n\r\n\r\n\r\n</li><li><span lang="EN-US">Fusce aliquet pede non pede. </span>Suspendisse\r\ndapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula.\r\nDonec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in\r\nlacinia nulla nisl eget sapien.</li><li style="text-align: justify; ">Lorem ipsum dolor sit amet,\r\nconsectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere,\r\nmagna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo\r\nmagna eros quis urna. Nunc viverra imperdiet enim. Fusce est.<o:p></o:p></li><li style="text-align: justify;">Vivamus a tellus.\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac\r\nturpis egestas. <span lang="EN-US">Proin pharetra nonummy pede. Mauris et orci. Aenean nec\r\nlorem.<o:p></o:p></span></li><li style="text-align: justify;"><span lang="EN-US">In porttitor. Donec laoreet nonummy augue. </span>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis,\r\nnunc. <span lang="EN-US">Mauris\r\neget neque at sem venenatis eleifend. Ut nonummy.<o:p></o:p></span></li><li style="text-align: justify;">\r\n\r\n\r\n\r\n\r\n\r\n</li><li style="text-align: justify;"><span lang="EN-US">Fusce aliquet pede non pede. </span>Suspendisse\r\ndapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula.\r\nDonec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in\r\nlacinia nulla nisl eget sapien.</li></ol>', '313337a804c90fb76503ff5b511778589f00478a.jpeg', 'teste-keyla', 1, 1, '2016-08-18 14:32:32', '2016-08-18 14:34:47', NULL),
(2, 'TESTE CARLA', '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu rutrum erat. Fusce viverra finibus odio ac malesuada. Ut tristique purus id turpis malesuada, ut rhoncus augue imperdiet. Nunc eget purus sed massa pharetra congue. Aliquam felis ipsum, pellentesque in nibh ut, malesuada tempor lacus. Vivamus rhoncus euismod aliquam. Sed semper risus nec ullamcorper aliquam. Phasellus id nibh faucibus, feugiat est a, accumsan nisi.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Donec libero arcu, sollicitudin sed scelerisque placerat, molestie eget nibh. Nam dolor augue, tristique ac mi aliquet, blandit ullamcorper nulla. Fusce ut sodales mauris. Aenean sit amet accumsan nunc. Praesent fringilla massa sit amet neque vestibulum facilisis. Vestibulum laoreet sit amet quam a cursus. In tempor tempor purus nec hendrerit. Donec eleifend, elit sit amet congue faucibus, elit nunc iaculis eros, eget pretium dolor erat sed leo. Nunc aliquam eget leo sed feugiat. Nulla luctus nulla sed ligula accumsan, quis ultrices sapien bibendum.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Curabitur elementum augue sit amet lectus maximus tincidunt. Vivamus suscipit interdum nisl, eget suscipit massa congue eget. Donec viverra malesuada eros, a suscipit diam commodo vitae. Aenean non metus id elit lobortis auctor. In ut elit in tortor semper interdum. Sed non nibh eu nisl maximus imperdiet id a felis. Sed a vulputate nisi. Donec odio lacus, vestibulum id dapibus sed, suscipit ac libero. Aenean sed velit leo. Vivamus purus erat, scelerisque eget scelerisque a, dictum non tortor. Fusce ornare tortor eleifend semper pretium. Phasellus elementum sem non neque lobortis efficitur. Integer aliquet eros sit amet lorem egestas pretium. Nullam commodo dapibus quam. Quisque libero ligula, consequat sit amet risus vel, mattis lobortis est.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Donec id volutpat augue. Donec ullamcorper quam rutrum, semper ante aliquet, vestibulum nibh. Nullam quis dui et velit porta tincidunt. Suspendisse pulvinar ex in purus iaculis pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. In convallis cursus massa, sit amet porttitor justo tincidunt vitae. Nulla eu magna elit. Praesent ac felis in arcu fringilla ullamcorper non vel justo. Nulla facilisi. Morbi cursus hendrerit aliquet. Cras ornare quam dolor, in accumsan mi tincidunt a. Nunc ut sapien eget neque tincidunt convallis sed sit amet odio.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Duis tincidunt feugiat mauris vitae hendrerit. Fusce laoreet, dui eget auctor pretium, orci libero ullamcorper tellus, sit amet mattis lorem urna eu eros. Pellentesque eu vestibulum dui. Curabitur eleifend, est sit amet eleifend luctus, elit ipsum mattis tortor, eget pellentesque risus lacus dapibus sem. Nunc facilisis orci id risus consectetur, nec viverra dolor rutrum. Etiam accumsan eget enim et iaculis. Curabitur gravida ante at sollicitudin posuere.</p>', '6da5849a145aff18d80b4f0b9f0133c214df4404.jpeg', 'teste-carla', 2, 1, '2016-08-18 14:45:27', '2016-08-18 14:45:31', NULL),
(3, 'TESTE 02', '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Vivamus porta, enim id imperdiet feugiat, purus magna porttitor dolor, eu convallis risus nibh sit amet purus. Pellentesque ornare ipsum at erat placerat tristique. Donec eu ante sit amet risus ultrices rhoncus. Nunc sed mi et est hendrerit hendrerit. Aliquam erat volutpat. Praesent at bibendum felis. Pellentesque placerat venenatis lectus, maximus molestie nunc pulvinar ut. Proin ligula nisl, varius vitae dignissim sit amet, sodales et tellus. Ut feugiat pretium semper. Aliquam viverra tellus sit amet turpis placerat, a bibendum lorem ultrices. Aliquam porta libero lectus, id porttitor mi condimentum at. Proin eu placerat purus. Aliquam erat volutpat. Vestibulum augue ex, rutrum non leo vitae, varius sodales ligula. Maecenas orci eros, pulvinar at arcu sit amet, facilisis dapibus turpis. Fusce commodo rhoncus finibus.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Vivamus hendrerit sollicitudin orci, sed bibendum arcu dignissim eget. Etiam sed commodo ex. Morbi laoreet pellentesque cursus. Nam ante lectus, fermentum eget auctor at, rutrum vitae urna. Curabitur aliquam maximus iaculis. Praesent posuere sit amet orci in pulvinar. Maecenas a odio dapibus, bibendum tellus vel, posuere tellus. Mauris pulvinar urna commodo lobortis cursus. Duis ultrices nec eros sit amet commodo. Nunc sit amet lobortis dui. Proin sit amet egestas mauris, ac lobortis libero. Vestibulum id ex vel augue pharetra facilisis. Vestibulum dapibus ante in ante porta, eu placerat urna commodo. Praesent dignissim efficitur nisi eget imperdiet. Quisque volutpat fringilla erat vitae tempus. Nulla facilisi.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Integer mattis nunc sed augue auctor, nec vulputate velit viverra. Nam dignissim elementum velit, non efficitur est malesuada at. In hac habitasse platea dictumst. Vivamus sodales rhoncus urna nec condimentum. Proin ipsum erat, consequat eu augue eu, porttitor scelerisque magna. Nulla lorem mauris, eleifend a nibh ac, placerat sollicitudin elit. Mauris sodales dui non enim volutpat, ut auctor sapien sodales. Curabitur elementum ullamcorper neque rhoncus aliquet. Donec tellus sem, laoreet sit amet semper at, euismod sit amet turpis. Sed egestas dapibus suscipit. Curabitur facilisis vitae ligula at eleifend. Phasellus eget nibh nec elit euismod tincidunt. Vivamus quis posuere elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin elementum at lorem sit amet commodo.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Mauris odio erat, iaculis sed congue vitae, porttitor at massa. Etiam fermentum pretium nunc sit amet fringilla. Fusce in tellus tempus, ultrices nunc a, efficitur dolor. Praesent at lorem augue. Ut sed luctus mauris, sed sagittis dolor. Phasellus sollicitudin lorem et quam consectetur, ac imperdiet est lacinia. Integer vel orci et ante faucibus porta quis id dolor. Aenean commodo nunc at est sollicitudin, molestie fermentum diam pharetra. Maecenas efficitur augue quis purus gravida accumsan congue quis augue. Etiam ipsum augue, tincidunt eu ex vel, accumsan porta justo. Etiam gravida pretium metus quis blandit. Curabitur a condimentum erat. Vestibulum non ante id turpis feugiat tincidunt quis et ipsum. Praesent nec justo at ligula finibus gravida sit amet quis orci. Phasellus efficitur justo eu eleifend faucibus. Donec rhoncus, lectus eleifend ullamcorper laoreet, tellus magna molestie purus, at cursus tellus nibh sed arcu.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Pellentesque maximus euismod massa ac porttitor. Nulla sed ipsum sagittis, elementum ipsum imperdiet, tristique massa. Nullam vel leo quis quam vehicula fringilla non sit amet elit. Suspendisse aliquam venenatis quam at dignissim. Nam nisi sem, pellentesque in ullamcorper sed, porta eu est. Proin lacinia aliquam ullamcorper. Proin porta, quam a ultricies vehicula, erat nibh egestas dolor, nec ultrices odio sapien in lacus. Aliquam nec suscipit justo. In hac habitasse platea dictumst. Curabitur ac libero quam. Praesent tincidunt dictum aliquam.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Vivamus porta, enim id imperdiet feugiat, purus magna porttitor dolor, eu convallis risus nibh sit amet purus. Pellentesque ornare ipsum at erat placerat tristique. Donec eu ante sit amet risus ultrices rhoncus. Nunc sed mi et est hendrerit hendrerit. Aliquam erat volutpat. Praesent at bibendum felis. Pellentesque placerat venenatis lectus, maximus molestie nunc pulvinar ut. Proin ligula nisl, varius vitae dignissim sit amet, sodales et tellus. Ut feugiat pretium semper. Aliquam viverra tellus sit amet turpis placerat, a bibendum lorem ultrices. Aliquam porta libero lectus, id porttitor mi condimentum at. Proin eu placerat purus. Aliquam erat volutpat. Vestibulum augue ex, rutrum non leo vitae, varius sodales ligula. Maecenas orci eros, pulvinar at arcu sit amet, facilisis dapibus turpis. Fusce commodo rhoncus finibus.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Vivamus hendrerit sollicitudin orci, sed bibendum arcu dignissim eget. Etiam sed commodo ex. Morbi laoreet pellentesque cursus. Nam ante lectus, fermentum eget auctor at, rutrum vitae urna. Curabitur aliquam maximus iaculis. Praesent posuere sit amet orci in pulvinar. Maecenas a odio dapibus, bibendum tellus vel, posuere tellus. Mauris pulvinar urna commodo lobortis cursus. Duis ultrices nec eros sit amet commodo. Nunc sit amet lobortis dui. Proin sit amet egestas mauris, ac lobortis libero. Vestibulum id ex vel augue pharetra facilisis. Vestibulum dapibus ante in ante porta, eu placerat urna commodo. Praesent dignissim efficitur nisi eget imperdiet. Quisque volutpat fringilla erat vitae tempus. Nulla facilisi.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Integer mattis nunc sed augue auctor, nec vulputate velit viverra. Nam dignissim elementum velit, non efficitur est malesuada at. In hac habitasse platea dictumst. Vivamus sodales rhoncus urna nec condimentum. Proin ipsum erat, consequat eu augue eu, porttitor scelerisque magna. Nulla lorem mauris, eleifend a nibh ac, placerat sollicitudin elit. Mauris sodales dui non enim volutpat, ut auctor sapien sodales. Curabitur elementum ullamcorper neque rhoncus aliquet. Donec tellus sem, laoreet sit amet semper at, euismod sit amet turpis. Sed egestas dapibus suscipit. Curabitur facilisis vitae ligula at eleifend. Phasellus eget nibh nec elit euismod tincidunt. Vivamus quis posuere elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin elementum at lorem sit amet commodo.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Mauris odio erat, iaculis sed congue vitae, porttitor at massa. Etiam fermentum pretium nunc sit amet fringilla. Fusce in tellus tempus, ultrices nunc a, efficitur dolor. Praesent at lorem augue. Ut sed luctus mauris, sed sagittis dolor. Phasellus sollicitudin lorem et quam consectetur, ac imperdiet est lacinia. Integer vel orci et ante faucibus porta quis id dolor. Aenean commodo nunc at est sollicitudin, molestie fermentum diam pharetra. Maecenas efficitur augue quis purus gravida accumsan congue quis augue. Etiam ipsum augue, tincidunt eu ex vel, accumsan porta justo. Etiam gravida pretium metus quis blandit. Curabitur a condimentum erat. Vestibulum non ante id turpis feugiat tincidunt quis et ipsum. Praesent nec justo at ligula finibus gravida sit amet quis orci. Phasellus efficitur justo eu eleifend faucibus. Donec rhoncus, lectus eleifend ullamcorper laoreet, tellus magna molestie purus, at cursus tellus nibh sed arcu.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Pellentesque maximus euismod massa ac porttitor. Nulla sed ipsum sagittis, elementum ipsum imperdiet, tristique massa. Nullam vel leo quis quam vehicula fringilla non sit amet elit. Suspendisse aliquam venenatis quam at dignissim. Nam nisi sem, pellentesque in ullamcorper sed, porta eu est. Proin lacinia aliquam ullamcorper. Proin porta, quam a ultricies vehicula, erat nibh egestas dolor, nec ultrices odio sapien in lacus. Aliquam nec suscipit justo. In hac habitasse platea dictumst. Curabitur ac libero quam. Praesent tincidunt dictum aliquam.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Vivamus porta, enim id imperdiet feugiat, purus magna porttitor dolor, eu convallis risus nibh sit amet purus. Pellentesque ornare ipsum at erat placerat tristique. Donec eu ante sit amet risus ultrices rhoncus. Nunc sed mi et est hendrerit hendrerit. Aliquam erat volutpat. Praesent at bibendum felis. Pellentesque placerat venenatis lectus, maximus molestie nunc pulvinar ut. Proin ligula nisl, varius vitae dignissim sit amet, sodales et tellus. Ut feugiat pretium semper. Aliquam viverra tellus sit amet turpis placerat, a bibendum lorem ultrices. Aliquam porta libero lectus, id porttitor mi condimentum at. Proin eu placerat purus. Aliquam erat volutpat. Vestibulum augue ex, rutrum non leo vitae, varius sodales ligula. Maecenas orci eros, pulvinar at arcu sit amet, facilisis dapibus turpis. Fusce commodo rhoncus finibus.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Vivamus hendrerit sollicitudin orci, sed bibendum arcu dignissim eget. Etiam sed commodo ex. Morbi laoreet pellentesque cursus. Nam ante lectus, fermentum eget auctor at, rutrum vitae urna. Curabitur aliquam maximus iaculis. Praesent posuere sit amet orci in pulvinar. Maecenas a odio dapibus, bibendum tellus vel, posuere tellus. Mauris pulvinar urna commodo lobortis cursus. Duis ultrices nec eros sit amet commodo. Nunc sit amet lobortis dui. Proin sit amet egestas mauris, ac lobortis libero. Vestibulum id ex vel augue pharetra facilisis. Vestibulum dapibus ante in ante porta, eu placerat urna commodo. Praesent dignissim efficitur nisi eget imperdiet. Quisque volutpat fringilla erat vitae tempus. Nulla facilisi.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Integer mattis nunc sed augue auctor, nec vulputate velit viverra. Nam dignissim elementum velit, non efficitur est malesuada at. In hac habitasse platea dictumst. Vivamus sodales rhoncus urna nec condimentum. Proin ipsum erat, consequat eu augue eu, porttitor scelerisque magna. Nulla lorem mauris, eleifend a nibh ac, placerat sollicitudin elit. Mauris sodales dui non enim volutpat, ut auctor sapien sodales. Curabitur elementum ullamcorper neque rhoncus aliquet. Donec tellus sem, laoreet sit amet semper at, euismod sit amet turpis. Sed egestas dapibus suscipit. Curabitur facilisis vitae ligula at eleifend. Phasellus eget nibh nec elit euismod tincidunt. Vivamus quis posuere elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin elementum at lorem sit amet commodo.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Mauris odio erat, iaculis sed congue vitae, porttitor at massa. Etiam fermentum pretium nunc sit amet fringilla. Fusce in tellus tempus, ultrices nunc a, efficitur dolor. Praesent at lorem augue. Ut sed luctus mauris, sed sagittis dolor. Phasellus sollicitudin lorem et quam consectetur, ac imperdiet est lacinia. Integer vel orci et ante faucibus porta quis id dolor. Aenean commodo nunc at est sollicitudin, molestie fermentum diam pharetra. Maecenas efficitur augue quis purus gravida accumsan congue quis augue. Etiam ipsum augue, tincidunt eu ex vel, accumsan porta justo. Etiam gravida pretium metus quis blandit. Curabitur a condimentum erat. Vestibulum non ante id turpis feugiat tincidunt quis et ipsum. Praesent nec justo at ligula finibus gravida sit amet quis orci. Phasellus efficitur justo eu eleifend faucibus. Donec rhoncus, lectus eleifend ullamcorper laoreet, tellus magna molestie purus, at cursus tellus nibh sed arcu.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;">Pellentesque maximus euismod massa ac porttitor. Nulla sed ipsum sagittis, elementum ipsum imperdiet, tristique massa. Nullam vel leo quis quam vehicula fringilla non sit amet elit. Suspendisse aliquam venenatis quam at dignissim. Nam nisi sem, pellentesque in ullamcorper sed, porta eu est. Proin lacinia aliquam ullamcorper. Proin porta, quam a ultricies vehicula, erat nibh egestas dolor, nec ultrices odio sapien in lacus. Aliquam nec suscipit justo. In hac habitasse platea dictumst. Curabitur ac libero quam. Praesent tincidunt dictum aliquam.</p><p style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; margin-bottom: 15px; padding: 0px; text-align: justify;"><br></p>', 'bdcf1df5ab650193d1c86738bec3f75ec9ea08a4.jpeg', 'teste-02', 1, 1, '2016-08-18 14:46:41', '2016-08-18 14:46:48', NULL),
(4, 'TESTE 03', '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Vestibulum tempor, dui non rhoncus rhoncus, odio diam eleifend augue, eget vestibulum augue nunc eget purus. Nunc facilisis nisi vitae tortor dignissim, id porta ex mattis. Duis a tempor urna. Integer eget semper libero, ut efficitur dolor. Nam et enim eget lacus rutrum accumsan. Pellentesque ultricies risus velit, ac pulvinar orci rhoncus eu. Pellentesque rhoncus dolor ac sollicitudin porta. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed iaculis, diam a mollis rutrum, sapien erat tristique leo, eget fermentum ipsum dolor at justo. Nulla facilisi.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Vestibulum a libero nulla. Curabitur ac viverra velit, sed tempus massa. Fusce in lacus sed urna eleifend suscipit id sed metus. In dignissim, magna nec feugiat interdum, tellus magna auctor dui, mollis posuere mauris turpis id lacus. In eleifend vitae massa scelerisque tempus. Nulla non facilisis justo. Donec eget lorem eget leo accumsan tincidunt non a purus. Curabitur accumsan pretium pharetra. Praesent eget quam vitae sem lacinia sollicitudin lacinia ut odio. Maecenas quis magna lectus. Suspendisse suscipit mi id urna finibus, non bibendum turpis lacinia. Aenean augue arcu, placerat non suscipit quis, facilisis quis felis. Nullam laoreet eu lorem et vestibulum.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Praesent sit amet faucibus massa. Mauris pulvinar ipsum elit, laoreet sodales sapien consectetur sit amet. Fusce a mauris in risus luctus convallis. Sed eleifend nulla eleifend, pretium ligula ut, convallis leo. Vestibulum nec justo vel ligula lacinia pellentesque id maximus sapien. Etiam porttitor enim pharetra, hendrerit ex molestie, porta tortor. Aenean at turpis efficitur, blandit elit ut, vehicula nunc. Mauris scelerisque lacinia nibh, quis vestibulum ligula tincidunt molestie. Donec id mi consectetur neque sollicitudin tempor a ut felis. Vestibulum posuere tellus mauris, nec pellentesque purus facilisis vitae.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Fusce aliquam congue ligula. Sed et nunc sit amet risus venenatis ornare at non tortor. Nulla tristique finibus tortor, in fringilla tellus dignissim non. Donec metus lacus, cursus et luctus sit amet, viverra vel magna. Mauris erat mi, laoreet at eros a, placerat euismod quam. Pellentesque molestie, velit vel consectetur mollis, nibh nisi viverra urna, non gravida ante nisi vitae eros. Suspendisse vel nisl a erat semper faucibus. Sed eu nisi cursus, aliquam lorem eget, vulputate urna. Quisque eget iaculis dolor. Etiam iaculis aliquam nisl, eget pulvinar mauris aliquam at. Suspendisse et mi iaculis, hendrerit orci ac, faucibus lacus. Nam egestas sit amet dolor a laoreet. Maecenas hendrerit, urna quis euismod porttitor, sem ipsum porttitor sem, quis aliquam arcu velit et nulla. Cras maximus at ipsum sed maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Suspendisse potenti. Donec ut ex vel dolor mattis elementum a quis orci. Pellentesque pulvinar enim non erat tristique, nec tristique est sodales. Nam at pretium nibh. Integer dignissim dictum placerat. Sed ac diam vestibulum, malesuada erat lacinia, pharetra nunc. Ut aliquet sagittis orci eget tincidunt. Sed consectetur neque nec libero dapibus eleifend.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Quisque rutrum placerat tellus, quis imperdiet odio porttitor semper. Pellentesque maximus nunc nec mauris suscipit pulvinar. Aenean in nibh quis metus sodales faucibus. Nullam porttitor malesuada nibh, vel dictum dui tristique malesuada. In et suscipit turpis, a eleifend enim. Quisque vel tincidunt lectus. Ut tempor nibh tortor, sed venenatis sapien facilisis eu. Nunc aliquam interdum nunc eget mollis. Mauris vulputate fringilla congue. Cras dictum accumsan ligula posuere iaculis.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Aenean feugiat ex et urna dapibus, commodo dapibus ipsum vestibulum. Donec vulputate porttitor feugiat. Praesent interdum commodo sapien id pellentesque. Nulla velit eros, lobortis vitae tempus a, fringilla in lacus. Integer lacinia nec tellus eget</p>', '2474085f595c38bcee581d4be6f891b5ff43cf33.jpeg', 'teste-03', 1, 1, '2016-08-18 14:48:27', '2016-08-18 14:48:32', NULL),
(5, 'TESTE 04', '<div id="lipsum" style="margin: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;"><p style="margin-bottom: 15px; padding: 0px;">Mauris vulputate fringilla congue. Cras dictum accumsan ligula posuere iaculis.</p><p style="margin-bottom: 15px; padding: 0px;">Aenean feugiat ex et urna dapibus, commodo dapibus ipsum vestibulum. Donec vulputate porttitor feugiat. Praesent interdum commodo sapien id pellentesque. Nulla velit eros, lobortis vitae tempus a, fringilla in lacus. Integer lacinia nec tellus eget consequat. Ut non leo turpis. Donec tincidunt non velit a porttitor. Vestibulum a tempor mi. Etiam nec sodales libero. Pellentesque pulvinar tempus ligula, eget venenatis lacus iaculis at. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed accumsan nulla ac ante laoreet, ac luctus nisl consectetur. Nam velit tellus, malesuada ac turpis in, varius blandit dolor. Suspendisse at pretium mauris. Suspendisse lobortis sapien ut leo vulputate, in consectetur risus fermentum.</p><p style="margin-bottom: 15px; padding: 0px;">Ut commodo diam in tellus bibendum tempor. Suspendisse at bibendum sapien. Nullam felis sapien, facilisis at malesuada at, facilisis sit amet risus. Sed sollicitudin purus felis, ut finibus quam venenatis non. Donec mattis quis ex vitae convallis. Sed sed sollicitudin lectus. Nunc sem leo, fringilla posuere iaculis ac, volutpat vitae purus. Mauris nisl metus, commodo eget malesuada et, mollis ut elit. Vivamus eu elit rutrum, vehicula leo eu, molestie est. Praesent dictum ligula odio, ut hendrerit quam lacinia id. Suspendisse fermentum metus non sapien interdum, ac faucibus eros gravida. Etiam faucibus vel nisi id accumsan. Nam eu tincidunt nibh. Donec varius enim nec sapien mattis, ut dignissim arcu eleifend. Pellentesque et mattis arcu, ut laoreet augue.</p><p style="margin-bottom: 15px; padding: 0px;">Sed efficitur purus at tortor feugiat feugiat. Nullam eleifend elit vitae nulla pellentesque finibus. Duis ac tellus fermentum, vestibulum est eget, accumsan velit. Integer velit urna, aliquam eu metus ut, elementum mattis nisi. Duis urna massa, efficitur id ligula nec, imperdiet condimentum erat. Quisque hendrerit velit a volutpat vehicula. Maecenas sed tincidunt ipsum. Fusce lacinia, diam vestibulum scelerisque porta, nisi erat convallis nisi, quis cursus urna velit eget libero. Sed molestie vehicula nisl vel dictum. Fusce mattis volutpat turpis, sed viverra dui fermentum non. Donec quam tortor, feugiat a leo non, tristique tempus justo. In eu odio egestas, luctus quam vel, cursus urna. Morbi at purus iaculis, mattis ipsum sed, ullamcorper eros.</p><p style="margin-bottom: 15px; padding: 0px;">Curabitur dapibus, metus non interdum efficitur, neque nibh porta diam, quis sollicitudin odio tellus semper est. Vivamus eu leo neque. Maecenas porta pulvinar euismod. Vivamus sagittis tellus justo. Suspendisse porttitor lacus nisl, congue consequat nisl maximus sed. Cras sed molestie diam, id sagittis tellus. Fusce hendrerit nunc sed ipsum imperdiet tempor. Morbi molestie lectus vitae faucibus vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque malesuada congue augue, nec elementum risus vehicula id. Nulla dolor arcu, ultricies ut urna vitae, interdum tempus velit. Donec pulvinar scelerisque nisl, vitae ultrices sapien semper nec.</p></div><div id="generated" style="margin: 0px; padding: 0px; font-weight: 700; font-family: &quot;Open Sans&quot;, Arial, sans-serif;"><br></div>', 'dbfd72046469a0e704e58f51889e32adbe919529.jpeg', 'teste-04', 1, 1, '2016-08-18 14:50:23', '2016-08-18 14:50:26', NULL),
(6, 'TESTE 05', '<p><span style="background-color: rgb(255, 255, 0);">asasASAsaSAsaSAs<span style="line-height: 1.42857;">asasAS AsaSAsaSAs</span><span style="line-height: 1.42857;">asasASAsaSAs aSAs</span><span style="line-height: 1.42857;">asasA SAsaSAsaS As</span><span style="line-height: 1.42857;">asas ASAsaSAsaS As</span><span style="line-height: 1.42857;">asasASA saSAsaSAs</span><span style="line-height: 1.42857;">asa sASAsaS AsaSAs</span></span><br></p>', 'c1540c2fdb82c59b3face92ffb0786e083b1222b.jpeg', 'teste-05', 1, 1, '2016-08-18 14:54:10', '2016-08-18 18:32:45', NULL),
(7, 'TESTE TSTET STETTS TETST ETTSTE', '<p class="MsoNormal" style="line-height: 1;">Lorem ipsum dolor sit amet,\r\nconsectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere,\r\nmagna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo\r\nmagna eros quis urna. Nunc viverra imperdiet enim. Fusce est.<o:p></o:p></p><p class="MsoNormal" style="line-height: 1;">Vivamus a tellus.\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac\r\nturpis egestas. <span lang="EN-US">Proin pharetra nonummy pede. Mauris et orci. Aenean nec\r\nlorem.<o:p></o:p></span></p><p class="MsoNormal" style="line-height: 1;"><span lang="EN-US">In porttitor. Donec laoreet nonummy augue. </span>Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis,\r\nnunc. <span lang="EN-US">Mauris\r\neget neque at sem venenatis eleifend. Ut nonummy.<o:p></o:p></span></p><p style="line-height: 1;">\r\n\r\n\r\n\r\n\r\n\r\n</p><p class="MsoNormal" style="line-height: 1;"><span lang="EN-US">Fusce aliquet pede non pede. </span>Suspendisse\r\ndapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula.\r\nDonec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in\r\nlacinia nulla nisl eget sapien.</p>', '13af28f4dc44614a276641deb5227bde2ba0f82f.jpeg', 'teste-tstet-stetts-tetst-ettste', 1, 1, '2016-08-18 16:31:48', '2016-08-18 18:32:13', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `institucionais`
--

CREATE TABLE `institucionais` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `imagem_destaque` varchar(255) NOT NULL,
  `texto` longtext NOT NULL,
  `slug` varchar(250) NOT NULL,
  `timeline` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `institucionais`
--

INSERT INTO `institucionais` (`id`, `titulo`, `subtitulo`, `imagem_destaque`, `texto`, `slug`, `timeline`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'VALORES HUMANOS', '“Quando uma equipe de trabalho está motivada, a excelência na produção torna-se inerente ao produto.”  Presidente IMAB', 'e0f1ac36e708a8f40de1407a9eb6d06ece81cfba.png', '<p>Incentivar o crescimento profissional de seus colaboradores é uma prioridade para a IMAB. Através de treinamentos e cursos de capacitações técnicas, a IMAB investe no aperfeiçoamento de sua mão de obra, fomentando sua produção sem comprometer o padrão de qualidade exigido. </p><p>O reconhecimento das pessoas e seus valores humanos também está presente no dia a dia da IMAB. Constantes programas de melhorias no ambiente de trabalho e investimentos estruturais garantem aos colaboradores maior satisfação em fazer parte da equipe IMAB.</p>', 'valores-humanos', NULL, '2016-08-18 10:48:03', '2016-08-22 21:17:10', NULL),
(2, 'INOVAÇÃO E QUALIDADE', 'A IMAB se destaca pelo constante lançamento de produtos atualizados em design, funcionalidade e preço.', 'c56a8502be8b5b72c6acaafda10183181c4f6c5e.png', '<p>A IMAB busca evolução tecnológica em um parque industrial 100% automatizado. Seus departamentos de projeto, engenharia, ferramentaria e qualidade, trabalham de forma integrada para garantir maior precisão e qualidade ao seu processo produtivo.</p><p>O desenvolvimento de um produto IMAB sempre é feito excedendo as exigências das normas vigentes, em um processo intensivo de testes de resistência e durabilidade, em laboratório de testes próprio e dedicado, o que garante a manufatura de produtos realmente duráveis e com o “Controle de Qualidade Total”. Essa posição de liderança é comprovada pelo fato de a IMAB ser a empresa com um dos maiores números de patentes ativas em seu segmento.</p><p>Assim, os produtos IMAB estão plenamente qualificados a serem utilizados em todos os tipos de obras, atendendo a todas as exigências de qualidade do mercado brasileiro.<br></p>', 'inovacao-e-qualidade', NULL, '2016-08-18 10:48:03', '2016-08-19 20:57:17', NULL),
(3, 'CIDADANIA CORPORATIVA', 'O conceito de cidadania corporativa engloba a forma como as empresas se relacionam e se encaixam com toda a sociedade, baseando-se em ações de sustentabilidade e responsabilidade social.', '6d6b1a79f6db827fba9674c9b70ebe65b550910b.png', '<p>Com uma intensa atuação em projetos de responsabilidade social, a IMAB contribui diretamente para a transformação social de sua comunidade, gerando empregos e renda para a população. Assim, o seu desenvolvimento acontece em total harmonia com a evolução econômica e social de todos.<br></p>', 'cidadania-corporativa', NULL, '2016-08-18 10:48:03', '2016-08-19 20:40:21', NULL),
(4, 'HISTÓRIA DA IMAB', '', 'b9785a9c3640aa77a845cbf399d9eeaf885a4715.png', '<p>Ao abrir suas portas em 1960, a IMAB talvez não imaginasse que a fabricação, quase artesanal, de suas pontas de lanças para portões fosse se transformar em uma grande empresa como é hoje.</p><p>Movidos pela paixão e motivados pelo comprometimento com a qualidade, em 1975 a IMAB mudou-se para uma nova sede, mais ampla e estruturada, onde está localizada até hoje. Nesse momento, sua produção ganhou novos horizontes quando diversificou seu mix de produtos, com maçanetas avulsas e o lançamento de sua própria fechadura, produzida dentro dos mais altos padrões de qualidade.</p><p>Na década de 1990, a IMAB mostrou seu pioneirismo ao lançar sua mais nova fechadura, cujos padrões eram baseados em normativas europeias e americanas, que só foram adotadas pelo mercado nacional após 10 anos, quando a ABNT passou a exigir tais configurações.</p><p>Ao completar 50 anos de história, a IMAB incorporou novos produtos ao seu mix e aliou a tecnologia à alta capacidade técnica de sua equipe de colaboradores. Assim, destaca-se como uma das maiores empresas do setor, com alta capacidade de produção, mantendo sua essência e paixão pelo que faz.<br></p>', 'historia-da-imab', '[{"titulo":["O COME\\u00c7O","AMPLIA\\u00c7\\u00c3O","NOVIDADES","MA\\u00c7ANETAS BRASILEIRAS COM JEITO EUROPEU","MAIS TECNOLOGIA","UM NOVO CONCEITO","50 ANOS DE SUCESSO","Novo ano","O COME\\u00c7O"],"ano":["1960","1970","1980","1990","2000","2010","2015","2020","2030"],"texto":["A IMAB come\\u00e7a sua hist\\u00f3ria fabricando pontas de lan\\u00e7as para port\\u00f5es, quase como obras de arte, feitas \\u00e0 m\\u00e3o com total cuidado.","A IMAB cresce e amplia sua produ\\u00e7\\u00e3o. Em 1973, muda-se para a sede pr\\u00f3pria em Embu-Gua\\u00e7u-SP, local onde a empresa est\\u00e1 at\\u00e9 os dias atuais, em grande \\u00e1rea constru\\u00edda.","A IMAB entra em um novo mercado, o de ma\\u00e7anetas avulsas. A empresa passa a fabricar ma\\u00e7anetas, com design e acabamento diferenciado para ser acoplado na fechadura de outros fabricantes. No fim da d\\u00e9cada, contava 69 tipos de fura\\u00e7\\u00e3o de rosetas e espelhos para acoplar com as fechaduras existentes no mercado. Neste momento, a empresa lan\\u00e7a sua pr\\u00f3pria fechadura, produzida dentro dos mais altos padr\\u00f5es de qualidade.","Foram empregados cinco anos em testes e an\\u00e1lises para se produzir o produto ideal, que chegou ao mercado, em 1994: uma fechadura da mais alta qualidade, cujos padr\\u00f5es eram baseados em normativas europeias e americanas.","Em janeiro de 2002, a ABNT \\u2013 Associa\\u00e7\\u00e3o Brasileira de Normas, publica a norma n\\u00ba 14.913, com exig\\u00eancias para a fabrica\\u00e7\\u00e3o de fechaduras de tr\\u00e1fego intenso. A IMAB n\\u00e3o precisou nem se adaptar \\u00e0 norma, pois os produtos j\\u00e1 atendiam e ultrapassavam as exig\\u00eancias desde o lan\\u00e7amento de sua primeira fechadura, em 1994.","A IMAB incorporou novos produtos ao seu mix, por\\u00e9m n\\u00e3o abandonou o seu mercado tradicional de ferragens, nem o padr\\u00e3o e a qualidade empregados na primeira ponta de lan\\u00e7a produzida na d\\u00e9cada de 1960.","Mantendo sua Qualidade inicial, ampliando sua linha de produtos, e cada vez mais presente no mercado da constru\\u00e7\\u00e3o, a IMAB completa seus 50 anos de Sucesso!","teste","teste"]},{"imagem":["9518d78f536f64da20fa8d976b59beb0a5b7a1f0.png","4835d02ec80d2b72062108c5f776224508c1b37a.png","25db8dda1b6f22960496c78deed79d24717e8248.png","afbdf560b439474e854298d1eff69f4169234d3f.png","1d3070add33b680c6eb1b71ef36d838a19a637f2.png","4a096e649a16a30e9d9a58af9858b1a4b58f2aa8.png","148c40ca6f2f14fdcd4e9ec589113578af24258d.png","ab541a425072b3e3b942dcea775a56e21eed6940.png","dd136f6ece396416597a40c5b3a0e508ddaba0c1.png"]}]', '2016-08-18 10:48:03', '2016-08-22 21:02:43', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Igor Carlos', 'igor@ph2.com.br', '$2y$10$FZ.l1e4BfnWXjbwOntoF3ec0P5EfbiqdGXD3XBTcIjPiuJP1S5gt.', 'wiG0z7Wj7HsvkKeIKD6uTG7w2VMNdoeEnlqCZflMx91hKbPY4izme51JTS3Q', '2016-08-17 19:04:26', '2016-08-18 11:45:23', NULL),
(2, 'Admin', 'admin@imab.com.br', '$2y$10$v3xfJCZen4fmu0bMFjS/i.NiA9MIbJWOHehU/D97u1c.3gVezEO7S', 'jCJ8anKUXlIoEHwPsVOW7JkYEN2TCprtFhyJPiRHrbqYs1rGHzkQubF3TukF', '2016-08-18 11:42:31', '2016-08-18 11:45:44', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios_site`
--

CREATE TABLE `usuarios_site` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categoria_dicas`
--
ALTER TABLE `categoria_dicas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `dicas`
--
ALTER TABLE `dicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dicas-categoria_idx` (`categoria`);

--
-- Índices de tabela `institucionais`
--
ALTER TABLE `institucionais`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `usuarios_site`
--
ALTER TABLE `usuarios_site`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `categoria_dicas`
--
ALTER TABLE `categoria_dicas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `dicas`
--
ALTER TABLE `dicas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `institucionais`
--
ALTER TABLE `institucionais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `usuarios_site`
--
ALTER TABLE `usuarios_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `dicas`
--
ALTER TABLE `dicas`
  ADD CONSTRAINT `fk_dicas-categoria` FOREIGN KEY (`categoria`) REFERENCES `categoria_dicas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
